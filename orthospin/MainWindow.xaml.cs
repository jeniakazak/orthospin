﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;
using orthospin.Consants;
using orthospin.Device;
using orthospin.Model;
using orthospin.Utils;
using orthospin.View;
using orthospin.View.FollowViews;
using orthospin.View.ManualModeViews;
using orthospin.View.NewPatientViews;
using orthospin.ViewModel;

namespace orthospin
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window,IDisposable
    {
        private SplashScreenView _splashScreenView;
        private LogInViewPage _logInView;
        private MainMenuPage _mainMenuPage;
        private ManualModePage _manualMode;
        private NewPatientMainPage _newPatientMainPage;
        private LogView _logView;
        public static SettingsModel _settings;

        private Page currentPage;
        private NativeHotKey _hotKey;

        SerialPortWatcher portWatcher;

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            Closing += (s, e) =>
            {
                try
                {
                    DeviceWrapper.Instance.Dispose();
                }
                catch(Exception ex)
                {
                    Debug.WriteLine(ex);
                }

                if (_settings == null)
                    _settings = new SettingsModel();

                XMLHelper.WriteToXmlFile<SettingsModel>("Settings.xml", _settings);
                ViewModelLocator.Cleanup();

                Dispose();
            };

            Messenger.Default.Register<eNavigationMainPage>(
                 this,
                 OnNavigate);

           
            if (File.Exists("Settings.xml"))
            {
                _settings = XMLHelper.ReadFromXmlFile<SettingsModel>("Settings.xml");
              //  File.Delete("Settings.xml");
            }
            else
                _settings = new SettingsModel();

            HotKeyRegister();

            OnNavigate(eNavigationMainPage.SplashScreen);

            portWatcher = new SerialPortWatcher();
            portWatcher.ComPorts.CollectionChanged += ComPorts_CollectionChanged;

            applicationMainFrame.Navigated += ApplicationMainFrame_Navigated;
        }

        private void ApplicationMainFrame_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            applicationMainFrame?.NavigationService.RemoveBackEntry();
        }

        private void ComPorts_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            _settings.com = e.NewItems[0]?.ToString();
        }

        private void OnNavigate(eNavigationMainPage navigationPage)
        {
            switch (navigationPage)
            {
                case eNavigationMainPage.SplashScreen:
                    _splashScreenView = new SplashScreenView();
                    currentPage = _splashScreenView;
                    break;

                case eNavigationMainPage.LoginPage:
                    _logInView = new LogInViewPage();
                    currentPage = _logInView;
                    break;

                case eNavigationMainPage.MainMenuPage:
                    _mainMenuPage = new MainMenuPage();
                    currentPage = _mainMenuPage;
                    break;

                case eNavigationMainPage.ManualMode:
                    _manualMode = new ManualModePage();
                    currentPage = _manualMode;
                    break;

                case eNavigationMainPage.LogPage:
                    _logView = new LogView();
                    currentPage = _logView;
                    break;

                case eNavigationMainPage.NewPatient:
                    _newPatientMainPage = new NewPatientMainPage();
                    currentPage = _newPatientMainPage;
                    break;

                default:
                    break;
            }

            if (currentPage != null)
                applicationMainFrame.Content = currentPage;
        }

        private void HotKeyRegister()
        {
            _hotKey = new NativeHotKey(Key.S, KeyModifier.Ctrl | KeyModifier.Alt, OnHotKeyHandler);
        }

        private void OnHotKeyHandler(NativeHotKey hotKey)
        {
            Close();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                        if (_hotKey != null)
                            _hotKey.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~MainWindow() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Title = Helpers.GetAppVersion();
        }
    }
}