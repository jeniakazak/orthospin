﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Windows;

namespace orthospin.Device
{
    class DeviceUtils
    {
        public static uint DateTime2UnixTime(DateTime? dt)
        {
            DateTime unixStart = new DateTime(1970, 1, 1);
            //Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            if (dt < unixStart)
                return 0;

            DateTime utcTime = dt.Value.ToUniversalTime();
            return (uint)(utcTime.Subtract(unixStart)).TotalSeconds;
        }

        public static DateTime? UnixTime2DateTime(uint unixTime)
        {
            if (unixTime == 0)
                return null;

            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTime);
            dtDateTime = dtDateTime.ToLocalTime();
            return dtDateTime;
        }

        public static ushort CalculateCRC(ushort[] buffer, int startoffset, int endoffset)
        {
            ushort crc = 0;
            ushort mask = (ushort)0x8004;

            ushort s1, s2;
            for (int i = startoffset; i <= endoffset; i++)
            {
                s1 = crc;
                s2 = (ushort)(s1 & mask);
                s1 = (ushort)(s1 << 1);
                if ((s2 == mask) || (s2 == 0))
                    s1 = (ushort)(s1 | 1);
                s1 += 0x6E3B;
                crc = (ushort)(s1 ^ buffer[i]);
            }
            return crc;
        }

        public static byte[] RawSerialize(object anything)
        {
            int rawsize = Marshal.SizeOf(anything);
            IntPtr buffer = Marshal.AllocHGlobal(rawsize);
            Marshal.StructureToPtr(anything, buffer, false);
            byte[] rawdata = new byte[rawsize];
            Marshal.Copy(buffer, rawdata, 0, rawsize);
            Marshal.FreeHGlobal(buffer);
            return rawdata;
        }

        public static TARGET_DEVICE GetDeviceFromStrutIndex(int strutIndex)
        {
            switch (strutIndex)
            {
                case 1:
                case 2:
                    return TARGET_DEVICE.MASTER_MAIN;
                //case 3:
                //case 4:
                //    return TARGET_DEVICE.MASTER_1;
                //case 5:
                //case 6:
                //    return TARGET_DEVICE.MASTER_2;
            }
            return TARGET_DEVICE.MASTER_MAIN;
        }

        public static int GetStrutFromStrutIndex(int strutIndex)
        {
            switch (strutIndex)
            {
                case 1:
                case 3:
                case 5:
                    return 1;
                case 2:
                case 4:
                case 6:
                    return 0;
            }
            return 0;
        }

        public static bool ConfigurationsAreEqual(EMBEDDED_CONFIG conf1, EMBEDDED_CONFIG conf2)
        {
            //if (conf1.version != conf2.version)
            //    return false;
            if (conf1.milimeter2encoder != conf2.milimeter2encoder)
                return false;
            if (conf1.encoderStepsPerSecond != conf2.encoderStepsPerSecond)
                return false;
            if (conf1.fallTimeMiliSecond != conf2.fallTimeMiliSecond)
                return false;
            if (conf1.treatmentDelaySeconds != conf2.treatmentDelaySeconds)
                return false;
            if (conf1.systemStatus != conf2.systemStatus)
                return false;
            if (conf1.lowBatteryThresh != conf2.lowBatteryThresh)
                return false;
            if (conf1.maxMotorCurrent != conf2.maxMotorCurrent)
                return false;
            if (conf1.maxPWM != conf2.maxPWM)
                return false;
            if (conf1.lastProgramStep != conf2.lastProgramStep)
                return false;

            return true;
        }

        public static bool ProgramsAreEqual(List<PROGRAM_LINE> prog1, List<PROGRAM_LINE> prog2)
        {
            if (prog1.Count != prog2.Count)
                return false;

            for (int i = 0; i < prog1.Count; i++)
            {
                if (prog1[i].unixTime != prog2[i].unixTime)
                    return false;
                if (prog1[i].strut0Position != prog2[i].strut0Position)
                    return false;
                if (prog1[i].strut1Position != prog2[i].strut1Position)
                    return false;
                if (prog1[i].strut2Position != prog2[i].strut2Position)
                    return false;
                if (prog1[i].strut3Position != prog2[i].strut3Position)
                    return false;
                if (prog1[i].strut4Position != prog2[i].strut4Position)
                    return false;
                if (prog1[i].strut5Position != prog2[i].strut5Position)
                    return false;
            }

            return true;
        }

        public static List<PROGRAM_LINE> Reduce30sec(List<PROGRAM_LINE> program)
        {
            List<PROGRAM_LINE> newProg = new List<PROGRAM_LINE>();
            foreach (PROGRAM_LINE line in program)
            {
                PROGRAM_LINE newline = new PROGRAM_LINE();
                newline.strut0Position = line.strut0Position;
                newline.strut1Position = line.strut1Position;
                newline.strut2Position = line.strut2Position;
                newline.strut3Position = line.strut3Position;
                newline.strut4Position = line.strut4Position;
                newline.strut5Position = line.strut5Position;
                newline.unixTime = line.unixTime - 30;
                newProg.Add(newline);
            }
            return newProg;
        }

        /// <summary>
        /// Parse log and present in program format
        /// </summary>
        /// <param name="logLst"></param>
        /// <param name="program"></param>
        /// <returns></returns>
        public static bool Log2Program(List<string> logLst, out List<LOG_LINE> program)
        {
            program = new List<LOG_LINE>();
            try
            {

                string[] log = logLst.ToArray();
                int strut;
                float[] initialValues = { 0, 0, 0, 0, 0, 0 };
                bool initialLoaded = false;
                float[] tmpStrutValues = { 0, 0, 0, 0, 0, 0 };
                float[] tmpStrutCurrents = { 0, 0, 0, 0, 0, 0 };
                float[] strutValues = { 0, 0, 0, 0, 0, 0 };
                string[] parts;
                float encoder2mm = 20864;
                DateTime linetime;
                DateTime blockStart = DateTime.MinValue;
                string[] formats = { "dd/MM/yy HH:mm:ss tt", "dd/MM/yyyy HH:mm:ss tt" };
             

                for (int i = 0; i < log.Length; i++)
                {
                    string line = log[i];
                    parts = line.Split(',');
                    if (!DateTime.TryParse(parts[0], out linetime))
                    {
                        MessageBox.Show("Bad time format: " + line);
                        return false;
                    }

                    if (line.Contains("Initial Position"))
                    {
                        if (parts.Length == 7)
                        {
                            parts = line.Split(':');
                            parts = parts[3].Split(',');
                            if (!initialLoaded)
                            {
                                for (int j = 0; j < 6; j++)
                                    strutValues[j] = initialValues[j] = float.Parse(parts[j].Trim()) / 1000;

                                initialLoaded = true;
                            }
                            else
                            {
                                for (int j = 0; j < 6; j++)
                                    if (initialValues[j] != float.Parse(parts[j].Trim()) / 1000)
                                    {
                                        //MessageBox.Show("Error - log contains 2 different treatments");
                                        //return false;
                                    }
                            }
                        }
                    }

                    if (line.Contains("Starting treatment"))
                    {
                        // Close current block and open a new one
                        bool firstBlock = true;
                        for (int j = 0; j < tmpStrutValues.Length; j++)
                        {
                            if (tmpStrutValues[j] != 0)
                            {
                                firstBlock = false;
                                break;
                            }
                        }
                        if (!firstBlock)
                        {
                            strutValues[0] += (float)tmpStrutValues[0] / encoder2mm;
                            strutValues[1] += (float)tmpStrutValues[1] / encoder2mm;
                            strutValues[2] += (float)tmpStrutValues[2] / encoder2mm;
                            strutValues[3] += (float)tmpStrutValues[3] / encoder2mm;
                            strutValues[4] += (float)tmpStrutValues[4] / encoder2mm;
                            strutValues[5] += (float)tmpStrutValues[5] / encoder2mm;

                            LOG_LINE pl = new LOG_LINE();
                            pl.unixTime = DeviceUtils.DateTime2UnixTime(blockStart);
                            pl.strut0Position = (float)strutValues[0];
                            pl.strut1Position = (float)strutValues[1];
                            pl.strut2Position = (float)strutValues[2];
                            pl.strut3Position = (float)strutValues[3];
                            pl.strut4Position = (float)strutValues[4];
                            pl.strut5Position = (float)strutValues[5];
                            pl.strut0Current = (float)tmpStrutCurrents[0];
                            pl.strut1Current = (float)tmpStrutCurrents[1];
                            pl.strut2Current = (float)tmpStrutCurrents[2];
                            pl.strut3Current = (float)tmpStrutCurrents[3];
                            pl.strut4Current = (float)tmpStrutCurrents[4];
                            pl.strut5Current = (float)tmpStrutCurrents[5];
                            program.Add(pl);
                        }
                        blockStart = linetime;
                    }

                    if (line.Contains("Strut=") && i + 1 < log.Length)
                    {
                        parts = log[i].Split('=');
                        if (parts.Length != 4)
                            continue;

                        strut = int.Parse(parts[1].Substring(0, 1));
                        string[] nextlineparts = log[++i].Split('=');
                        if (nextlineparts.Length != 4)
                            continue;
                        tmpStrutCurrents[strut] = float.Parse(nextlineparts[3].Split(new char[] { ',' },StringSplitOptions.RemoveEmptyEntries)[1].Replace("[","").Replace("]", ""));
                        nextlineparts = nextlineparts[1].Split(',');
                        tmpStrutValues[strut] = float.Parse(nextlineparts[0].Trim());
                    }
                }

                {
                    strutValues[0] += (float)tmpStrutValues[0] / encoder2mm;
                    strutValues[1] += (float)tmpStrutValues[1] / encoder2mm;
                    strutValues[2] += (float)tmpStrutValues[2] / encoder2mm;
                    strutValues[3] += (float)tmpStrutValues[3] / encoder2mm;
                    strutValues[4] += (float)tmpStrutValues[4] / encoder2mm;
                    strutValues[5] += (float)tmpStrutValues[5] / encoder2mm;

                    LOG_LINE pl = new LOG_LINE();
                    pl.unixTime = DeviceUtils.DateTime2UnixTime(blockStart);
                    pl.strut0Position = (float)strutValues[0];
                    pl.strut1Position = (float)strutValues[1];
                    pl.strut2Position = (float)strutValues[2];
                    pl.strut3Position = (float)strutValues[3];
                    pl.strut4Position = (float)strutValues[4];
                    pl.strut5Position = (float)strutValues[5];
                    pl.strut0Current = (float)tmpStrutCurrents[0];
                    pl.strut1Current = (float)tmpStrutCurrents[1];
                    pl.strut2Current = (float)tmpStrutCurrents[2];
                    pl.strut3Current = (float)tmpStrutCurrents[3];
                    pl.strut4Current = (float)tmpStrutCurrents[4];
                    pl.strut5Current = (float)tmpStrutCurrents[5];
                    program.Add(pl);
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }
    }
}
