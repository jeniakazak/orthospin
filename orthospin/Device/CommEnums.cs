﻿//using System.Runtime.InteropServices;

//namespace orthospin.Device
//{
//    public enum TARGET_DEVICE
//    {
//        MASTER_MAIN = 1,
//        SUPERVISOR_MAIN = 2,
//        ANYONE = 7
//    }

//    public enum COMMAND
//    {
//        ECHO = 5,

//        // boot loader
//        START_FW_UPGRADE,// START FW UPGRADE (BOTH MASTER AND SUPERVISOR)
//        SEND_FW_SECTOR,// SEND FW SECTOR DURING FW UPGRADE (BOTH MASTER AND SUPERVISOR)
//        SEND_FW_CRC,// SEND FW CRC DURING FW UPGRADE (BOTH MASTER AND SUPERVISOR)
//        START_TECHNICIAN_MODE,// ENTER TECHNICIAN MODE (BOTH MASTER AND SUPERVISOR)
//        STOP_TECHNICIAN_MODE,// EXIT FROM TECHNICIAN MODE (BOTH MASTER AND SUPERVISOR)
//        GET_TECHNICIAN_MODE,// READ WORKING MODE FROM DEVICE (BOTH MASTER AND SUPERVISOR)
//        READ_MOTOR_CURRENT,// READ THE MOTOR CURRENT
//        READ_ENCODER,// READ ENCODER
//        SET_PWM_CW,// SET PWM CLOCKWISE
//        SET_PWM_CCW,// SET PWM COUNTER CLOCKWISE
//        SET_TIME,// SET TIME ON DEVICE
//        GET_TIME,// READ TIME FROM DEVICE
//        UPLOAD_PROGRAM_START,// UPLOAD TREATMENT PROGRAM FROM PC TO MASTER DEVICE
//        UPLOAD_PROGRAM_PAGE,// UPLOAD TREATMENT PROGRAM FROM PC TO MASTER DEVICE
//        UPLOAD_PROGRAM_END,// UPLOAD TREATMENT PROGRAM FROM PC TO MASTER DEVICE
//        DOWNLOAD_PROGRAM_INFO,// DOWNLOAD TREATMENT PROGRAM FROM MASTER DEVICE TO PC
//        DOWNLOAD_PROGRAM_PAGE,// DOWNLOAD TREATMENT PROGRAM FROM MASTER DEVICE TO PC
//        RUN_PROGRAM,// RUN PROGRAM
//        GET_PROGRAM_STATUS,// GET PROGRAM STATUS
//        UPLOAD_CONFIGURATION,// UPLOAD CONFIGURATION STRUCTURE
//        DOWNLOAD_CONFIGURATION,// DOWNLOAD CONFIGURATION STRUCTURE
//        RESET_LOG,// DELETE ALL LOG ENTRIES
//        GET_LOG_INFO,// GET LOG PAGES COUNT
//        DOWNLOAD_LOG_PAGE,// DOWNLOAD A LOG PAGE
//        START_TREATMENT,// ENTER TREATMENT MODE
//        READ_BATTERY,// READ BATTERY
//        SET_WAKEUP_TIME,// SET WAKEUP TIME
//        SHUTDOWN,// SHUTDOWN
//        SPIN_STRUT,// SPIN STRUT
//        RUN_BIT,// RUN INITIATED BIT
//        RUN_MOTOR_TEST,// RUN MOTOR TEST
//        SET_LED,// SET_LED
//        RESET_LED,// RESET_LED
//        BUZZER,// BUZZER
//        RUN_PROGRAM_NOW,// RUN PROGRAM NOW
//        READ_ALL_ENCODERS,// READ ENCODER
//        READ_SV_ERROR,// READ SUPERVISOR ERROR REGISTERS
//        ERROR_CODE
//    }

//    public enum LED
//    {
//        GPIO_RED_LED = 0,
//        GPIO_GREEN_LED,
//        GPIO_ORANGE_LED
//    }

//    public enum RET_STATUS
//    {
//        RET_OK = 0,
//        RET_FAIL
//    }

//    [StructLayout(LayoutKind.Sequential, Pack = 1)]
//    public struct MSG_HDR
//    {
//        public uint separator;      // message delimiter
//        public TARGET_DEVICE device;// master / supervisor
//        public COMMAND msgCode;     // messager code
//        public int msgData;         // data - used as addition to the payload or as the message data itself
//        public ushort dataSize;     // payload size (including crc)
//    }

//    [StructLayout(LayoutKind.Sequential, Pack = 1)]
//    public struct PROGRAM_INFO
//    {
//        public uint lineCount;
//        public uint lastExecutedLine;
//        public uint programCRC;
//    }

//    [StructLayout(LayoutKind.Sequential, Pack = 1)]
//    public struct PROGRAM_LINE
//    {
//        public uint unixTime;
//        public float strut0Position;
//        public float strut1Position;
//        public float strut2Position;
//        public float strut3Position;
//        public float strut4Position;
//        public float strut5Position;
//        public float strut0Current;
//        public float strut1Current;
//        public float strut2Current;
//        public float strut3Current;
//        public float strut4Current;
//        public float strut5Current;
//        public PROGRAM_LINE(uint ut, float s0, float s1, float s2, float s3, float s4, float s5)
//        {
//            unixTime = ut;
//            strut0Position = s0;
//            strut1Position = s1;
//            strut2Position = s2;
//            strut3Position = s3;
//            strut4Position = s4;
//            strut5Position = s5;
//            strut0Current = 0;
//            strut1Current = 0;
//            strut2Current = 0;
//            strut3Current = 0;
//            strut4Current = 0;
//            strut5Current = 0;
//        }
//    }

//    [StructLayout(LayoutKind.Sequential, Pack = 1)]
//    public struct LOG_RECORD
//    {
//        public uint header;         // 0x23242526
//        public uint timestamp;      // unix time
//        public byte size;           // messager size
//    }

//    [StructLayout(LayoutKind.Sequential, Pack = 1)]
//    public struct EMBEDDED_CONFIG
//    {
//        public uint version;                 // configuration version
//        public uint milimeter2encoder;       // how many encoder steps represent 1 milimeter
//        public uint encoderStepsPerSecond;   // max number of encoder steps per second during treatment
//        public uint fallTimeMiliSecond;      // treatment motor speed fall time
//        public uint treatmentDelaySeconds;   // how long to wait before treatment
//        public uint systemStatus;			 // status (OK / error) - used for blink color
//        public float lowBatteryThresh;		 // low batt thresh in volts
//        public uint maxMotorCurrent;		 // maximum allowed motor current
//        public uint maxPWM;         		 // maximum allowed pwm during treatment
//        public float P;                      // strut motion control - P
//        public float I;                      // strut motion control - I
//        public float D;                      // strut motion control - D
//        public uint lastProgramStep;         // index of the last program step that was successfully executed
//        public uint programCompleteNextBeep; // next unix time to sound program complete beep (if status is program complete)
//        public uint emergencyButtonNextBeep; // next unix time to sound emergency button beep (if status is emergency button pressed)
//        public ushort safetyLevel;           // 0=max safety, 1=no current measure, 2=no supervisor
//        public ushort crc;                   // crc
//    }

//    [StructLayout(LayoutKind.Sequential, Pack = 1)]
//    public struct HOST2BOARD
//    {
//        public MSG_HDR header;  // message header
//        public byte[] data;     // message payload
//        public ushort crc;      // crc for all message data (including header)
//    }

//    [StructLayout(LayoutKind.Sequential, Pack = 1)]
//    public struct BOARD2HOST
//    {
//        public MSG_HDR header;  // message header
//        public byte[] data;     // message payload
//        public ushort crc;      // crc for all message data (including header)
//    }
//}
