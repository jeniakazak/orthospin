﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows;

namespace orthospin.Device
{

    public enum TARGET_DEVICE
    {
        MASTER_MAIN = 1,
        SUPERVISOR_MAIN = 2,
        ANYONE = 7
    }

    public enum COMMAND
    {
        ECHO = 5,

        // boot loader
        START_FW_UPGRADE,// START FW UPGRADE (BOTH MASTER AND SUPERVISOR)
        SEND_FW_SECTOR,// SEND FW SECTOR DURING FW UPGRADE (BOTH MASTER AND SUPERVISOR)
        SEND_FW_CRC,// SEND FW CRC DURING FW UPGRADE (BOTH MASTER AND SUPERVISOR)
        START_TECHNICIAN_MODE,// ENTER TECHNICIAN MODE (BOTH MASTER AND SUPERVISOR)
        STOP_TECHNICIAN_MODE,// EXIT FROM TECHNICIAN MODE (BOTH MASTER AND SUPERVISOR)
        GET_TECHNICIAN_MODE,// READ WORKING MODE FROM DEVICE (BOTH MASTER AND SUPERVISOR)
        READ_MOTOR_CURRENT,// READ THE MOTOR CURRENT
        READ_ENCODER,// READ ENCODER
        SET_PWM_CW,// SET PWM CLOCKWISE
        SET_PWM_CCW,// SET PWM COUNTER CLOCKWISE
        SET_TIME,// SET TIME ON DEVICE
        GET_TIME,// READ TIME FROM DEVICE
        UPLOAD_PROGRAM_START,// UPLOAD TREATMENT PROGRAM FROM PC TO MASTER DEVICE
        UPLOAD_PROGRAM_PAGE,// UPLOAD TREATMENT PROGRAM FROM PC TO MASTER DEVICE
        UPLOAD_PROGRAM_END,// UPLOAD TREATMENT PROGRAM FROM PC TO MASTER DEVICE
        DOWNLOAD_PROGRAM_INFO,// DOWNLOAD TREATMENT PROGRAM FROM MASTER DEVICE TO PC
        DOWNLOAD_PROGRAM_PAGE,// DOWNLOAD TREATMENT PROGRAM FROM MASTER DEVICE TO PC
        RUN_PROGRAM,// RUN PROGRAM
        GET_PROGRAM_STATUS,// GET PROGRAM STATUS
        UPLOAD_CONFIGURATION,// UPLOAD CONFIGURATION STRUCTURE
        DOWNLOAD_CONFIGURATION,// DOWNLOAD CONFIGURATION STRUCTURE
        RESET_LOG,// DELETE ALL LOG ENTRIES
        GET_LOG_INFO,// GET LOG PAGES COUNT
        DOWNLOAD_LOG_PAGE,// DOWNLOAD A LOG PAGE
        START_TREATMENT,// ENTER TREATMENT MODE
        READ_BATTERY,// READ BATTERY
        SET_WAKEUP_TIME,// SET WAKEUP TIME
        SHUTDOWN,// SHUTDOWN
        SPIN_STRUT,// SPIN STRUT
        RUN_BIT,// RUN INITIATED BIT
        RUN_MOTOR_TEST,// RUN MOTOR TEST
        SET_LED,// SET_LED
        RESET_LED,// RESET_LED
        BUZZER,// BUZZER
        RUN_PROGRAM_NOW,// RUN PROGRAM NOW
        READ_ALL_ENCODERS,// READ ENCODER
        READ_SV_ERROR,// READ SUPERVISOR ERROR REGISTERS
        ERROR_CODE
    }

    public enum LED
    {
        GPIO_RED_LED = 0,
        GPIO_GREEN_LED,
        GPIO_ORANGE_LED
    }

    public enum RET_STATUS
    {
        RET_OK = 0,
        RET_FAIL
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MSG_HDR
    {
        public uint separator;      // message delimiter
        public TARGET_DEVICE device;// master / supervisor
        public COMMAND msgCode;     // messager code
        public int msgData;         // data - used as addition to the payload or as the message data itself
        public ushort dataSize;     // payload size (including crc)
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PROGRAM_INFO
    {
        public uint lineCount;
        public uint lastExecutedLine;
        public uint programCRC;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PROGRAM_LINE
    {
        public uint unixTime;
        public float strut0Position;
        public float strut1Position;
        public float strut2Position;
        public float strut3Position;
        public float strut4Position;
        public float strut5Position;
        public PROGRAM_LINE(uint ut, float s0, float s1, float s2, float s3, float s4, float s5)
        {
            unixTime = ut;
            strut0Position = s0;
            strut1Position = s1;
            strut2Position = s2;
            strut3Position = s3;
            strut4Position = s4;
            strut5Position = s5;
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct LOG_LINE
    {
        public uint unixTime;
        public float strut0Position;
        public float strut1Position;
        public float strut2Position;
        public float strut3Position;
        public float strut4Position;
        public float strut5Position;
        public float strut0Current;
        public float strut1Current;
        public float strut2Current;
        public float strut3Current;
        public float strut4Current;
        public float strut5Current;
        public LOG_LINE(uint ut, float s0, float s1, float s2, float s3, float s4, float s5)
        {
            unixTime = ut;
            strut0Position = s0;
            strut1Position = s1;
            strut2Position = s2;
            strut3Position = s3;
            strut4Position = s4;
            strut5Position = s5;
            strut0Current = 0;
            strut1Current = 0;
            strut2Current = 0;
            strut3Current = 0;
            strut4Current = 0;
            strut5Current = 0;
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct LOG_RECORD
    {
        public uint header;         // 0x23242526
        public uint timestamp;      // unix time
        public byte size;           // messager size
    }


    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct EMBEDDED_CONFIG
    {
        public uint version;                 // configuration version
        public uint milimeter2encoder;       // how many encoder steps represent 1 milimeter
        public uint encoderStepsPerSecond;   // max number of encoder steps per second during treatment
        public uint fallTimeMiliSecond;      // treatment motor speed fall time
        public uint treatmentDelaySeconds;   // how long to wait before treatment
        public uint systemStatus;			 // status (OK / error) - used for blink color
        public float lowBatteryThresh;		 // low batt thresh in volts
        public uint maxMotorCurrent;		 // maximum allowed motor current
        public uint maxPWM;         		 // maximum allowed pwm during treatment
        public float P;                      // strut motion control - P
        public float I;                      // strut motion control - I
        public float D;                      // strut motion control - D
        public uint lastProgramStep;         // index of the last program step that was successfully executed
        public uint programCompleteNextBeep; // next unix time to sound program complete beep (if status is program complete)
        public uint emergencyButtonNextBeep; // next unix time to sound emergency button beep (if status is emergency button pressed)
        public ushort safetyLevel;           // 0=max safety, 1=no current measure, 2=no supervisor
        public ushort crc;                   // crc
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct HOST2BOARD
    {
        public MSG_HDR header;  // message header
        public byte[] data;     // message payload
        public ushort crc;      // crc for all message data (including header)
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BOARD2HOST
    {
        public MSG_HDR header;  // message header
        public byte[] data;     // message payload
        public ushort crc;      // crc for all message data (including header)
    }

    public class Comm : IDisposable
    {
        private const uint SEPARATOR = 0x89ABCDEF;
        private const int PROGRAM_PAGE_SIZE = 0x800;
        private const int TIMEOUT = 5000;

        //bool _simulation = true;
       public bool _simulation = false;
        SerialPort _port;
        object _commLock = new object();

        // progress indication for firmware upgrade
        private int _fwUpgradePercent = 0;
        public int FwUpgradePercent
        {
            get { return _fwUpgradePercent; }
        }

        public bool Connected
        {
            get
            {
                if (_simulation)
                    return true;

                if (_port == null)
                    return false;

                return _port.IsOpen;
            }
        }

        public bool Disconnect()
        {
            if (_simulation)
                return true;

            if (_port.IsOpen)
            {
                _port.Close();
                return true;
            }
            return false;
        }

        public bool Connect(string comport)
        {
            try
            {
                if (_simulation)
                    return true;

                _port = new SerialPort();
                _port.BaudRate = 115200; // 921600;
                _port.Parity = Parity.None;
                _port.StopBits = StopBits.One;
                _port.DataBits = 8;
                _port.PortName = comport;
                _port.WriteBufferSize = 4096;
                _port.ReadBufferSize = 4096;

                _port.ReadTimeout = TIMEOUT;
                _port.DtrEnable = true;
                _port.RtsEnable = true;

                _port.Open();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool TestEncoder(int strutIndex)
        {
            return true;
            //BOARD2HOST inMsg;
            //HOST2BOARD outMsg = new HOST2BOARD();
            //outMsg.header.separator = SEPARATOR;
            //outMsg.header.device = TARGET_DEVICE.MASTER_MAIN;
            //outMsg.header.msgCode = COMMAND.TEST_ENCODER_POWER;
            //outMsg.header.msgData = strutIndex;
            //outMsg.header.dataSize = 0;
            //return SendReceive(outMsg, out inMsg);
        }

        public bool SpinStrut(int strutIndex, int encoderSteps)
        {
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = TARGET_DEVICE.MASTER_MAIN;
            outMsg.header.msgCode = COMMAND.SPIN_STRUT;
            outMsg.header.msgData = 0;

            outMsg.header.dataSize = 12;
            outMsg.data = new byte[outMsg.header.dataSize];

            byte[] tmp = BitConverter.GetBytes(strutIndex);
            Buffer.BlockCopy(tmp, 0, outMsg.data, 0, 4);

            tmp = BitConverter.GetBytes(Math.Abs(encoderSteps));
            Buffer.BlockCopy(tmp, 0, outMsg.data, 4, 4);

            tmp = BitConverter.GetBytes(encoderSteps > 0 ? 0 : 1);
            Buffer.BlockCopy(tmp, 0, outMsg.data, 8, 4);

            return SendReceive(outMsg, out inMsg, 10000, false);
        }

        public bool SetPWM(int motor, bool cw, int pwm)
        {
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = TARGET_DEVICE.MASTER_MAIN;
            outMsg.header.msgCode = cw ? COMMAND.SET_PWM_CW : COMMAND.SET_PWM_CCW;
            outMsg.header.msgData = Math.Abs(pwm);
            outMsg.header.dataSize = 4;
            outMsg.data = new byte[4];
            outMsg.data[0] = (byte)motor;
            return SendReceive(outMsg, out inMsg);
        }

        public bool RunBIT(TARGET_DEVICE device)
        {
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.RUN_BIT;
            outMsg.header.msgData = 0;
            outMsg.header.dataSize = 0;
            return SendReceive(outMsg, out inMsg);
        }

        public bool Shutdown(TARGET_DEVICE device)
        {
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.SHUTDOWN;
            outMsg.header.dataSize = 0;
            return SendReceive(outMsg, out inMsg, 0, false);
        }

        public bool StartTechnicianMode(TARGET_DEVICE device, bool synchronousMode)
        {
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.START_TECHNICIAN_MODE;
            outMsg.header.dataSize = 0;
            if (synchronousMode)
                return SendReceive(outMsg, out inMsg);
            else
            {
                lock (_commLock)
                {
                    // purge
                    _port.DiscardInBuffer();
                    _port.DiscardOutBuffer();

                    // use list for byte[] manipulation
                    List<byte> msg = new List<byte>();

                    byte[] arrHeader = Header2ByteArray(outMsg.header);
                    msg.AddRange(arrHeader);
                    if (outMsg.data != null)
                        msg.AddRange(outMsg.data);

                    ushort[] dataforcrc = new ushort[(int)Math.Round((double)(msg.Count / 2))];
                    Buffer.BlockCopy(msg.ToArray(), 0, dataforcrc, 0, msg.Count);
                    ushort crc = DeviceUtils.CalculateCRC(dataforcrc, 0, dataforcrc.Length - 1);
                    msg.Add((byte)crc); // lsb
                    msg.Add((byte)((crc >> 8) & 0xFF));
                    _port.Write(msg.ToArray(), 0, msg.Count);
                }
                return true;
            }
        }

        public bool StopTechnicianMode(TARGET_DEVICE device)
        {
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.STOP_TECHNICIAN_MODE;
            outMsg.header.dataSize = 0;
            return SendReceive(outMsg, out inMsg);
        }

        public bool GetTechnicianMode(TARGET_DEVICE device, out bool mode)
        {
            mode = false;
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.GET_TECHNICIAN_MODE;
            outMsg.header.dataSize = 0;
            if (!SendReceive(outMsg, out inMsg))
                return false;

            mode = inMsg.data[0] == 1 ? true : false;
            return true;
        }

        public bool ReadEncoder(int index, out int encoder, out int encoderIndex)
        {
            encoder = 0;
            encoderIndex = 0;
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = TARGET_DEVICE.MASTER_MAIN;
            outMsg.header.msgCode = COMMAND.READ_ENCODER;
            outMsg.header.msgData = index;
            outMsg.header.dataSize = 0;
            if (!SendReceive(outMsg, out inMsg))
                return false;

            encoder = (int)(((uint)inMsg.data[3] << 24) + ((uint)inMsg.data[2] << 16) + ((uint)inMsg.data[1] << 8) + (uint)inMsg.data[0]);
            encoderIndex = (int)(((uint)inMsg.data[7] << 24) + ((uint)inMsg.data[6] << 16) + ((uint)inMsg.data[5] << 8) + (uint)inMsg.data[4]);
            return true;
        }

        public bool ReadAllEncoders(TARGET_DEVICE device,
                out int encoder0, out int encoder1, out int encoder2, out int encoder3, out int encoder4, out int encoder5,
                out byte errA0, out byte errA1, out byte errA2, out byte errA3, out byte errA4, out byte errA5,
                out byte errB0, out byte errB1, out byte errB2, out byte errB3, out byte errB4, out byte errB5)
        {
            encoder0 = encoder1 = encoder2 = encoder3 = encoder4 = encoder5 = 0;
            errA0 = errA1 = errA2 = errA3 = errA4 = errA5 = 0;
            errB0 = errB1 = errB2 = errB3 = errB4 = errB5 = 0;
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.READ_ALL_ENCODERS;
            outMsg.header.msgData = 0;
            outMsg.header.dataSize = 0;
            if (!SendReceive(outMsg, out inMsg))
                return false;
            if (inMsg.data == null)
                return false;

            encoder0 = (int)(((uint)inMsg.data[3] << 24) + ((uint)inMsg.data[2] << 16) + ((uint)inMsg.data[1] << 8) + (uint)inMsg.data[0]);
            encoder1 = (int)(((uint)inMsg.data[7] << 24) + ((uint)inMsg.data[6] << 16) + ((uint)inMsg.data[5] << 8) + (uint)inMsg.data[4]);
            encoder2 = (int)(((uint)inMsg.data[11] << 24) + ((uint)inMsg.data[10] << 16) + ((uint)inMsg.data[9] << 8) + (uint)inMsg.data[8]);
            encoder3 = (int)(((uint)inMsg.data[15] << 24) + ((uint)inMsg.data[14] << 16) + ((uint)inMsg.data[13] << 8) + (uint)inMsg.data[12]);
            encoder4 = (int)(((uint)inMsg.data[19] << 24) + ((uint)inMsg.data[18] << 16) + ((uint)inMsg.data[17] << 8) + (uint)inMsg.data[16]);
            encoder5 = (int)(((uint)inMsg.data[23] << 24) + ((uint)inMsg.data[22] << 16) + ((uint)inMsg.data[21] << 8) + (uint)inMsg.data[20]);
            errA0 = inMsg.data[24];
            errA1 = inMsg.data[25];
            errA2 = inMsg.data[26];
            errA3 = inMsg.data[27];
            errA4 = inMsg.data[28];
            errA5 = inMsg.data[29];
            errB0 = inMsg.data[30];
            errB1 = inMsg.data[31];
            errB2 = inMsg.data[32];
            errB3 = inMsg.data[33];
            errB4 = inMsg.data[34];
            errB5 = inMsg.data[35];
            return true;
        }

        public bool ReadSupervisorErrors(out int encoderIndex,
                                        out int encoderExpectedValue,
                                        out int encoderActualValue,
                                        out int current,
                                        out int time,
                                        out int failReason)
        {
            encoderIndex = encoderExpectedValue = encoderActualValue = current = time = failReason = 0;
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = TARGET_DEVICE.SUPERVISOR_MAIN;
            outMsg.header.msgCode = COMMAND.READ_SV_ERROR;
            outMsg.header.msgData = 0;
            outMsg.header.dataSize = 0;
            if (!SendReceive(outMsg, out inMsg))
                return false;
            if (inMsg.data == null)
                return false;

            if (inMsg.data.Length < 24)
            {
                MessageBox.Show(string.Format("msg size is {0} bytes", inMsg.data.Length));
                return false;
            }
            encoderIndex = (int)(((uint)inMsg.data[3] << 24) + ((uint)inMsg.data[2] << 16) + ((uint)inMsg.data[1] << 8) + (uint)inMsg.data[0]);
            encoderExpectedValue = (int)(((uint)inMsg.data[7] << 24) + ((uint)inMsg.data[6] << 16) + ((uint)inMsg.data[5] << 8) + (uint)inMsg.data[4]);
            encoderActualValue = (int)(((uint)inMsg.data[11] << 24) + ((uint)inMsg.data[10] << 16) + ((uint)inMsg.data[9] << 8) + (uint)inMsg.data[8]);
            current = (int)(((uint)inMsg.data[15] << 24) + ((uint)inMsg.data[14] << 16) + ((uint)inMsg.data[13] << 8) + (uint)inMsg.data[12]);
            time = (int)(((uint)inMsg.data[19] << 24) + ((uint)inMsg.data[18] << 16) + ((uint)inMsg.data[17] << 8) + (uint)inMsg.data[16]);
            failReason = (int)(((uint)inMsg.data[23] << 24) + ((uint)inMsg.data[22] << 16) + ((uint)inMsg.data[21] << 8) + (uint)inMsg.data[20]);
            return true;
        }

        public bool ReadBattery(out ushort adc)
        {
            adc = 0;
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = TARGET_DEVICE.MASTER_MAIN;
            outMsg.header.msgCode = COMMAND.READ_BATTERY;
            outMsg.header.dataSize = 0;
            if (!SendReceive(outMsg, out inMsg))
                return false;

            adc = (ushort)inMsg.header.msgData;
            return true;
        }

        public bool ReadMotorCurrent(out ushort current)
        {
            current = 0;
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = TARGET_DEVICE.MASTER_MAIN;
            outMsg.header.msgCode = COMMAND.READ_MOTOR_CURRENT;
            outMsg.header.dataSize = 0;
            if (!SendReceive(outMsg, out inMsg))
                return false;

            current = (ushort)inMsg.header.msgData;
            return true;
        }

        public bool SetWakeupTime(TARGET_DEVICE device, int secondsFromNow)
        {
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.SET_WAKEUP_TIME;
            outMsg.header.dataSize = 0;
            outMsg.header.msgData = (int)secondsFromNow;
            return (SendReceive(outMsg, out inMsg));
        }

        public bool SetLED(TARGET_DEVICE device, LED led)
        {
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.SET_LED;
            outMsg.header.dataSize = 0;
            outMsg.header.msgData = (int)led;
            return (SendReceive(outMsg, out inMsg));
        }

        public bool ResetLED(TARGET_DEVICE device, LED led)
        {
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.RESET_LED;
            outMsg.header.dataSize = 0;
            outMsg.header.msgData = (int)led;
            return (SendReceive(outMsg, out inMsg));
        }

        public bool PlayBuzzer(TARGET_DEVICE device, int miliseconds)
        {
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.BUZZER;
            outMsg.header.dataSize = 0;
            outMsg.header.msgData = miliseconds;
            return (SendReceive(outMsg, out inMsg));
        }

        public bool SetTime(TARGET_DEVICE device, DateTime time)
        {
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.SET_TIME;
            outMsg.header.dataSize = 0;
            outMsg.header.msgData = (int)DeviceUtils.DateTime2UnixTime(time);
            return (SendReceive(outMsg, out inMsg));
        }

        public bool GetTime(TARGET_DEVICE device, out DateTime? time)
        {
            time = new DateTime();

            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.GET_TIME;
            outMsg.header.dataSize = 0;
            if (!SendReceive(outMsg, out inMsg))
                return false;

            time = DeviceUtils.UnixTime2DateTime((uint)inMsg.header.msgData);
            return true;
        }

        /// <summary>
        /// This function can take a long time to run
        /// It should be executed in a separate thread
        /// </summary>
        /// <param name="device"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool UpgradeFirmware(TARGET_DEVICE device, string filename)
        {
            _fwUpgradePercent = 0;

            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.START_FW_UPGRADE;
            outMsg.header.dataSize = 0;
            if (!SendReceive(outMsg, out inMsg))
                return false;

            byte[] fileBytes = File.ReadAllBytes(filename);
            int offset = 0;
            while (offset < fileBytes.Length)
            {
                int pageSize = (fileBytes.Length - offset) > PROGRAM_PAGE_SIZE ? PROGRAM_PAGE_SIZE : (fileBytes.Length - offset);
                outMsg = new HOST2BOARD();
                outMsg.header.separator = SEPARATOR;
                outMsg.header.device = device;
                outMsg.header.msgCode = COMMAND.SEND_FW_SECTOR;
                outMsg.header.dataSize = (ushort)pageSize;
                outMsg.data = new byte[pageSize];
                Buffer.BlockCopy(fileBytes, offset, outMsg.data, 0, pageSize);
                if (!SendReceive(outMsg, out inMsg))
                    return false;

                offset += pageSize;
                _fwUpgradePercent = (int)((double)offset / (double)fileBytes.Length * 100.0);
            }

            ushort[] dataforcrc = new ushort[(int)Math.Round((double)(fileBytes.Length / 2))];
            Buffer.BlockCopy(fileBytes, 0, dataforcrc, 0, fileBytes.Length);
            ushort crc = DeviceUtils.CalculateCRC(dataforcrc, 0, dataforcrc.Length - 1);

            outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.SEND_FW_CRC;
            outMsg.header.dataSize = 0;
            outMsg.header.msgData = crc;
            if (!SendReceive(outMsg, out inMsg))
                return false;

            return true;
        }

        bool _abortTechnicianMode = false;
        bool _technicianMode = false;
        public bool TechnicianMode
        {
            get { return _technicianMode; }
        }

        public bool EnterTechnicianMode(TARGET_DEVICE device)
        {
            Thread t = new Thread(EnterTechnicianModeThread);
            t.Start(device);
            return true;
        }

        public bool ExitTechnicianMode()
        {
            _abortTechnicianMode = true;
            return true;
        }

        private void EnterTechnicianModeThread(object o)
        {
            if (_simulation)
            {
                _technicianMode = true;
                return;
            }
               


            TARGET_DEVICE device = (TARGET_DEVICE)o;
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.GET_TIME;
            outMsg.header.dataSize = 0;

            _port.DiscardInBuffer();
            _abortTechnicianMode = false;

            int delay = 20; //ms
            while (!_abortTechnicianMode)
            {
                if (_port.BytesToRead > 10)
                {
                    _technicianMode = true;
                    Thread.Sleep(200);
                    return;
                }

                SendReceive(outMsg, out inMsg, delay, false);

                Thread.Sleep(delay);
            }
        }

        public bool UploadConfiguration(TARGET_DEVICE device, EMBEDDED_CONFIG config)
        {
            config.crc = 0;

            byte[] cfgArray = Config2ByteArray(config);
            ushort[] dataforcrc = new ushort[cfgArray.Length / 2];
            Buffer.BlockCopy(cfgArray, 0, dataforcrc, 0, cfgArray.Length);
            ushort crc = DeviceUtils.CalculateCRC(dataforcrc, 0, dataforcrc.Length - 1);
            config.crc = crc;

            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.UPLOAD_CONFIGURATION;
            outMsg.header.dataSize = (ushort)Marshal.SizeOf(typeof(EMBEDDED_CONFIG));
            outMsg.data = DeviceUtils.RawSerialize(config);
            return (SendReceive(outMsg, out inMsg, 5000, true, 1000));
        }

        public bool DownloadConfiguration(TARGET_DEVICE device, out EMBEDDED_CONFIG config)
        {
            config = new EMBEDDED_CONFIG();

            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.DOWNLOAD_CONFIGURATION;
            outMsg.header.dataSize = 0;
            if (!SendReceive(outMsg, out inMsg, 5000, true, 1000))
                return false;

            config = ByteArray2Config(inMsg.data);
            return true;
        }

        public bool UploadProgram(TARGET_DEVICE device, List<PROGRAM_LINE> program)
        {
            // Message is limited to x
            int lineSize = Marshal.SizeOf(typeof(PROGRAM_LINE));
            BOARD2HOST inMsg;

            // Serialize program
            int index = 0;
            byte[] binProgram = new byte[program.Count * lineSize];
            for (int i = 0; i < program.Count; i++)
            {
                PROGRAM_LINE line = program[i];
                byte[] tmp = BitConverter.GetBytes(line.unixTime);
                binProgram[index++] = tmp[0];
                binProgram[index++] = tmp[1];
                binProgram[index++] = tmp[2];
                binProgram[index++] = tmp[3];

                tmp = BitConverter.GetBytes(line.strut0Position);
                binProgram[index++] = tmp[0];
                binProgram[index++] = tmp[1];
                binProgram[index++] = tmp[2];
                binProgram[index++] = tmp[3];

                tmp = BitConverter.GetBytes(line.strut1Position);
                binProgram[index++] = tmp[0];
                binProgram[index++] = tmp[1];
                binProgram[index++] = tmp[2];
                binProgram[index++] = tmp[3];

                tmp = BitConverter.GetBytes(line.strut2Position);
                binProgram[index++] = tmp[0];
                binProgram[index++] = tmp[1];
                binProgram[index++] = tmp[2];
                binProgram[index++] = tmp[3];

                tmp = BitConverter.GetBytes(line.strut3Position);
                binProgram[index++] = tmp[0];
                binProgram[index++] = tmp[1];
                binProgram[index++] = tmp[2];
                binProgram[index++] = tmp[3];

                tmp = BitConverter.GetBytes(line.strut4Position);
                binProgram[index++] = tmp[0];
                binProgram[index++] = tmp[1];
                binProgram[index++] = tmp[2];
                binProgram[index++] = tmp[3];

                tmp = BitConverter.GetBytes(line.strut5Position);
                binProgram[index++] = tmp[0];
                binProgram[index++] = tmp[1];
                binProgram[index++] = tmp[2];
                binProgram[index++] = tmp[3];
            }

            ushort[] dataforcrc = new ushort[(int)Math.Round((double)(binProgram.Length / 2))];
            Buffer.BlockCopy(binProgram, 0, dataforcrc, 0, binProgram.Length);

            PROGRAM_INFO pinfo = new PROGRAM_INFO();
            pinfo.lastExecutedLine = 0;
            pinfo.lineCount = (uint)program.Count;
            pinfo.programCRC = DeviceUtils.CalculateCRC(dataforcrc, 0, dataforcrc.Length - 1);

            // Send start upload message
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.UPLOAD_PROGRAM_START;
            outMsg.header.msgData = 0;
            outMsg.header.dataSize = (ushort)Marshal.SizeOf(typeof(PROGRAM_INFO));
            outMsg.data = ProgramInfoByteArray(pinfo);
            if (!SendReceive(outMsg, out inMsg, 5000, true, 1000))
                return false;

            int pages = binProgram.Length / PROGRAM_PAGE_SIZE;
            int page = 0;
            for (int i = 0; i < pages; i++)
            {
                outMsg = new HOST2BOARD();
                outMsg.header.separator = SEPARATOR;
                outMsg.header.device = device;
                outMsg.header.msgCode = COMMAND.UPLOAD_PROGRAM_PAGE;
                outMsg.header.msgData = page;
                outMsg.header.dataSize = PROGRAM_PAGE_SIZE;
                outMsg.data = new byte[outMsg.header.dataSize];
                Buffer.BlockCopy(binProgram, page * PROGRAM_PAGE_SIZE, outMsg.data, 0, PROGRAM_PAGE_SIZE);
                if (!SendReceive(outMsg, out inMsg, 5000, true, 1000))
                    return false;

                page++;
            }

            int lastpage = binProgram.Length % PROGRAM_PAGE_SIZE;
            if (lastpage > 0)
            {
                outMsg = new HOST2BOARD();
                outMsg.header.separator = SEPARATOR;
                outMsg.header.device = device;
                outMsg.header.msgCode = COMMAND.UPLOAD_PROGRAM_PAGE;
                outMsg.header.msgData = page;
                outMsg.header.dataSize = (ushort)lastpage;
                outMsg.data = new byte[outMsg.header.dataSize];
                Buffer.BlockCopy(binProgram, page * PROGRAM_PAGE_SIZE, outMsg.data, 0, lastpage);
                if (!SendReceive(outMsg, out inMsg, 5000, true, 1000))
                    return false;
            }

            // Send start upload message
            outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.UPLOAD_PROGRAM_END;
            outMsg.header.msgData = 0;
            outMsg.header.dataSize = 0;
            SendReceive(outMsg, out inMsg);
            if (inMsg.header.msgCode == COMMAND.ERROR_CODE) // on crc mismatch - error is returned
                return false;

            return true;
        }

        public bool DownloadProgram(TARGET_DEVICE device, out List<PROGRAM_LINE> program)
        {
            int lineSize = Marshal.SizeOf(typeof(PROGRAM_LINE));
            program = new List<PROGRAM_LINE>();

            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.DOWNLOAD_PROGRAM_INFO;
            outMsg.header.dataSize = 0;
            if (!SendReceive(outMsg, out inMsg))
                return false;

            PROGRAM_INFO pinfo = ByteArray2ProgramInfo(inMsg.data);
            uint programSize = (uint)(pinfo.lineCount * lineSize);
            Thread.Sleep(200);

            int page = 0;
            int offset = 0;
            List<byte> binData = new List<byte>();
            while (offset < programSize)
            {
                outMsg = new HOST2BOARD();
                outMsg.header.separator = SEPARATOR;
                outMsg.header.device = device;
                outMsg.header.msgCode = COMMAND.DOWNLOAD_PROGRAM_PAGE;
                outMsg.header.dataSize = 0;
                outMsg.header.msgData = page;
                if (!SendReceive(outMsg, out inMsg))
                    return false;

                binData.AddRange(inMsg.data);
                offset += inMsg.data.Length;
                page++;
            }

            byte[] prog = binData.ToArray();
            int lineCount = prog.Length / lineSize;
            int index = 0;
            for (int i = 0; i < lineCount; i++)
            {
                PROGRAM_LINE line = new PROGRAM_LINE();
                line.unixTime = System.BitConverter.ToUInt32(prog, index);
                index += 4;

                line.strut0Position = System.BitConverter.ToSingle(prog, index);
                index += 4;

                line.strut1Position = System.BitConverter.ToSingle(prog, index);
                index += 4;

                line.strut2Position = System.BitConverter.ToSingle(prog, index);
                index += 4;

                line.strut3Position = System.BitConverter.ToSingle(prog, index);
                index += 4;

                line.strut4Position = System.BitConverter.ToSingle(prog, index);
                index += 4;

                line.strut5Position = System.BitConverter.ToSingle(prog, index);
                index += 4;

                program.Add(line);
            }

            return true;
        }

        /// <summary>
        /// Parse log and present in program format
        /// </summary>
        /// <param name="logLst"></param>
        /// <param name="program"></param>
        /// <returns></returns>
        public bool Log2Program(List<string> logLst, out List<LOG_LINE> program)
        {
            string[] log = logLst.ToArray();
            int strut;
            float[] initialValues = { 0, 0, 0, 0, 0, 0 };
            bool initialLoaded = false;
            float[] tmpStrutValues = { 0, 0, 0, 0, 0, 0 };
            //float[] tmpStrutMinCurrents = { 0, 0, 0, 0, 0, 0 };
            float[] tmpStrutMaxCurrents = { 0, 0, 0, 0, 0, 0 };
            float[] strutValues = { 0, 0, 0, 0, 0, 0 };
            string[] parts;
            float encoder2mm = 20864;
            DateTime linetime;
            DateTime blockStart = DateTime.MinValue;
            string[] formats = { "M/dd/yy HH:mm:ss", "M/dd/yyyy HH:mm:ss", "dd/MM/yy HH:mm:ss", "dd/MM/yyyy HH:mm:ss" };
            program = new List<LOG_LINE>();

            for (int i = 0; i < log.Length; i++)
            {
                string line = log[i];
                parts = line.Split(',');
                if (!DateTime.TryParseExact(parts[0], "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out linetime))
                {
                    MessageBox.Show("Bad time format: " + line);
                    return false;
                }

                if (line.Contains("Initial Position"))
                {
                    if (parts.Length == 7)
                    {
                        parts = line.Split(':');
                        parts = parts[3].Split(',');
                        if (!initialLoaded)
                        {
                            for (int j = 0; j < 6; j++)
                                strutValues[j] = initialValues[j] = float.Parse(parts[j].Trim()) / 1000;

                            initialLoaded = true;
                        }
                        else
                        {
                            for (int j = 0; j < 6; j++)
                                if (initialValues[j] != float.Parse(parts[j].Trim()) / 1000)
                                {
                                    MessageBox.Show("Error - log contains 2 different treatments");
                                    return false;
                                }
                        }
                    }
                }

                if (line.Contains("Starting treatment"))
                {
                    // Close current block and open a new one
                    bool firstBlock = true;
                    for (int j = 0; j < tmpStrutValues.Length; j++)
                    {
                        if (tmpStrutValues[j] != 0)
                        {
                            firstBlock = false;
                            break;
                        }
                    }
                    if (!firstBlock)
                    {
                        strutValues[0] += (float)tmpStrutValues[0] / encoder2mm;
                        strutValues[1] += (float)tmpStrutValues[1] / encoder2mm;
                        strutValues[2] += (float)tmpStrutValues[2] / encoder2mm;
                        strutValues[3] += (float)tmpStrutValues[3] / encoder2mm;
                        strutValues[4] += (float)tmpStrutValues[4] / encoder2mm;
                        strutValues[5] += (float)tmpStrutValues[5] / encoder2mm;

                        LOG_LINE pl = new LOG_LINE();
                        pl.unixTime = DeviceUtils.DateTime2UnixTime(blockStart);
                        pl.strut0Position = (float)strutValues[0];
                        pl.strut1Position = (float)strutValues[1];
                        pl.strut2Position = (float)strutValues[2];
                        pl.strut3Position = (float)strutValues[3];
                        pl.strut4Position = (float)strutValues[4];
                        pl.strut5Position = (float)strutValues[5];
                        pl.strut0Current = (float)tmpStrutMaxCurrents[0];
                        pl.strut1Current = (float)tmpStrutMaxCurrents[1];
                        pl.strut2Current = (float)tmpStrutMaxCurrents[2];
                        pl.strut3Current = (float)tmpStrutMaxCurrents[3];
                        pl.strut4Current = (float)tmpStrutMaxCurrents[4];
                        pl.strut5Current = (float)tmpStrutMaxCurrents[5];
                        program.Add(pl);
                    }
                    blockStart = linetime;
                }

                if (line.Contains("Strut=") && i + 1 < log.Length)
                {
                    parts = log[i].Split('=');
                    if (parts.Length != 4)
                        continue;

                    strut = int.Parse(parts[1].Substring(0, 1));
                    string[] nextlineparts = log[++i].Split('=');
                    if (nextlineparts.Length != 4)
                        continue;

                    char[] trimchars = { '[', ']' };
                    string[] currentparts = nextlineparts[3].Trim(trimchars).Split(',');
                    //tmpStrutMinCurrents[strut] = float.Parse(currentparts[0]);
                    tmpStrutMaxCurrents[strut] = float.Parse(currentparts[1]);
                    nextlineparts = nextlineparts[1].Split(',');
                    tmpStrutValues[strut] = float.Parse(nextlineparts[0].Trim());
                }
            }

            //if (!firstBlock)
            {
                strutValues[0] += (float)tmpStrutValues[0] / encoder2mm;
                strutValues[1] += (float)tmpStrutValues[1] / encoder2mm;
                strutValues[2] += (float)tmpStrutValues[2] / encoder2mm;
                strutValues[3] += (float)tmpStrutValues[3] / encoder2mm;
                strutValues[4] += (float)tmpStrutValues[4] / encoder2mm;
                strutValues[5] += (float)tmpStrutValues[5] / encoder2mm;

                LOG_LINE pl = new LOG_LINE();
                pl.unixTime = DeviceUtils.DateTime2UnixTime(blockStart);
                pl.strut0Position = (float)strutValues[0];
                pl.strut1Position = (float)strutValues[1];
                pl.strut2Position = (float)strutValues[2];
                pl.strut3Position = (float)strutValues[3];
                pl.strut4Position = (float)strutValues[4];
                pl.strut5Position = (float)strutValues[5];
                pl.strut0Current = (float)tmpStrutMaxCurrents[0];
                pl.strut1Current = (float)tmpStrutMaxCurrents[1];
                pl.strut2Current = (float)tmpStrutMaxCurrents[2];
                pl.strut3Current = (float)tmpStrutMaxCurrents[3];
                pl.strut4Current = (float)tmpStrutMaxCurrents[4];
                pl.strut5Current = (float)tmpStrutMaxCurrents[5];
                program.Add(pl);
            }

            return true;
        }

        public bool DownloadLog(TARGET_DEVICE device, out List<string> log)
        {
            log = new List<string>();

            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.GET_LOG_INFO;
            outMsg.header.dataSize = 0;
            if (!SendReceive(outMsg, out inMsg))
                return false;

            List<byte[]> pages = new List<byte[]>();
            int logPages = (int)inMsg.header.msgData;
            for (int i = 0; i < logPages; i++)
            {
                outMsg = new HOST2BOARD();
                outMsg.header.separator = SEPARATOR;
                outMsg.header.device = device;
                outMsg.header.msgCode = COMMAND.DOWNLOAD_LOG_PAGE;
                outMsg.header.msgData = i;
                outMsg.header.dataSize = 0;
                if (!SendReceive(outMsg, out inMsg))
                    return false;

                pages.Add(new byte[inMsg.header.dataSize]);
                Array.Copy(inMsg.data, 0, pages[i], 0, inMsg.header.dataSize);
            }

            byte[] allPages = new byte[pages.Count * 512];
            byte[] structPointer = new byte[512];
            log = new List<string>();
            for (int i = 0; i < pages.Count; i++)
            {
                Array.Copy(pages[i], 0, allPages, i * 512, 512);
            }

            int offset = 0;
            LOG_RECORD record;
            for (; ; )
            {
                Array.Copy(allPages, offset, structPointer, 0, 9);
                record = ByteArray2LogRecord(structPointer);

                if (record.header != 0x23242526 || record.size < 9 || record.size > 200)
                {
                    // skip current line and look for next line
                    for (int ofst = 0; ofst < 512; ofst++)
                    {
                        if (offset + ofst >= allPages.Length)
                            break;

                        if ((allPages[offset + ofst] == 0x26) &&
                            (allPages[offset + ofst + 1] == 0x25) &&
                            (allPages[offset + ofst + 1] == 0x24) &&
                            (allPages[offset + ofst + 1] == 0x23))
                        {
                            offset += ofst;
                            //break;
                        }
                    }
                    break; // continue;
                }

                DateTime? dt = DeviceUtils.UnixTime2DateTime(record.timestamp);
                string line = string.Format("{0}/{1}/{2} {3}:{4}:{5},", dt.Value.Month.ToString("00"), dt.Value.Day.ToString("00"), dt.Value.Year.ToString("0000"), dt.Value.Hour.ToString("00"), dt.Value.Minute.ToString("00"), dt.Value.Second.ToString("00"));
                byte[] tmp = new byte[record.size - 9];
                if (allPages.Length < (offset + record.size))
                    break;

                Array.Copy(allPages, offset + 9, tmp, 0, record.size - 9);
                for (int i = 0; i < tmp.Length; i++)
                {
                    if (tmp[i] == 0xFF)
                        tmp[i] = 0x20;
                }
                line += Encoding.ASCII.GetString(tmp);
                offset += record.size;
                log.Add(line);
            }

            return true;
        }

        public bool ResetLog(TARGET_DEVICE device)
        {
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.RESET_LOG;
            outMsg.header.dataSize = 0;
            return SendReceive(outMsg, out inMsg);
        }

        public bool RunProgram(TARGET_DEVICE device)
        {
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.RUN_PROGRAM;
            outMsg.header.dataSize = 0;
            return SendReceive(outMsg, out inMsg, 40000);
        }

        public bool GetProgramStatus(TARGET_DEVICE device, out uint status)
        {
            status = 0;

            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.GET_PROGRAM_STATUS;
            outMsg.header.dataSize = 0;
            if (!SendReceive(outMsg, out inMsg))
                return false;

            status = (uint)outMsg.header.msgData;
            return true;
        }

        public bool RunMotorTest(TARGET_DEVICE device, uint cycles)
        {
            BOARD2HOST inMsg;
            HOST2BOARD outMsg = new HOST2BOARD();
            outMsg.header.separator = SEPARATOR;
            outMsg.header.device = device;
            outMsg.header.msgCode = COMMAND.RUN_MOTOR_TEST;
            outMsg.header.dataSize = 0;
            if (!SendReceive(outMsg, out inMsg, TIMEOUT * 3))
                return false;

            return true;
        }

        unsafe public bool SendReceive(HOST2BOARD outMsg, out BOARD2HOST inMsg, int timeout_ms = TIMEOUT, bool readReply = true, int RWDelay = 100)
        {
            inMsg = new BOARD2HOST();

            if (_simulation)
                return true;

            try
            {
                lock (_commLock)
                {
                    _port.ReadTimeout = timeout_ms;

                    // purge
                    _port.DiscardInBuffer();
                    _port.DiscardOutBuffer();

                    // use list for byte[] manipulation
                    List<byte> msg = new List<byte>();

                    byte[] arrHeader = Header2ByteArray(outMsg.header);
                    msg.AddRange(arrHeader);
                    if (outMsg.data != null)
                        msg.AddRange(outMsg.data);

                    ushort[] dataforcrc = new ushort[(int)Math.Round((double)(msg.Count / 2))];
                    Buffer.BlockCopy(msg.ToArray(), 0, dataforcrc, 0, msg.Count);
                    ushort crc = DeviceUtils.CalculateCRC(dataforcrc, 0, dataforcrc.Length - 1);
                    msg.Add((byte)crc); // lsb
                    msg.Add((byte)((crc >> 8) & 0xFF));
                    _port.Write(msg.ToArray(), 0, msg.Count);

                    Thread.Sleep(RWDelay);

                    msg = new List<byte>();

                    if (readReply)
                    {
                        // Read reply header to find out what is the message size
                        arrHeader = new byte[Marshal.SizeOf(inMsg.header) + 2];
                        try
                        {
                            _port.Read(arrHeader, 0, arrHeader.Length);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            return false;
                        }
                        msg.AddRange(arrHeader);
                        inMsg.header = ByteArray2Header(arrHeader);
                        inMsg.crc = (ushort)((ushort)(arrHeader[arrHeader.Length - 1] << 8) + (ushort)arrHeader[arrHeader.Length - 2]);

                        // Read data and crc
                        byte[] arrData;
                        if (inMsg.header.dataSize > 0)
                        {
                            arrData = new byte[inMsg.header.dataSize];
                            try
                            {
                                _port.Read(arrData, 0, arrData.Length);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                                return false;
                            }
                            msg.AddRange(arrData);
                            inMsg.crc = (ushort)((ushort)(msg[msg.Count - 1] << 8) + (ushort)msg[msg.Count - 2]);
                        }

                        // crc
                        dataforcrc = new ushort[msg.Count / 2];
                        Buffer.BlockCopy(msg.ToArray(), 0, dataforcrc, 0, msg.Count);
                        crc = DeviceUtils.CalculateCRC(dataforcrc, 0, dataforcrc.Length - 2);

                        //                       Console.WriteLine(string.Format("{0} completed", outMsg.header.msgCode));

                        if (crc != inMsg.crc)
                            return false;

                        if (inMsg.header.device != outMsg.header.device)
                            return false;

                        if (inMsg.header.msgCode != outMsg.header.msgCode)
                            return false;

                        if (inMsg.header.dataSize > 0)
                        {
                            inMsg.data = new byte[inMsg.header.dataSize];
                            Buffer.BlockCopy(msg.ToArray(), Marshal.SizeOf(inMsg.header), inMsg.data, 0, inMsg.header.dataSize);
                        }
                    }
                    if (_port.ReadTimeout != TIMEOUT)
                        _port.ReadTimeout = TIMEOUT;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

            return true;
        }

        #region Utility Functions

        unsafe public string Echo(string echoValue)
        {
            if (_simulation)
                return "";

            lock (_commLock)
            {
                echoValue = echoValue + "\r\n";
                byte[] arr = Encoding.ASCII.GetBytes(echoValue);
                _port.Write(arr, 0, arr.Length);
                Thread.Sleep(100);

                _port.Read(arr, 0, 4);
                return System.Text.Encoding.UTF8.GetString(arr);
            }
        }

        unsafe public byte[] Config2ByteArray(EMBEDDED_CONFIG config)
        {
            int size = Marshal.SizeOf(config);
            byte[] arr = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(config, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);

            return arr;
        }

        unsafe public byte[] Header2ByteArray(MSG_HDR header)
        {
            int size = Marshal.SizeOf(header);
            byte[] arr = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(header, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);

            return arr;
        }

        unsafe public MSG_HDR ByteArray2Header(byte[] arr)
        {
            GCHandle pinnedPacket = GCHandle.Alloc(arr, GCHandleType.Pinned);
            MSG_HDR header = (MSG_HDR)Marshal.PtrToStructure(pinnedPacket.AddrOfPinnedObject(),
                                                                 typeof(MSG_HDR));
            pinnedPacket.Free();
            return header;
        }

        unsafe public PROGRAM_INFO ByteArray2ProgramInfo(byte[] arr)
        {
            GCHandle pinnedPacket = GCHandle.Alloc(arr, GCHandleType.Pinned);
            PROGRAM_INFO pinfo = (PROGRAM_INFO)Marshal.PtrToStructure(pinnedPacket.AddrOfPinnedObject(),
                                                                 typeof(PROGRAM_INFO));
            pinnedPacket.Free();
            return pinfo;
        }

        unsafe public LOG_RECORD ByteArray2LogRecord(byte[] arr)
        {
            GCHandle pinnedPacket = GCHandle.Alloc(arr, GCHandleType.Pinned);
            LOG_RECORD pinfo = (LOG_RECORD)Marshal.PtrToStructure(pinnedPacket.AddrOfPinnedObject(),
                                                                 typeof(LOG_RECORD));
            pinnedPacket.Free();
            return pinfo;
        }

        unsafe public byte[] ProgramInfoByteArray(PROGRAM_INFO pinfo)
        {
            int size = Marshal.SizeOf(pinfo);
            byte[] arr = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(pinfo, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);

            return arr;
        }

        unsafe public EMBEDDED_CONFIG ByteArray2Config(byte[] arr)
        {
            GCHandle pinnedPacket = GCHandle.Alloc(arr, GCHandleType.Pinned);
            EMBEDDED_CONFIG pinfo = (EMBEDDED_CONFIG)Marshal.PtrToStructure(pinnedPacket.AddrOfPinnedObject(),
                                                                 typeof(EMBEDDED_CONFIG));
            pinnedPacket.Free();
            return pinfo;
        }
        #endregion Utility Functions

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_port != null)
                    {
                        if (_port.IsOpen)
                            _port.Close();

                        _port.Dispose();

                        _technicianMode = false;
                    }

                    GC.SuppressFinalize(this);
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Comm() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}
