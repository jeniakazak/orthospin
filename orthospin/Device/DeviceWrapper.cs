﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows;

namespace orthospin.Device
{
    public sealed class DeviceWrapper : ObservableObject , IDisposable
    {
        private DeviceWrapper() 
        {
            _comm = new Comm();
        }
        private static readonly Lazy<DeviceWrapper> lazy = new Lazy<DeviceWrapper>(() => new DeviceWrapper());
        public static DeviceWrapper Instance
        {
            get
            {
                return lazy.Value;
            }
        }


        public bool Simulation
        {
            get
            {
                return (bool)_comm?._simulation;
            }
            set
            {
               
                if (_comm!=null)
                Set(ref _comm._simulation, value);

                if (value == false)
                {
                    ConnectionChangedEventArgs args = new ConnectionChangedEventArgs();
                    Connected = false;
                    TechnitianMode = false;
                    args.Connected = Connected;
                    args.TimeReached = DateTime.Now;
                    args.TechnitianMode = TechnitianMode;
                    OnConnectionChanged(args);
                }

            }
        }

        private Comm _comm;

        private bool _connected = false;
        public bool Connected
        {
            get
            {
                return _connected;
            }
            set
            {
                Set(ref _connected, value);
            }
        }

        private bool _technitianMode = false;
        public bool TechnitianMode
        {
            get
            {
                return _technitianMode;
            }
            set
            {
                Set(ref _technitianMode, value);
            }
        }

        private bool _busy = false;
        public bool Busy
        {
            get
            {
                return _busy;
            }
            set
            {
                Set(ref _busy, value);
            }
        }


        public void Connect()
        {
            string commPort = MainWindow._settings.com;
            Thread _connectionThread = new Thread(ConnectionThread);
            _connectionThread.Start(commPort);
        }

        public void EnterTechnicianMode()
        {
            string commPort = MainWindow._settings.com;
            Thread _technicianModeThread = new Thread(TechnicianModeThread);
            _technicianModeThread.Start(commPort);
        }

        TimeSpan ConnectionTime = TimeSpan.Zero;
        TimeSpan TechnicianModeTime = TimeSpan.Zero;

        private void ConnectionThread(object obj)
        {
            Busy = true;
            ConnectionChangedEventArgs args = new ConnectionChangedEventArgs();

            try
            {
                string commPort = (string)obj;
                if (!_comm.Connected)
                {
                    if (!_comm.Connect(commPort))
                    {
                        Connected = false;
                        TechnitianMode = false;
                        Busy = false;
                        args.Connected = Connected;
                        args.TimeReached = DateTime.Now;
                        args.TechnitianMode = TechnitianMode;
                        OnConnectionChanged(args);
                        ConnectionTime = TimeSpan.Zero;
                        return;
                    }
                }

                Busy = false;
            }
            catch (Exception ex)
            {
                Busy = false;
                Connected = false;
                TechnitianMode = false;
                args.Connected = Connected;
                args.TimeReached = DateTime.Now;
                args.TechnitianMode = TechnitianMode;
                OnConnectionChanged(args);
                ConnectionTime = TimeSpan.Zero;
                _comm?.ExitTechnicianMode();
                MessageBox.Show(ex.Message);
            }
        }


        int abortConnectionTime = 60 * 60;

        private void TechnicianModeThread(object obj)
        {
            Busy = true;
            ConnectionChangedEventArgs args = new ConnectionChangedEventArgs();

            try
            {
               
                _comm.EnterTechnicianMode(TARGET_DEVICE.MASTER_MAIN);

                while (true)
                {
                    if (_comm.TechnicianMode)
                    {
                        ResetConfiguration(false);
                        Connected = true;
                        TechnitianMode = true;
                        Busy = false;
                        args.Connected = Connected;
                        args.TimeReached = DateTime.Now;
                        args.TechnitianMode = TechnitianMode;
                        OnConnectionChanged(args);
                        TechnicianModeTime = TimeSpan.Zero;
                        break;
                    }

                    Thread.Sleep(200);

                    TechnicianModeTime += TimeSpan.FromMilliseconds(200);

                    if (TechnicianModeTime.TotalSeconds > abortConnectionTime)
                    {
                        throw new Exception("Technician mode timeout");
                    }
                }

                _comm.ExitTechnicianMode();
            }
            catch (Exception ex)
            {
                Busy = false;
                Connected = false;
                TechnitianMode = false;
                args.Connected = Connected;
                args.TimeReached = DateTime.Now;
                args.TechnitianMode = TechnitianMode;
                OnConnectionChanged(args);
                TechnicianModeTime = TimeSpan.Zero;
                _comm?.ExitTechnicianMode();
                MessageBox.Show(ex.Message);
            }
        }


        public bool UploadProgram(TARGET_DEVICE device, List<PROGRAM_LINE> program)
        {
            return  _comm.UploadProgram(device, program);
        }

        public bool DownloadProgram(TARGET_DEVICE device, out List<PROGRAM_LINE> program)
        {
            return _comm.DownloadProgram(device, out program);
        }

        public bool DownloadLog(TARGET_DEVICE device, out List<string> logs)
        {
            return _comm.DownloadLog(device, out logs);
        }

        public bool SetTime(TARGET_DEVICE device, DateTime time)
        {
             return _comm.SetTime(device, time);
        }

        public bool GetTime(TARGET_DEVICE device, out DateTime? time)
        {
            return _comm.GetTime(device, out time);
        }

        public bool ResetConfiguration(bool resetSteps)
        {
            if (_comm._simulation)
                return true;

            bool OK = false;

            EMBEDDED_CONFIG config = new EMBEDDED_CONFIG
            {
                version = 100,
                milimeter2encoder = (uint)MainWindow._settings.encoder2mm,
                encoderStepsPerSecond = 20000,
                fallTimeMiliSecond = 2000,
                P = 0.01f,
                I = 0.001f,
                D = 0,
                treatmentDelaySeconds = 6,
                systemStatus = 1,
                lowBatteryThresh = 6.5f,
                maxMotorCurrent = 4000,
                maxPWM = 99,
                lastProgramStep = 0,
                safetyLevel = 0
            };

            EMBEDDED_CONFIG deviceConfig;
            OK = false;
            for (int i = 0; i < 5; i++)
            {
                Thread.Sleep(500);
                _comm.UploadConfiguration(TARGET_DEVICE.MASTER_MAIN, config);
                Thread.Sleep(500);
                _comm.DownloadConfiguration(TARGET_DEVICE.MASTER_MAIN, out deviceConfig);
                if (DeviceUtils.ConfigurationsAreEqual(config, deviceConfig))
                {
                    OK = true;
                    break;
                }
            }
            if (!OK)
            {
                return OK;
            }

            OK = false;
            for (int i = 0; i < 5; i++)
            {
                Thread.Sleep(500);
                _comm.UploadConfiguration(TARGET_DEVICE.SUPERVISOR_MAIN, config);
                Thread.Sleep(500);
                _comm.DownloadConfiguration(TARGET_DEVICE.SUPERVISOR_MAIN, out deviceConfig);
                if (DeviceUtils.ConfigurationsAreEqual(config, deviceConfig))
                {
                    OK = true;
                    break;
                }
            }
            if (!OK)
            {
                return OK;
            }
            
            return OK;
        }

        public bool Shutdown(TARGET_DEVICE device)
        {
            return _comm.Shutdown(device);
        }

        public bool SetWakeupTime(TARGET_DEVICE device, int secondsFromNow)
        {
            return _comm.SetWakeupTime(device, secondsFromNow);
        }

        public bool SpinStrut(int strutIndex, int encoderSteps)
        {
           return _comm.SpinStrut(strutIndex, encoderSteps);
        }

        #region Events
        void OnConnectionChanged(ConnectionChangedEventArgs e)
        {
            ConnectionChanged?.Invoke(this, e);
        }

        public event ConnectionChangedEventHandler ConnectionChanged;

        public class ConnectionChangedEventArgs : EventArgs
        {
            public bool Connected { get; set; }
            public bool TechnitianMode { get; set; }
            public DateTime TimeReached { get; set; } 
        }

        public delegate void ConnectionChangedEventHandler(Object sender, ConnectionChangedEventArgs e);

        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_comm != null)
                        _comm.Dispose();

                    Connected = false;
                    TechnitianMode = false;
                    OnConnectionChanged(new ConnectionChangedEventArgs() { Connected = Connected, TimeReached = DateTime.Now , TechnitianMode = TechnitianMode });
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~DeviceWrapper() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
