﻿namespace orthospin.Consants
{
    public enum eNavigationMainPage
    {
        SplashScreen = 100,
        LoginPage = 101,
        MainMenuPage = 102,
        NewPatient =103,
        ManualMode = 104,
        LogPage = 105
    }

    public enum eNavigationNewPatientPage
    {
        Connection = 0,
        BrowseFile = 1,
        Upload = 2,
        Segmentation =3,
        AfterSegmentation = 4,
        SelectDate = 5,
        Chart = 6,


        None = 666
    }

    public enum SourceFile
    {
       none = 0,
       csv = 1, 
       pdf = 2
    }
}
