﻿using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight.Threading;
using orthospin.Utils;

namespace orthospin
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private Mutex _instanceMutex = null;

        static App()
        {
            DispatcherHelper.Initialize();

            CultureInfo en = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = en;

        }

        private static void keyUp(object sender, KeyEventArgs e)
        {
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            e.Exception.LogException();
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Helpers.ApplicationName = Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().Location);

            Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
        }

        protected override void OnStartup(StartupEventArgs e)
        {


            if (System.Environment.MachineName != "DESKTOP-21TUVCJ")
            {
                //test that only one instance of the application is running
                bool createdNew;
                _instanceMutex = new Mutex(true,
    @"4vDAS7DhUA8yi]g%rySxbG%0n0PRY3GrDDIuIsjv{QVb(+z5NDrwmu&XEhxaAf8c$)$N3M{O3sD9Y^*EpKfW(jp2-mS*Z~
[JzjUJCgI[Y2sW~zkxzrSi9k{Vu&a&gJUoA7#eqr]R@Xb!}2]x!@C%i!QaObt9y3%xsigmYzQpN2G&aw7NhU%cIrh^2zeQtd
X@ShZhUiL4ZvpQ[hZjW#NhsUPP!DG9xBJ&d]5mMJ++7fAmPmQxXCO)@r9nzAJJoUkT",
    out createdNew);
                if (!createdNew)
                {
                    MessageBox.Show("An instance of the orthospin application is already running");
                    _instanceMutex = null;
                    Current.Shutdown();
                    return;
                }
            }           

            base.OnStartup(e);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            if (_instanceMutex != null)
                _instanceMutex.ReleaseMutex();
            base.OnExit(e);
        }

    }
}
