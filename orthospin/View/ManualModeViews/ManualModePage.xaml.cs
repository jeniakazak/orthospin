﻿using System.Windows.Controls;

namespace orthospin.View.ManualModeViews
{
    /// <summary>
    /// Interaction logic for ManualModePage.xaml
    /// </summary>
    public partial class ManualModePage : Page
    {
        public ManualModePage()
        {
            InitializeComponent();
        }
    }
}
