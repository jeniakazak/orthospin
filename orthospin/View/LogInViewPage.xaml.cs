﻿using System.Windows.Controls;

namespace orthospin.View
{
    /// <summary>
    /// Interaction logic for LogInViewPage.xaml
    /// </summary>
    public partial class LogInViewPage : Page
    {
        public LogInViewPage()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            userNameTextBox.Focus();
        }
    }
}
