﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace orthospin.View.FollowViews
{
    /// <summary>
    /// Interaction logic for LogView.xaml
    /// </summary>
    public partial class LogView : Page
    {
        public LogView()
        {
            InitializeComponent();

            Loaded += new RoutedEventHandler(MainWindow_Loaded);
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            ((FrameworkElement)svContainer1.Content).SizeChanged += new SizeChangedEventHandler(content_SizeChanged);

            sbScroller.Maximum = Math.Abs(svContainer1.ExtentWidth - svContainer1.ViewportWidth);
            sbScroller.Minimum = 0;
            sbScroller.Value = sbScroller.Minimum;

        }

        void content_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            sbScroller.Maximum = Math.Abs(svContainer1.ExtentWidth - svContainer1.ViewportWidth);
            sbScroller.Minimum = 0;
            sbScroller.Value = sbScroller.Minimum;
        }

        //ScrollBar event handler
        private void ScrollBar_Scroll(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
        {
            UpdateScrollOffset(Math.Abs(sbScroller.Value));
        }

        //Updates the offset of both ScrollViewer
        public void UpdateScrollOffset(double newOffset)
        {
            svContainer1.ScrollToHorizontalOffset(newOffset);
        }

        //Event handler for all ScrollViewers
        private void Grid_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
          //  UpdateScrollOffset(e.HorizontalOffset);
        }

        private void SbScroller_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            UpdateScrollOffset(Math.Abs(sbScroller.Value));
        }
    }
}
