﻿using System.Windows.Controls;

namespace orthospin.View.NewPatientViews
{
    /// <summary>
    /// Interaction logic for NewPatientTreatmentChartPage.xaml
    /// </summary>
    public partial class NewPatientTreatmentChartPage : Page
    {
        public NewPatientTreatmentChartPage()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            
        }
    }
}
