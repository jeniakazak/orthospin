﻿using GalaSoft.MvvmLight.Messaging;
using orthospin.Consants;
using System;
using System.Windows.Controls;

namespace orthospin.View.NewPatientViews
{
    /// <summary>
    /// Interaction logic for NewPatientMainPage.xaml
    /// </summary>
    public partial class NewPatientMainPage : Page
    {
        private NewPatienConnectPage _patienConnectPage;
        private NewPatientBrowseFilePage _browseFilePage;
        private NewPatientUploadProgramPage _uploadProgramPage;
        private NewPatientSegmenaionPage _segmenaionPage;
        private NewPatientAfterSegmentationPage _afterSegmentationPage;
        private NewPatientSelectDatePage _selectDatePage;
        private NewPatientTreatmentChartPage _newPatientTreatmentChartPage;

        private Page currentPage;


        public NewPatientMainPage()
        {
            InitializeComponent();

            NewPatientMainFrame.Navigated += NewPatientMainFrame_Navigated;
        }

        private void NewPatientMainFrame_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            NewPatientMainFrame?.NavigationService.RemoveBackEntry();
        }

        private void OnNewPatientNavigate(eNavigationNewPatientPage navigationPage)
        {
            switch (navigationPage)
            {
                case eNavigationNewPatientPage.Connection:
                    if (_patienConnectPage == null)
                        _patienConnectPage = new NewPatienConnectPage();

                    currentPage = null;
                    currentPage = _patienConnectPage;
                    break;

                case eNavigationNewPatientPage.BrowseFile:
                    if (_browseFilePage == null)
                        _browseFilePage = new NewPatientBrowseFilePage();
                    currentPage = null;
                    currentPage = _browseFilePage;
                    break;

                case eNavigationNewPatientPage.Segmentation:
                    if (_segmenaionPage == null)
                        _segmenaionPage = new NewPatientSegmenaionPage();
                    currentPage = null;
                    currentPage = _segmenaionPage;
                    break;


                case eNavigationNewPatientPage.AfterSegmentation:
                    if (_afterSegmentationPage == null)
                        _afterSegmentationPage = new NewPatientAfterSegmentationPage();
                    currentPage = null;
                    currentPage = _afterSegmentationPage;
                    break;

                case eNavigationNewPatientPage.SelectDate:
                    if (_selectDatePage == null)
                        _selectDatePage = new NewPatientSelectDatePage();
                    currentPage = null;
                    currentPage = _selectDatePage;
                    break;

                case eNavigationNewPatientPage.Upload:
                    if (_uploadProgramPage == null)
                        _uploadProgramPage = new NewPatientUploadProgramPage();
                    currentPage = null;
                    currentPage = _uploadProgramPage;
                    break;

                case eNavigationNewPatientPage.Chart:
                        _newPatientTreatmentChartPage = new NewPatientTreatmentChartPage();
                    currentPage = null;
                    currentPage = _newPatientTreatmentChartPage;
                    break;

                default:
                    break;
            }

            if (currentPage != null)
            {
                NewPatientMainFrame.Content = currentPage;
               
            }

            try
            {
                GC.Collect();
            }
            catch
            {

            }
        }

        private void Page_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {
            Messenger.Default.Unregister<eNavigationNewPatientPage>(
            this, OnNewPatientNavigate);
        }

        private void Page_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            Messenger.Default.Register<eNavigationNewPatientPage>(
             this, OnNewPatientNavigate);

            OnNewPatientNavigate(eNavigationNewPatientPage.Connection);
        }
    }
}
