﻿using System.Windows.Controls;

namespace orthospin.View.NewPatientViews
{
    /// <summary>
    /// Interaction logic for NewPatientAfterSegmentationPage.xaml
    /// </summary>
    public partial class NewPatientAfterSegmentationPage : Page
    {
        public NewPatientAfterSegmentationPage()
        {
            InitializeComponent();
        }
    }
}
