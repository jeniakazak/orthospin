﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace orthospin.View.NewPatientViews
{
    /// <summary>
    /// Interaction logic for NewPatientBrowseFilePage.xaml
    /// </summary>
    public partial class NewPatientBrowseFilePage : Page
    {
        public NewPatientBrowseFilePage()
        {
            InitializeComponent();
        }

        private void DropBox_Drop(object sender, DragEventArgs e)
        {

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            }

            var DropGrid = sender as Grid;
            DropGrid.Background = new SolidColorBrush(Color.FromArgb(28, 225, 225, 225));
        }

        private void DropBox_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effects = DragDropEffects.Copy;
                var DropGrid = sender as Grid;
                DropGrid.Background = new SolidColorBrush(Color.FromRgb(32, 220, 223));
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        private void DropBox_DragLeave(object sender, DragEventArgs e)
        {
            var DropGrid = sender as Grid;
            DropGrid.Background = new SolidColorBrush(Color.FromArgb(28,225, 225, 225));
        }

    }
}
