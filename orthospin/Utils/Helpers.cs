﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;

namespace orthospin.Utils
{
    public static class Helpers
    {
        public static string ApplicationName { get; set; }

        public static double[] Divide(double number, int div)
        {
            double res = 0;
            if (number != 0)
            {
                res = Math.Round(number / (double)div, 3);

                if (Math.Abs(res) < MainWindow._settings.minResolution)// min resolution
                    throw new Exception("Number of segments too large.");
            }

            List<double> resultArr = new List<double>();
            double temp = 0.0;

            for (int i = 0; i < div; i++)
            {
                if (i < (div - 1))
                    resultArr.Add(res);
                else
                {
                    temp = (Math.Round(number - resultArr.Sum(), 3));
                    resultArr.Add(temp);
                }
            }

            return resultArr.ToArray();
        }

        private static object _CanExecuteLock = new object();

        public static void RaiseCanExecuteChangedAsync(object relayCommand)
        {
            try
            {
                lock (_CanExecuteLock)
                {
                    RelayCommand relay = null;

                    RelayCommand<object> relayObj = null;

                    if (relayCommand.GetType() == typeof(RelayCommand))
                    {
                        relay = relayCommand as RelayCommand;
                    }
                    else if (relayCommand.GetType() == typeof(RelayCommand<object>))
                    {
                        relayObj = relayCommand as RelayCommand<object>;
                    }

                    Application.Current?.Dispatcher?.Invoke(() =>
                   {
                       relay?.RaiseCanExecuteChanged();

                       relayObj?.RaiseCanExecuteChanged();
                   });
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        public static string GetAppVersion()
        {
            try
            {
                return "Version : 0.1.0.18 build : " + Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
            catch 
            {
                return string.Empty;
            }
        }

        public static void LogException(this Exception ex, string details = null)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(GetDateTimeNowWithMS());
            sb.AppendLine();
            sb.AppendLine(details);
            sb.AppendLine(ex.ToString());
            sb.AppendLine("***************************************************************");
            sb.AppendLine();
            if (!Directory.Exists($@"c:\{ApplicationName}_Log"))
                Directory.CreateDirectory($@"c:\{ApplicationName}_Log");
            File.AppendAllText($@"c:\{ApplicationName}_Log\{ApplicationName}ErrorLog.txt", sb.ToString());
        }

        public static string GetDateTimeNowWithMS()
        {
            return DateTime.Now.ToString("HH:mm:ss.fff");
        }

        public static double Truncate(this double value, int digits)
        {
            double mult = Math.Pow(10.0, digits);
            double result = Math.Truncate(mult * value) / mult;
            return result;
        }

        public static float Truncate(this float value, int digits)
        {
            double mult = Math.Pow(10.0, digits);
            float result = (float)(Math.Truncate(mult * value) / mult);
            return result;
        }

        public static bool HasOnlyOne(this string text, char character)
        {
            int count = 0;
            foreach (var ch in text)
            {
                if (ch == character)
                {
                    if (count == 1)
                        return false;

                    count++;
                }
            }

            return count == 1;
        }

    }
}
