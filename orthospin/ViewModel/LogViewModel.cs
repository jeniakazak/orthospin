﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using orthospin.Consants;
using orthospin.Device;
using orthospin.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace orthospin.ViewModel
{
    public class LogViewModel : ViewModelBase
    {
        Dispatcher dispatcher = Application.Current.Dispatcher;
        bool WaitToLoad = false;
        private bool _busy = false;
        public bool Busy
        {
            get
            {
                return _busy;
            }
            set
            {
                Set(ref _busy, value);
                Utils.Helpers.RaiseCanExecuteChangedAsync(BackToMainMenuCommand);
                Utils.Helpers.RaiseCanExecuteChangedAsync(LoadLogsCommand);
            }
        }

        #region BackToMainMenuCommand
        public ICommand BackToMainMenuCommand
        {
            get { return _BackToMainMenuCommand ?? (_BackToMainMenuCommand = new RelayCommand<object>(BackToMainMenu, CanBackToMainMenu)); }
            set
            {
                Set(ref _BackToMainMenuCommand, value);
            }
        }
        private ICommand _BackToMainMenuCommand;

        private void BackToMainMenu(object obj)
        {
            try
            {
                dispatcher.Invoke(() =>
                {
                    Cleanup();
                    Messenger.Default.Send(eNavigationMainPage.MainMenuPage);

                });
            }
            catch (TaskCanceledException tex)
            {
                Debug.WriteLine(tex);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool CanBackToMainMenu(object obj)
        {
            return !Busy;
        }
        #endregion

        #region LoadLogsCommand
        public ICommand LoadLogsCommand
        {
            get { return _LoadLogsCommand ?? (_LoadLogsCommand = new RelayCommand<object>(BeginLoadLogs, CanLoadLogs)); }
            set
            {
                Set(ref _LoadLogsCommand, value);
            }
        }
        private ICommand _LoadLogsCommand;

        private void BeginLoadLogs(object obj)
        {
            try
            {
                if (!_commWrapper.Connected)
                {
                    WaitToLoad = true;
                    _commWrapper.Connect();
                }
                else
                {
                    WaitToLoad = false;
                    LoadLogs();
                }
            }catch
            {
                MessageBox.Show("Load logs error "); 
            }
        }

        private void LoadLogs()
        {
            List<string> logList = new List<string>();
            CommWrapper.DownloadLog(TARGET_DEVICE.MASTER_MAIN, out logList);

            List<LOG_LINE> programList = new List<LOG_LINE>();
            DeviceUtils.Log2Program(logList, out programList);

            dispatcher.Invoke(new Action(() => 
            {
                itemsList.Clear();
                foreach (LOG_LINE pr in programList)
                {
                    itemsList.Add(new LOG_LINE_PROGRAMM()
                    {
                        ExecuteTime = pr.unixTime,
                        Strut1Movement = pr.strut0Current,
                        Strut2Movement = pr.strut1Current,
                        Strut3Movement = pr.strut2Current,
                        Strut4Movement = pr.strut3Current,
                        Strut5Movement = pr.strut4Current,
                        Strut6Movement = pr.strut5Current,

                        Strut1Position = pr.strut0Position,
                        Strut2Position = pr.strut1Position,
                        Strut3Position = pr.strut2Position,
                        Strut4Position = pr.strut3Position,
                        Strut5Position = pr.strut4Position,
                        Strut6Position = pr.strut5Position,

                    });
                }
            }));
        }

        private bool CanLoadLogs(object obj)
        {
            return !Busy;
        }
        #endregion


        DeviceWrapper _commWrapper;
        public DeviceWrapper CommWrapper
        {
            get
            {
                return _commWrapper;
            }
            set
            {
                Set(ref _commWrapper, value);
            }
        }

        public LogViewModel()
        {

            CommWrapper = DeviceWrapper.Instance;
            CommWrapper.ConnectionChanged += CommWrapper_ConnectionChanged;
        }

        private void CommWrapper_ConnectionChanged(object sender, DeviceWrapper.ConnectionChangedEventArgs e)
        {
            if (e.Connected)
            {
                if (WaitToLoad)
                {
                    LoadLogs();
                }
            }

            //if (!e.Connected)
               // MessageBox.Show("Disconnected");
        }

        public ObservableCollection<LOG_LINE_PROGRAMM> itemsList { get; set; } = new ObservableCollection<LOG_LINE_PROGRAMM>();

        public class TodoItem
        {
            public string Title { get; set; }
        }
    }
}
