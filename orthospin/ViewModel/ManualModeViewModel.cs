﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using orthospin.Consants;
using orthospin.Device;
using orthospin.Model;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace orthospin.ViewModel
{
    public class ManualModeViewModel : ViewModelBase
    {
        Dispatcher dispatcher = Application.Current.Dispatcher;
        private readonly IDataService _dataService;
        string[] separator = new string[] { "," };

        DeviceWrapper _commWrapper;
        public DeviceWrapper CommWrapper
        {
            get
            {
                return _commWrapper;
            }
            set
            {
                Set(ref _commWrapper, value);
            }
        }


        public ManualPrepare Prepare1 { get; set; } = new ManualPrepare() { MotorId = 1, ReadyToMove = false };
        public ManualPrepare Prepare2 { get; set; } = new ManualPrepare() { MotorId = 2, ReadyToMove = false };
        public ManualPrepare Prepare3 { get; set; } = new ManualPrepare() { MotorId = 3, ReadyToMove = false };
        public ManualPrepare Prepare4 { get; set; } = new ManualPrepare() { MotorId = 4, ReadyToMove = false };
        public ManualPrepare Prepare5 { get; set; } = new ManualPrepare() { MotorId = 5, ReadyToMove = false };
        public ManualPrepare Prepare6 { get; set; } = new ManualPrepare() { MotorId = 6, ReadyToMove = false };


        public ManualModeViewModel(IDataService dataService)
        {
            _dataService = dataService;
            CommWrapper = DeviceWrapper.Instance;
            CommWrapper.ConnectionChanged += CommWrapper_ConnectionChanged;
        }

        private void CommWrapper_ConnectionChanged(object sender, DeviceWrapper.ConnectionChangedEventArgs e)
        {
            if (e.Connected)
                MessageBox.Show("Connected");

            if (!e.Connected)
                MessageBox.Show("Disconnected");
        }

        #region PrepareMovementCommand

        public ICommand PrepareMovementCommand
        {
            get { return _PrepareMovementCommand ?? (_PrepareMovementCommand = new RelayCommand<object>(PrepareMovement)); }
            set
            {
                Set(ref _PrepareMovementCommand, value);
            }
        }
        private ICommand _PrepareMovementCommand;

        private void PrepareMovement(object obj)
        {
            if (obj == null)
                return;
            string param = obj as string;

            string[] paramarr = param.Split(separator, StringSplitOptions.RemoveEmptyEntries);

            int id = int.Parse(paramarr[1]);

            int MoveType = 1;
            if (paramarr[0] == "short")
            {
                MoveType = -1;
            }
            else if (paramarr[0] == "long")
            {
                MoveType = 1;
            }

            switch (id)
            {
                case 1:
                    Prepare1.MoveType = MoveType;
                    Prepare1.ReadyToMove = true;
                    break;
                case 2:
                    Prepare2.MoveType = MoveType;
                    Prepare2.ReadyToMove = true;
                    break;
                case 3:
                    Prepare3.MoveType = MoveType;
                    Prepare3.ReadyToMove = true;
                    break;
                case 4:
                    Prepare4.MoveType = MoveType;
                    Prepare4.ReadyToMove = true;
                    break;
                case 5:
                    Prepare5.MoveType = MoveType;
                    Prepare5.ReadyToMove = true;
                    break;
                case 6:
                    Prepare6.MoveType = MoveType;
                    Prepare6.ReadyToMove = true;
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region MovemeCommand

        public ICommand MovemeCommand
        {
            get { return _MovemeCommand ?? (_MovemeCommand = new RelayCommand<object>(Move)); }
            set
            {
                Set(ref _MovemeCommand, value);
            }
        }
        private ICommand _MovemeCommand;

        private void Move(object obj)
        {
            if (obj == null)
                return;

            string param = obj as string;

            string[] paramarr = param.Split(separator, StringSplitOptions.RemoveEmptyEntries);

            int id = int.Parse(paramarr[1]);

            bool CanMove = false;
            if (paramarr[0] == "yes")
            {
                CanMove = true;
            }

            if (!CommWrapper.Connected)
            {
                CommWrapper.Connect();
                return;
            }

            int MoveType = 1;

            switch (id)
            {
                case 1:
                    Prepare1.ReadyToMove = false;
                    MoveType = Prepare1.MoveType;
                    break;
                case 2:
                    Prepare2.ReadyToMove = false;
                    MoveType = Prepare2.MoveType;
                    break;
                case 3:
                    Prepare3.ReadyToMove = false;
                    MoveType = Prepare3.MoveType;
                    break;
                case 4:
                    Prepare4.ReadyToMove = false;
                    MoveType = Prepare4.MoveType;
                    break;
                case 5:
                    Prepare5.ReadyToMove = false;
                    MoveType = Prepare5.MoveType;
                    break;
                case 6:
                    Prepare6.ReadyToMove = false;
                    MoveType = Prepare6.MoveType;
                    break;
                default:
                    break;
            }

            if (CanMove)
            {
                double encoderSteps = (double)MainWindow._settings.step * ((double)MoveType);
                encoderSteps *= MainWindow._settings.encoder2mm;
                CommWrapper.SpinStrut(DeviceUtils.GetStrutFromStrutIndex(id), (int)encoderSteps);
            }
        }
        #endregion

        #region BackToMainMenuCommand
        public ICommand BackToMainMenuCommand
        {
            get { return _BackToMainMenuCommand ?? (_BackToMainMenuCommand = new RelayCommand<object>(BackToMainMenu)); }
            set
            {
                Set(ref _BackToMainMenuCommand, value);
            }
        }
        private ICommand _BackToMainMenuCommand;

        private void BackToMainMenu(object obj)
        {
            bool approve = false;
            bool.TryParse(obj?.ToString(), out approve);
            if (approve)
            {
            }

            dispatcher.Invoke(() =>
            {
                Cleanup();
                Messenger.Default.Send(eNavigationMainPage.MainMenuPage);

            });
        }
        #endregion


        public override void Cleanup()
        {
            if (CommWrapper != null)
            {
                CommWrapper.ConnectionChanged -= CommWrapper_ConnectionChanged;
            }

            base.Cleanup();
        }

    }
}
