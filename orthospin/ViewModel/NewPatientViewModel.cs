﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Events;
using LiveCharts.Geared;
using Microsoft.Win32;
using orthospin.Consants;
using orthospin.Device;
using orthospin.Model;
using orthospin.Utils;
using PDFParser;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace orthospin.ViewModel
{
    public class NewPatientViewModel : ViewModelBase
    {
        #region private fields
        private TimeSpan _startTreatmentTime = new TimeSpan (0,0,0);
        private TimeSpan _stopTreatmentTime = new TimeSpan(23, 59, 59);
        private DeviceWrapper _commWrapper;
        private Dispatcher dispatcher = Application.Current.Dispatcher;
        private int _selectedSegmentation = 1;
        private StateModel _selectedState;
        private VerifyConnectionTask _selectedTask;
        private string _buttonText = "OK";
        private bool _segmentationPicked = false;
        private int _segmentationFinished = 0;
        private string _pathToFile = string.Empty;
        private bool _busy = false;
        private int _uploadingProgress;
        private bool _uploadingSuccess = false;
        private bool _treatmentStarted = false;
        private bool _dateTimePicked = false;
        private DateTime _selectedDate = DateTime.Now;
        private string _selectedDateText;
        private DispatcherTimer dayTimer;
        private bool loaded = false;
        private bool _enabledOnErrors = true;
        private bool _enabledOnTreatmentDateErrors = true;
        private OpenFileDialog openFileDialog;
        private string _selectedSegmentationText;
        private SourceFile source = SourceFile.none;
        private IChartValues strut1Values = new GearedValues<DateTimePoint>();
        private IChartValues strut2Values = new GearedValues<DateTimePoint>();
        private IChartValues strut3Values = new GearedValues<DateTimePoint>();
        private IChartValues strut4Values = new GearedValues<DateTimePoint>();
        private IChartValues strut5Values = new GearedValues<DateTimePoint>();
        private IChartValues strut6Values = new GearedValues<DateTimePoint>();


        private string patienName = null;
        private DateTime? programGenerated = null;
        #endregion

        #region public fields
        public bool EnabledOnTreatmentDateErrors
        {
            get
            {
                return _enabledOnTreatmentDateErrors;
            }
            set
            {
                Set(ref _enabledOnTreatmentDateErrors, value);
                Utils.Helpers.RaiseCanExecuteChangedAsync(SelectSegmentationCommand);
            }
        }
        public int SelectedSegmentation
        {
            get
            {
                return _selectedSegmentation;
            }
            set
            {
                Set(ref _selectedSegmentation, value);
                SegmentationPicked = false;
            }
        }
        public string SelectedSegmentationText
        {
            get
            {
                return _selectedSegmentationText;
            }
            set
            {
                Set(ref _selectedSegmentationText, value);
                SegmentationPicked = false;
            }
        }
        public string ButtonText
        {
            get
            {
                return _buttonText;
            }
            set
            {
                Set(ref _buttonText, value);
            }
        }
        public bool SegmentationPicked
        {
            get
            {
                return _segmentationPicked;
            }
            set
            {
                Set(ref _segmentationPicked, value);
            }
        }
        public int SegmentationFinished
        {
            get
            {
                return _segmentationFinished;
            }
            set
            {
                Set(ref _segmentationFinished, value);
            }
        }
        public string PathToFile
        {
            get
            {
                return _pathToFile;
            }
            set
            {
                if (Set(ref _pathToFile, value))
                {
                    NewFileProcess();
                }
            }
        }
        public int UploadingProgress
        {
            get
            {
                return _uploadingProgress;
            }
            set
            {
                Set(ref _uploadingProgress, value);
            }
        }
        public bool Busy
        {
            get
            {
                return _busy;
            }
            set
            {
                Set(ref _busy, value);
                Utils.Helpers.RaiseCanExecuteChangedAsync(BackCommand);
                Utils.Helpers.RaiseCanExecuteChangedAsync(SelectSegmentationCommand);
                Utils.Helpers.RaiseCanExecuteChangedAsync(StartTreatmentCommand);

            }
        }
        public bool UploadingSuccess
        {
            get
            {
                return _uploadingSuccess;
            }
            set
            {
                Set(ref _uploadingSuccess, value);
                Utils.Helpers.RaiseCanExecuteChangedAsync(StartTreatmentCommand);

            }
        }
        public bool TreatmentStarted
        {
            get
            {
                return _treatmentStarted;
            }
            set
            {
                Set(ref _treatmentStarted, value);
            }
        }
        public bool DateTimePicked
        {
            get
            {
                return _dateTimePicked;
            }
            set
            {
                Set(ref _dateTimePicked, value);
            }
        }
        public string SelectedDateText
        {
            get
            {
                return _selectedDateText;
            }
            set
            {
                Set(ref _selectedDateText, value);
            }
        }
        public bool EnabledOnErrors
        {
            get
            {
                return _enabledOnErrors;
            }
            set
            {
                Set(ref _enabledOnErrors, value);
            }
        }
        public TimeSpan StartTreatmentTime
        {

            get
            {
                return _startTreatmentTime;
            }
            set
            {
                Set(ref _startTreatmentTime, value);
            }
        }
        public TimeSpan StopTreatmentTime
        {

            get
            {
                return _stopTreatmentTime;
            }
            set
            {
                Set(ref _stopTreatmentTime, value);
            }
        }
        public DateTime SelectedDate
        {
            get
            {
                return _selectedDate;
            }
            set
            {
                Set(ref _selectedDate, value);

            }
        }
        public DeviceWrapper CommWrapper
        {
            get
            {
                return _commWrapper;
            }
            set
            {
                Set(ref _commWrapper, value);
            }
        }
        public StateModel SelectedState
        {
            get
            {
                return _selectedState;
            }
            set
            {
                Set(ref _selectedState, value);
            }
        }
        public VerifyConnectionTask SelectedTask
        {
            get
            {
                return _selectedTask;
            }
            set
            {
                Set(ref _selectedTask, value);
            }
        }
        public string PatienName
        {
            get
            {
                return patienName;
            }
            set
            {
                Set(ref patienName, value);
            }
        }
        public DateTime? ProgramGenerated
        {
            get
            {
                return programGenerated;
            }
            set
            {
                Set(ref programGenerated, value);
            }
        }



        public ObservableCollection<VerifyConnectionTask> TaskCollection { get; set; } = new ObservableCollection<VerifyConnectionTask>();
        public ObservableCollection<PatientProgram> PatientProgramCollection { get; set; } = new ObservableCollection<PatientProgram>();
        public ObservableCollection<PatientProgram> SegmentedProgramCollection { get; set; } = new ObservableCollection<PatientProgram>();
        public ObservableCollection<int> SegmentationNumbersList { get; set; } = new ObservableCollection<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        public ObservableCollection<StateModel> StateCollection { get; set; } = new ObservableCollection<StateModel>();
        public ObservableCollection<ValidationError> Errorslist { get; set; } = new ObservableCollection<ValidationError>();
        public ObservableCollection<ValidationError> TreatmentStartEndDateErrorslist { get; set; } = new ObservableCollection<ValidationError>();


        #endregion

        #region Commands 

        private ICommand _StartTreatmentCommand;
        private ICommand _ConnectionPageLoaded;
        private ICommand _EditableDate_Error;
        private ICommand _TreatmentDate_Error;
        private ICommand _ExecuteTaskCommand;
        private ICommand _OpenFileDialogCommand;
        private ICommand _ApproveProgramCommand;
        private ICommand _PeakDateCommand;
        private ICommand _ApproveDateCommand;
        private ICommand _BackCommand;
        private ICommand _SelectSegmentationCommand;
        private ICommand _UploadProgramCommand;
        private ICommand _PreviewDropCommand;
        private ICommand _ApproveProgramWithSegmentationCommand;
        private ICommand _ApproveSegmentationCommand;
        private ICommand _ApproveChartCommand;
        private ICommand _ChangeAxisRangeCommand;
        private ICommand _BackToMainMenuCommand;

        public ICommand StartTreatmentCommand
        {
            get { return _StartTreatmentCommand ?? 
                    (_StartTreatmentCommand = new RelayCommand(StartTreatment , CanStartTreatment));
            }
        }
        public ICommand ConnectionPageLoaded
        {
            get{ return _ConnectionPageLoaded ?? 
                    (_ConnectionPageLoaded = new RelayCommand(OnConnectionPageLoaded));
            }
        }
        public ICommand EditableDate_Error
        {
            get{ return _EditableDate_Error ?? 
                    (_EditableDate_Error = new RelayCommand<RoutedEventArgs>(param => OnSelectedProgramDateError(param)));
            }
        }
        public ICommand TreatmentDate_Error
        {
            get{ return _TreatmentDate_Error ??
                   ( _TreatmentDate_Error = new RelayCommand<RoutedEventArgs>(param => OnTreatmentSEDateError(param)));
            }
        }
        public ICommand ExecuteTaskCommand
        {
            get { return _ExecuteTaskCommand ?? 
                    (_ExecuteTaskCommand = new RelayCommand(ExecuteTask));
            }
        }
        public ICommand OpenFileDialogCommand
        {
            get { return _OpenFileDialogCommand ?? 
                    (_OpenFileDialogCommand = new RelayCommand(OpenFileDialog));
            }
        }
        public ICommand ApproveProgramCommand
        {
            get { return _ApproveProgramCommand ?? 
                    (_ApproveProgramCommand = new RelayCommand(ApproveProgram));
            }
        }
        public ICommand PreviewDropCommand
        {
            get { return _PreviewDropCommand ?? 
                    (_PreviewDropCommand = new RelayCommand<object>(HandlePreviewDrop));
            }
        }
        public ICommand UploadProgramCommand
        {
            get {
                return _UploadProgramCommand ?? 
                    (_UploadProgramCommand = new RelayCommand(UploadProgram));
            }
        }
        public ICommand SelectSegmentationCommand
        {
            get { return _SelectSegmentationCommand ?? 
                    (_SelectSegmentationCommand = new RelayCommand(SelectSegmentation, CanSelectSegmentation));
            }
           
        }
        public ICommand BackCommand
        {
            get { return _BackCommand ?? 
                    (_BackCommand = new RelayCommand<object>(Back, CanBack));
            }
        }
        public ICommand ApproveDateCommand
        {
            get { return _ApproveDateCommand ?? 
                    (_ApproveDateCommand = new RelayCommand<object>(ApproveDate));
            }
        }
        public ICommand PeakDateCommand
        {
            get { return _PeakDateCommand ?? 
                    (_PeakDateCommand = new RelayCommand(PeakDate));
            }
        }
        public ICommand ApproveProgramWithSegmentationCommand
        {
            get { return _ApproveProgramWithSegmentationCommand ?? 
                    (_ApproveProgramWithSegmentationCommand = new RelayCommand(ApproveProgramWithSegmentation));
            }
        }
        public ICommand ApproveSegmentationCommand
        {
            get { return _ApproveSegmentationCommand ?? 
                    (_ApproveSegmentationCommand = new RelayCommand<object>(ApproveSegmentation));
            }
        }
        public ICommand ApproveChartCommand
        {
            get
            {
                return _ApproveChartCommand ??
                  (_ApproveChartCommand = new RelayCommand(ApproveChart));
            }
        }
        public ICommand ChangeAxisRangeCommand
        {
            get
            {
                return _ChangeAxisRangeCommand ??
                  (_ChangeAxisRangeCommand = new RelayCommand<RangeChangedEventArgs>(Axis_OnRangeChanged));
            }
        }
        public ICommand BackToMainMenuCommand
        {
            get
            {
                return _BackToMainMenuCommand ??
                  (_BackToMainMenuCommand = new RelayCommand(BackToMainMenu));
            }
        }

        #endregion

        #region chart  



        public IChartValues Strut1Values
        {
            get
            {
                return strut1Values;
            }
            set
            {
                Set(ref strut1Values, value);
            }
        }
       public IChartValues Strut2Values
        {
            get
            {
                return strut2Values;
            }
            set
            {
                Set(ref strut2Values, value);
            }
        }
        public IChartValues Strut3Values
        {
            get
            {
                return strut3Values;
            }
            set
            {
                Set(ref strut3Values, value);
            }
        }
        public IChartValues Strut4Values
        {
            get
            {
                return strut4Values;
            }
            set
            {
                Set(ref strut4Values, value);
            }
        }
        public IChartValues Strut5Values
        {
            get
            {
                return strut5Values;
            }
            set
            {
                Set(ref strut5Values, value);
            }
        }
        public IChartValues Strut6Values
        {
            get
            {
                return strut6Values;
            }
            set
            {
                Set(ref strut6Values, value);
            }
        }



        private Func<double, string> _x_formatter;
        private Func<double, string> _y_formatter;
        private double _from;
        private double _to;

        private double _maxRange;
        private double _minRange;

        public Func<double, string> X_Formatter
        {
            get { return _x_formatter; }
            set
            {
                Set(ref _x_formatter, value);
            }
        }

        public Func<double, string> Y_Formatter
        {
            get { return _y_formatter; }
            set
            {
                Set(ref _y_formatter, value);
            }
        }
        public double From
        {
            get { return _from; }
            set
            {
                Set(ref _from, value);
            }
        }
        public double To
        {
            get { return _to; }
            set
            {
                Set(ref _to, value);
            }
        }

        public double MaxRange
        {
            get { return _maxRange; }
            set
            {
                Set(ref _maxRange, value);
            }
        }
        public double MinRange
        {
            get { return _minRange; }
            set
            {
                Set(ref _minRange, value);
            }
        }

        private void Axis_OnRangeChanged(RangeChangedEventArgs eventargs)
        {

            var currentRange = eventargs.Range;

            if (currentRange < TimeSpan.TicksPerDay * 2)
            {
                X_Formatter = x => new DateTime((long)x).ToString("t");
                return;
            }

            if (currentRange < TimeSpan.TicksPerDay * 60)
            {
                X_Formatter = x => new DateTime((long)x).ToString("dd MMM yy");
                return;
            }

            if (currentRange < TimeSpan.TicksPerDay * 540)
            {
                X_Formatter = x => new DateTime((long)x).ToString("MMM yy");
                return;
            }

            X_Formatter = x => new DateTime((long)x).ToString("yyyy");
        }
        #endregion

        #region On Event 

        private void dayTimer_Tick(object sender, EventArgs e)
        {
            SelectedDate = new DateTime(DateTime.Now.Ticks);
        }

        private void OnConnectionPageLoaded()
        {
            Cleanup();

            LoadAction();
        }

        private void CommWrapper_ConnectionChanged(object sender, DeviceWrapper.ConnectionChangedEventArgs e)
        {
            if (SelectedTask?.ID == 2 && e.Connected && e.TechnitianMode)
            {
                try
                {
                    dispatcher.Invoke(() =>
                    {
                        Messenger.Default.Send(eNavigationNewPatientPage.BrowseFile);
                        SelectedState = StateCollection[2];
                    });
                }
                catch (TaskCanceledException tex)
                {
                    Debug.WriteLine(tex);
                }
            }
            else if (!e.Connected)
            {
                SelectedTask = TaskCollection.Where(x => x.ID == 0).FirstOrDefault();
                ButtonText = "OK";
            }
        }

        private void OnSelectedProgramDateError(RoutedEventArgs obj)
        {
            ValidationErrorEventArgs ErrorEventobj = obj as ValidationErrorEventArgs;

            if (ErrorEventobj.Action == ValidationErrorEventAction.Added)
            {
                Errorslist.Add(ErrorEventobj.Error);
            }
            else if (ErrorEventobj.Action == ValidationErrorEventAction.Removed)
            {
                Errorslist.Remove(ErrorEventobj.Error);
            }

            if (Errorslist.Count > 0)
            {
                EnabledOnErrors = false;
            }
            else
            {
                EnabledOnErrors = true;
            }
        }

        private void OnTreatmentSEDateError(RoutedEventArgs obj)
        {
            ValidationErrorEventArgs ErrorEventobj = obj as ValidationErrorEventArgs;

            if (ErrorEventobj.Action == ValidationErrorEventAction.Added)
            {
                TreatmentStartEndDateErrorslist.Add(ErrorEventobj.Error);
            }
            else if (ErrorEventobj.Action == ValidationErrorEventAction.Removed)
            {
                TreatmentStartEndDateErrorslist.Remove(ErrorEventobj.Error);
            }

            if (TreatmentStartEndDateErrorslist.Count > 0)
            {
                EnabledOnTreatmentDateErrors = false;
            }
            else
            {
                EnabledOnTreatmentDateErrors = true;
            }
        }

        #endregion

        public NewPatientViewModel()
        {
            if (dayTimer == null ? true : !dayTimer.IsEnabled)
            {
                dayTimer = new DispatcherTimer();
                dayTimer.Interval = TimeSpan.FromMilliseconds(1000);
                dayTimer.Tick += new EventHandler(dayTimer_Tick);
                dayTimer.Start();
            }

            LoadAction();
        }

        #region private methods
        private void ReloadChart()
        {
            var Strut1Movement = new List<DateTimePoint>();
            var Strut2Movement = new List<DateTimePoint>();
            var Strut3Movement = new List<DateTimePoint>();
            var Strut4Movement = new List<DateTimePoint>();
            var Strut5Movement = new List<DateTimePoint>();
            var Strut6Movement = new List<DateTimePoint>();

            DateTime ExecuteDate, ExecuteTime, dateTime;
            DateTime minDate = DateTime.MaxValue;
            DateTime maxDate = DateTime.MinValue;


            Strut1Values?.Clear();
            Strut2Values?.Clear();
            Strut3Values?.Clear();
            Strut4Values?.Clear();
            Strut5Values?.Clear();
            Strut6Values?.Clear();

            foreach (PatientProgram patientProgram in SegmentedProgramCollection)
            {

                ExecuteDate = (DateTime)DeviceUtils.UnixTime2DateTime(patientProgram.ExecuteDate);

                ExecuteTime = (DateTime)DeviceUtils.UnixTime2DateTime(patientProgram.ExecuteTime);

                dateTime = new DateTime(ExecuteDate.Year, ExecuteDate.Month, ExecuteDate.Day, ExecuteTime.Hour, ExecuteTime.Minute, ExecuteTime.Second);

                if (dateTime < minDate)
                    minDate = dateTime;
                if (dateTime > maxDate)
                    maxDate = dateTime;

                Strut1Movement.Add(new DateTimePoint(dateTime, patientProgram.Strut1Movement));
                Strut2Movement.Add(new DateTimePoint(dateTime, patientProgram.Strut2Movement));
                Strut3Movement.Add(new DateTimePoint(dateTime, patientProgram.Strut3Movement));
                Strut4Movement.Add(new DateTimePoint(dateTime, patientProgram.Strut4Movement));
                Strut5Movement.Add(new DateTimePoint(dateTime, patientProgram.Strut5Movement));
                Strut6Movement.Add(new DateTimePoint(dateTime, patientProgram.Strut6Movement));

            }

            Strut1Values.AddRange(Strut1Movement.AsGearedValues().WithQuality(Quality.Medium));
            Strut2Values.AddRange(Strut2Movement.AsGearedValues().WithQuality(Quality.Medium));
            Strut3Values.AddRange(Strut3Movement.AsGearedValues().WithQuality(Quality.Medium));
            Strut4Values.AddRange(Strut4Movement.AsGearedValues().WithQuality(Quality.Medium));
            Strut5Values.AddRange(Strut5Movement.AsGearedValues().WithQuality(Quality.Medium));
            Strut6Values.AddRange(Strut6Movement.AsGearedValues().WithQuality(Quality.Medium));

            X_Formatter = x => new DateTime((long)x).ToString("dd/MM/yyyy HH:mm");

            Y_Formatter = y => Helpers.Truncate(y, 3).ToString();

            From = minDate.Ticks;

            To = maxDate.Ticks;

            MaxRange = (To - From) * 2;

            MinRange = TimeSpan.TicksPerHour / 2;
        }

        private void StartTreatment()
        {
            try
            {
                Busy = true;

                CommWrapper.SetWakeupTime(TARGET_DEVICE.MASTER_MAIN, 30);
                CommWrapper.Shutdown(TARGET_DEVICE.MASTER_MAIN);
                CommWrapper.Dispose();

                TreatmentStarted = true;

            }
            catch 
            {
                TreatmentStarted = false;
            }
            finally
            {
                Busy = false;
            }
        }

        private bool CanStartTreatment()
        {
            return !Busy && UploadingSuccess;
        }

        private void SaveProgramm()
        {
            try
            {

                DateTime CurrentTime = DateTime.Now;

                string delimeter = ",";

                string PathToUserDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Orthospin\" +
                    CurrentTime.Year + "." + CurrentTime.Month + "." + CurrentTime.Day;

                string DocPath = PathToUserDirectory + @"\treatment.csv";

                if (!Directory.Exists(PathToUserDirectory))
                {
                    Directory.CreateDirectory(PathToUserDirectory);
                }

                int fCountdocx = Directory.GetFiles(PathToUserDirectory, "*.csv", SearchOption.AllDirectories).Length;

                if (fCountdocx != 0)
                    DocPath = PathToUserDirectory + @"\treatment" + " (" + fCountdocx + ")" + ".csv";


                if (SegmentedProgramCollection?.Count > 0)
                {

                    using (StreamWriter sw = File.CreateText(DocPath))
                    {
                        int count = 0;
                        foreach (PatientProgram pr in SegmentedProgramCollection)
                        {

                            StringBuilder builder = new StringBuilder();
                            DateTime? ExecuteTime = DeviceUtils.UnixTime2DateTime(pr.ExecuteTime);
                            DateTime? ExecuteDate = DeviceUtils.UnixTime2DateTime(pr.ExecuteDate);

                            DateTime programmDate = new DateTime(ExecuteDate.Value.Year, ExecuteDate.Value.Month, ExecuteDate.Value.Day,
                               ExecuteTime.Value.Hour, ExecuteTime.Value.Minute, ExecuteTime.Value.Second);


                            builder.Append(count);
                            builder.Append(delimeter);
                            builder.Append(programmDate.Day);
                            builder.Append(delimeter);
                            builder.Append(programmDate.Month);
                            builder.Append(delimeter);
                            builder.Append(programmDate.Year);
                            builder.Append(delimeter);
                            builder.Append(programmDate.Hour);
                            builder.Append(delimeter);
                            builder.Append(programmDate.Minute);
                            builder.Append(delimeter);
                            builder.Append(programmDate.Second);
                            builder.Append(delimeter);
                            builder.Append(pr.Strut1Movement);
                            builder.Append(delimeter);
                            builder.Append(pr.Strut2Movement);
                            builder.Append(delimeter);
                            builder.Append(pr.Strut3Movement);
                            builder.Append(delimeter);
                            builder.Append(pr.Strut4Movement);
                            builder.Append(delimeter);
                            builder.Append(pr.Strut5Movement);
                            builder.Append(delimeter);
                            builder.Append(pr.Strut6Movement);

                            sw.WriteLine(builder.ToString());

                            count++;
                        }
                    }
                }
            }
            catch
            {
                throw new Exception("Save file error");
            }
        }

        private void LoadAction()
        {

            if (!loaded)
            {
                loaded = true;

                CommWrapper = DeviceWrapper.Instance;

                CommWrapper.ConnectionChanged += CommWrapper_ConnectionChanged;

                TaskCollection.Add(new VerifyConnectionTask() { ID = 0, Displayext = "① Connect the system to patient" });
                TaskCollection.Add(new VerifyConnectionTask() { ID = 1, Displayext = "② Connect the system to PC USB port" });
                TaskCollection.Add(new VerifyConnectionTask() { ID = 2, Displayext = "③ Press wake up BUTTON" });

                SelectedTask = TaskCollection[0];

                StateCollection.Add(new StateModel { Title = "NEW PATIENT", index = 0 });
                StateCollection.Add(new StateModel { Title = "Verify connection", index = 1 });
                StateCollection.Add(new StateModel { Title = "BROWSE", index = 2 });
                StateCollection.Add(new StateModel { Title = "Approve patient program", index = 3 });
                StateCollection.Add(new StateModel { Title = "Approve Current date & time", index = 4 });
                StateCollection.Add(new StateModel { Title = "Upload Program", index = 5 });
                StateCollection.Add(new StateModel { Title = "Start Program", index = 6 });

                SelectedState = StateCollection[1];
            }
        }

        private void Connect()
        {
            CommWrapper.Connect();
        }

        private void EnterTechnicianMode()
        {
            CommWrapper.EnterTechnicianMode();
        }

        private void NewFileProcess()
        {
            string path = PathToFile?.ToLower();

            if (string.IsNullOrWhiteSpace(path))
                return;

          

            if (Path.GetExtension(path).Contains("csv"))
            {

                try
                {
                    PatientProgramCollection.Clear();
                    PatienName = null;
                    ProgramGenerated = null;
                    SegmentationFinished = 0;

                    char[] delimiters = new char[] { ',', ' ', '\t', '#' };
                    string[] lines = File.ReadAllLines(PathToFile);
                    uint _count = 0;

                    foreach (string line in lines)
                    {
                        string[] parts = line.Split(delimiters);
                        if (parts == null || parts.Length == 0)
                            continue;

                        int test;
                        if (!int.TryParse(parts[0], out test))
                            continue;


                        DateTime dt = new DateTime(int.Parse(parts[3]), int.Parse(parts[2]), int.Parse(parts[1]), int.Parse(parts[4]), int.Parse(parts[5]), int.Parse(parts[6]));


                        PatientProgramCollection.Add(new PatientProgram()
                        {
                            Count = _count,
                            ExecuteTime = DeviceUtils.DateTime2UnixTime(dt),
                            ExecuteDate = DeviceUtils.DateTime2UnixTime(dt),
                            Strut1Movement = float.Parse(parts[7]),
                            Strut2Movement = float.Parse(parts[8]),
                            Strut3Movement = float.Parse(parts[9]),
                            Strut4Movement = float.Parse(parts[10]),
                            Strut5Movement = float.Parse(parts[11]),
                            Strut6Movement = float.Parse(parts[12]),
                        });

                        _count++;

                    }

                    source = SourceFile.csv;
                }
                catch (IOException ioe)
                {
                    PatientProgramCollection.Clear();

                    SegmentationFinished = 0;

                    source = SourceFile.none;

                    MessageBox.Show(ioe.Message);
                }
                catch 
                {
                    PatientProgramCollection.Clear();

                    SegmentationFinished = 0;

                    source = SourceFile.none;

                    MessageBox.Show("Read csv error");
                }

            }
            else if (Path.GetExtension(path).Contains("pdf"))
            {
                try
                {

                    PatientProgramCollection.Clear();
                    PatienName = null;
                    ProgramGenerated = null;
                    SegmentationFinished = 0;

                    PatientData patientData = ParsePdf.ReadPdfFile(PathToFile);

                    PatienName = patientData.PatienName;
                    ProgramGenerated = patientData.GeneratedDate;


                    foreach (PatientTable patientTable in patientData.patientTables)
                    {

                        int limit = patientTable.TableValues.Count / 6;

                        for (int i = 0; i < limit; i++)
                        {
                            PatientProgramCollection.Add(new PatientProgram()
                            {
                                Count = patientTable.count,
                                ExecuteTime = DeviceUtils.DateTime2UnixTime(patientTable.RunTime),
                                ExecuteDate = DeviceUtils.DateTime2UnixTime(patientTable.RunTime),
                                Strut1Movement = patientTable.TableValues[i],
                                Strut2Movement = patientTable.TableValues[(i + 1 * limit)],
                                Strut3Movement = patientTable.TableValues[(i + 2 * limit)],
                                Strut4Movement = patientTable.TableValues[(i + 3 * limit)],
                                Strut5Movement = patientTable.TableValues[(i + 4 * limit)],
                                Strut6Movement = patientTable.TableValues[(i + 5 * limit)],
                            });
                        }
                    }


                    if (PatientProgramCollection.Count > 1)
                    {
                        PatientProgramCollection[0].ExecuteTime = PatientProgramCollection[1].ExecuteTime;
                        PatientProgramCollection[0].ExecuteDate = PatientProgramCollection[1].ExecuteDate;
                    }

                    source = SourceFile.pdf;

                }
                catch (IOException ioe)
                {
                    PatientProgramCollection.Clear();

                    SegmentationFinished = 0;

                    PatienName = null;

                    ProgramGenerated = null;

                    source = SourceFile.none;

                    MessageBox.Show(ioe.Message);
                }
                catch (Exception ex)
                {
                    PatientProgramCollection.Clear();

                    SegmentationFinished = 0;

                    PatienName = null;

                    ProgramGenerated = null;

                    source = SourceFile.none;

                    MessageBox.Show(ex.Message);
                }
            }


            if (PatientProgramCollection.Count > 0)
                SelectedState = StateCollection[3];

            PathToFile = string.Empty;

        }

        private void ExecuteTask()
        {

            if (SelectedTask == null)
                return;

            int id = SelectedTask.ID;



            switch (SelectedTask.ID)
            {
                case 0:
                    break;
                case 1:
                    ButtonText = "NEXT";
                    Connect();
                    break;
                case 2:
                    EnterTechnicianMode();
                    break;
                default:
                    break;
            }


            if (id < TaskCollection.Count - 1)
                SelectedTask = TaskCollection[++id];
            else if (id == 2 && CommWrapper.Connected && CommWrapper.TechnitianMode)
            {
                try
                {
                    dispatcher.Invoke(() =>
                    {
                        Messenger.Default.Send(eNavigationNewPatientPage.BrowseFile);

                        SelectedState = StateCollection[2];
                    });
                }
                catch (TaskCanceledException tex)
                {
                    Debug.WriteLine(tex);
                }
            }
            else
            {
                ButtonText = "OK";
            }
        }

        private void HandlePreviewDrop(object inObject)
        {
            IDataObject ido = inObject as IDataObject;
            if (null == ido) return;

            if (ido.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])ido.GetData(DataFormats.FileDrop);
                PathToFile = files[0];
            }
        }
        
        private void OpenFileDialog()
        {
            openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "pdf (*.pdf;)|*.pdf;|csv (*.csv;)|*.csv;"; //"pdf (*.pdf;)|*.pdf; |"|csv (*.csv;)|*.csv;
            openFileDialog.Multiselect = false;

            if ((bool)openFileDialog.ShowDialog())
            {
                var result = openFileDialog.FileName;

                if (result.Length < 1)
                {
                    return;
                }

                PathToFile = result;
            }
        }

        private void ApproveProgram()
        {
            if (PatientProgramCollection.Count > 0)
            {
                try
                {
                    dispatcher.Invoke(() =>
                    {
                        Messenger.Default.Send(eNavigationNewPatientPage.Segmentation);
                    });
                }
                catch (TaskCanceledException tex)
                {
                    Debug.WriteLine(tex);
                }
            }
        }

        private void UploadProgram()
        {
            Thread thread = new Thread(UploadProgramExecute);
            thread.Start();
        }

        private void UploadProgramExecute()
        {

            Busy = true;
            bool OK = false;
            UploadingSuccess = false;

            try
            {
                UploadingProgress++;

                List<PROGRAM_LINE> program = new List<PROGRAM_LINE>();
                List<PROGRAM_LINE> deviceProgram = new List<PROGRAM_LINE>();
                foreach (PatientProgram prog in SegmentedProgramCollection)
                {
                    DateTime? ExecuteTime = DeviceUtils.UnixTime2DateTime(prog.ExecuteTime);
                    DateTime? ExecuteDate = DeviceUtils.UnixTime2DateTime(prog.ExecuteDate);

                    DateTime programDate = new DateTime(ExecuteDate.Value.Year, ExecuteDate.Value.Month, ExecuteDate.Value.Day,
                        ExecuteTime.Value.Hour, ExecuteTime.Value.Minute, ExecuteTime.Value.Second);

                    program.Add(new PROGRAM_LINE(
                       DeviceUtils.DateTime2UnixTime(programDate),
                      (float)prog.Strut1Movement,
                       prog.Strut2Movement,
                       prog.Strut3Movement,
                       prog.Strut4Movement,
                       prog.Strut5Movement,
                       prog.Strut6Movement
                    ));
                }

                for (int i = UploadingProgress; UploadingProgress <= 23; UploadingProgress++)
                {
                    Thread.Sleep(166);
                }

                if (_commWrapper.Simulation)
                {
                    for (int i = UploadingProgress; UploadingProgress <= 92; UploadingProgress++)
                    {
                        Thread.Sleep(22);
                    }

                    UploadingSuccess = true;

                    SelectedState = StateCollection[6];

                }
                else
                {

                    for (int i = 0; i < 5; i++)
                    {
                        Thread.Sleep(500);
                        CommWrapper.UploadProgram(TARGET_DEVICE.MASTER_MAIN, program);
                        Thread.Sleep(500);
                        CommWrapper.DownloadProgram(TARGET_DEVICE.MASTER_MAIN, out deviceProgram);
                        if (DeviceUtils.ProgramsAreEqual(program, deviceProgram))
                        {
                            OK = true;
                            break;
                        }
                    }
                    if (!OK)
                    {
                        MessageBox.Show("Master Program Upload failed");
                        return;
                    }


                    List<PROGRAM_LINE> supervisorProgram = DeviceUtils.Reduce30sec(program);

                    for (int i = UploadingProgress; UploadingProgress <= 43; UploadingProgress++)
                    {
                        Thread.Sleep(22);
                    }

                    OK = false;
                    for (int i = 0; i < 5; i++)
                    {
                        Thread.Sleep(500);
                        CommWrapper.UploadProgram(TARGET_DEVICE.SUPERVISOR_MAIN, supervisorProgram);
                        Thread.Sleep(500);
                        CommWrapper.DownloadProgram(TARGET_DEVICE.SUPERVISOR_MAIN, out deviceProgram);
                        if (DeviceUtils.ProgramsAreEqual(supervisorProgram, deviceProgram))
                        {
                            OK = true;
                            break;
                        }
                    }
                    if (!OK)
                    {
                        MessageBox.Show("Supervisor Program Upload failed");
                        return;
                    }

                    for (int i = UploadingProgress; UploadingProgress <= 63; UploadingProgress++)
                    {
                        Thread.Sleep(22);
                    }


                    DateTime startTime = DateTime.Now;
                    DateTime? deviceTime;
                    OK = false;
                    for (int i = 0; i < 5; i++)
                    {
                        Thread.Sleep(500);
                        startTime = DateTime.Now;
                        CommWrapper.SetTime(TARGET_DEVICE.MASTER_MAIN, startTime);
                        Thread.Sleep(500);
                        CommWrapper.GetTime(TARGET_DEVICE.MASTER_MAIN, out deviceTime);
                        uint localunixtime = DeviceUtils.DateTime2UnixTime(DateTime.Now);
                        uint deviceunixtime = DeviceUtils.DateTime2UnixTime(deviceTime);
                        if (Math.Abs((int)localunixtime - (int)localunixtime) < 3)
                        {
                            OK = true;
                            break;
                        }
                    }
                    if (!OK)
                    {
                        MessageBox.Show("Master time set failed");
                        return;
                    }


                    OK = false;
                    for (int i = 0; i < 5; i++)
                    {
                        Thread.Sleep(500);
                        startTime = DateTime.Now;
                        CommWrapper.SetTime(TARGET_DEVICE.SUPERVISOR_MAIN, startTime);
                        Thread.Sleep(500);
                        CommWrapper.GetTime(TARGET_DEVICE.SUPERVISOR_MAIN, out deviceTime);
                        uint localunixtime = DeviceUtils.DateTime2UnixTime(DateTime.Now);
                        uint deviceunixtime = DeviceUtils.DateTime2UnixTime(deviceTime);
                        if (Math.Abs((int)localunixtime - (int)localunixtime) < 3)
                        {
                            OK = true;
                            break;
                        }
                    }
                    if (!OK)
                    {
                        MessageBox.Show("Master time set failed");
                        return;
                    }

                    for (int i = UploadingProgress; UploadingProgress <= 83; UploadingProgress++)
                    {
                        Thread.Sleep(22);
                    }

                    CommWrapper.ResetConfiguration(true);

                    for (int i = UploadingProgress; UploadingProgress <= 92; UploadingProgress++)
                    {
                        Thread.Sleep(22);
                    }

                    UploadingSuccess = true;

                    SelectedState = StateCollection[6];
                }

                SaveProgramm();

                for (int i = UploadingProgress; UploadingProgress <= 100; UploadingProgress++)
                {
                    Thread.Sleep(22);
                }
            }
            catch (Exception ex)
            {
                UploadingProgress = 0;
                UploadingSuccess = false;
                Debug.WriteLine(ex);

                MessageBox.Show("Upload failed " + Environment.NewLine + ex.Message);
            }
            finally
            {
                Busy = false;
            }

        }

        private void SelectSegmentation()
        {
            int segmentationNumber = 1;

            if (int.TryParse(SelectedSegmentationText, out segmentationNumber))
            {
                if (!SegmentationNumbersList.Contains(segmentationNumber))
                {
                    SegmentationNumbersList.Add(segmentationNumber);
                    SelectedSegmentation = segmentationNumber;
                }
                else
                {
                    SelectedSegmentation = segmentationNumber;
                }

                SegmentationPicked = true;
            }
            else
            {
                MessageBox.Show("Please enter a positive integer.");
                SegmentationPicked = false;
            }
        }

        private bool CanSelectSegmentation()
        {
            return !Busy && EnabledOnTreatmentDateErrors;
        }

        private void ApproveSegmentation(object obj)
        {
            bool approve = false;
            bool.TryParse(obj?.ToString(), out approve);
            if (!approve)
            {
                SegmentationPicked = false;
                return;
            }

            try
            {

                SegmentedProgramCollection.Clear();
                foreach (PatientProgram patientProgram in PatientProgramCollection)
                {
                    SegmentedProgramCollection.Add((PatientProgram)patientProgram.Clone());
                }

                if (SelectedSegmentation <= 1 && source == SourceFile.csv)
                {
                    SegmentationPicked = false;
                    SegmentationFinished = 1;

                }
                else
                {


                    int count = PatientProgramCollection == null ? 0 : PatientProgramCollection.Count;
                    ObservableCollection<PatientProgram> tempCollection = new ObservableCollection<PatientProgram>();

                    if (count < 1)
                    {
                        MessageBox.Show("Please select programm file.");
                        dispatcher.Invoke(() =>
                        {
                            Messenger.Default.Send(eNavigationNewPatientPage.BrowseFile);
                            SelectedState = StateCollection[2];
                        });
                    }
                    else if (count == 1)
                    {
                        tempCollection.Clear();
                        tempCollection = null;

                        SegmentationPicked = false;
                        SegmentationFinished = 1;
                    }
                    else if (count > 1)
                    {

                        PatientProgram zeroProgramm = PatientProgramCollection[0];

                        DateTime? zeroProgrammExecuteTime = DeviceUtils.UnixTime2DateTime(zeroProgramm.ExecuteTime);
                        DateTime? zeroProgrammExecuteDate = DeviceUtils.UnixTime2DateTime(zeroProgramm.ExecuteDate);

                        DateTime zeroProgrammExecute = new DateTime(zeroProgrammExecuteDate.Value.Year, zeroProgrammExecuteDate.Value.Month,
                            zeroProgrammExecuteDate.Value.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

                        zeroProgramm.ExecuteTime = DeviceUtils.DateTime2UnixTime(zeroProgrammExecute);
                        zeroProgramm.ExecuteDate = DeviceUtils.DateTime2UnixTime(zeroProgrammExecute);

                        for (int k = 1; k < count; k++)
                        {

                            #region Time
                            DateTime? ExecuteTime = DeviceUtils.UnixTime2DateTime(PatientProgramCollection[k].ExecuteTime);
                            DateTime? ExecuteDate = DeviceUtils.UnixTime2DateTime(PatientProgramCollection[k].ExecuteDate);

                            DateTime programExecute = new DateTime(ExecuteDate.Value.Year, ExecuteDate.Value.Month, ExecuteDate.Value.Day,
                                StartTreatmentTime.Hours, StartTreatmentTime.Minutes, StartTreatmentTime.Seconds);


                            DateTime programExAtid = new DateTime(programExecute.Year, programExecute.Month, programExecute.Day,
                                StopTreatmentTime.Hours, StopTreatmentTime.Minutes, StopTreatmentTime.Seconds);

                            double diffInSeconds = (programExAtid - programExecute).TotalSeconds;

                            if (Math.Round(diffInSeconds, MidpointRounding.AwayFromZero) <= 0)
                            {
                                throw new Exception("End Time should be greater than Start Time");
                            }



                            int tempSegmentation = SelectedSegmentation - 1;
                            if (tempSegmentation < 1)
                                tempSegmentation = 1;

                            double interval = Math.Round(diffInSeconds / tempSegmentation);

                            if (interval < 60 * 2)
                            {
                                throw new Exception("The difference between runs must be 2 minutes or more.");
                            }

                            List<DateTime> DateTimeSegments = new List<DateTime>();

                            for (int i = 0; i < SelectedSegmentation; i++)
                            {
                                DateTimeSegments.Add(programExecute.AddSeconds(interval * i));
                            }
                            #endregion

                            #region Movement
                            double[][] MoveSegmens = new double[SelectedSegmentation][];

                            var Strut1MovementSegments = Utils.Helpers.Divide(PatientProgramCollection[k].Strut1Movement - PatientProgramCollection[k - 1].Strut1Movement, SelectedSegmentation);
                            var Strut2MovementSegments = Utils.Helpers.Divide(PatientProgramCollection[k].Strut2Movement - PatientProgramCollection[k - 1].Strut2Movement, SelectedSegmentation);
                            var Strut3MovementSegments = Utils.Helpers.Divide(PatientProgramCollection[k].Strut3Movement - PatientProgramCollection[k - 1].Strut3Movement, SelectedSegmentation);
                            var Strut4MovementSegments = Utils.Helpers.Divide(PatientProgramCollection[k].Strut4Movement - PatientProgramCollection[k - 1].Strut4Movement, SelectedSegmentation);
                            var Strut5MovementSegments = Utils.Helpers.Divide(PatientProgramCollection[k].Strut5Movement - PatientProgramCollection[k - 1].Strut5Movement, SelectedSegmentation);
                            var Strut6MovementSegments = Utils.Helpers.Divide(PatientProgramCollection[k].Strut6Movement - PatientProgramCollection[k - 1].Strut6Movement, SelectedSegmentation);

                            double Strut1Movement = 0;
                            double Strut2Movement = 0;
                            double Strut3Movement = 0;
                            double Strut4Movement = 0;
                            double Strut5Movement = 0;
                            double Strut6Movement = 0;

                            for (int i = 0; i < SelectedSegmentation; i++)
                            {
                                Strut1Movement += Strut1MovementSegments[i];
                                Strut2Movement += Strut2MovementSegments[i];
                                Strut3Movement += Strut3MovementSegments[i];
                                Strut4Movement += Strut4MovementSegments[i];
                                Strut5Movement += Strut5MovementSegments[i];
                                Strut6Movement += Strut6MovementSegments[i];

                                MoveSegmens[i] = new double[7];
                                MoveSegmens[i][0] = DeviceUtils.DateTime2UnixTime(DateTimeSegments[i]);
                                MoveSegmens[i][1] = Strut1Movement + PatientProgramCollection[k - 1].Strut1Movement;
                                MoveSegmens[i][2] = Strut2Movement + PatientProgramCollection[k - 1].Strut2Movement;
                                MoveSegmens[i][3] = Strut3Movement + PatientProgramCollection[k - 1].Strut3Movement;
                                MoveSegmens[i][4] = Strut4Movement + PatientProgramCollection[k - 1].Strut4Movement;
                                MoveSegmens[i][5] = Strut5Movement + PatientProgramCollection[k - 1].Strut5Movement;
                                MoveSegmens[i][6] = Strut6Movement + PatientProgramCollection[k - 1].Strut6Movement;
                            }

                            for (int i = 0; i < SelectedSegmentation; i++)
                            {
                                tempCollection.Add(new PatientProgram()
                                {
                                    Count = PatientProgramCollection[k].Count,
                                    ExecuteTime = (uint)MoveSegmens[i][0],
                                    ExecuteDate = (uint)MoveSegmens[i][0],
                                    Strut1Movement = (float)MoveSegmens[i][1],
                                    Strut2Movement = (float)MoveSegmens[i][2],
                                    Strut3Movement = (float)MoveSegmens[i][3],
                                    Strut4Movement = (float)MoveSegmens[i][4],
                                    Strut5Movement = (float)MoveSegmens[i][5],
                                    Strut6Movement = (float)MoveSegmens[i][6]
                                });
                            }

                            #endregion
                        }

                        SegmentedProgramCollection.Clear();
                        SegmentedProgramCollection.Add((PatientProgram)zeroProgramm.Clone());
                        foreach (PatientProgram patientProgram in tempCollection)
                        {
                            SegmentedProgramCollection.Add((PatientProgram)patientProgram.Clone());
                        }

                        tempCollection.Clear();
                        tempCollection = null;

                        SegmentationPicked = false;
                        SegmentationFinished = 1;
                    }

                }

                dispatcher.Invoke(() =>
                {
                    Messenger.Default.Send(eNavigationNewPatientPage.AfterSegmentation);
                });


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                SegmentationPicked = false;
                SegmentationFinished = 0;
            }
        }

        private void ApproveProgramWithSegmentation()
        {
            try
            {
                ReloadChart();

                dispatcher.Invoke(() =>
                {
                    Messenger.Default.Send(eNavigationNewPatientPage.Chart);
                });
            }
            catch (TaskCanceledException tex)
            {
                Debug.WriteLine(tex);
            }
        }

        private void ApproveChart()
        {
            try
            {
                dispatcher.Invoke(() =>
                {
                    Messenger.Default.Send(eNavigationNewPatientPage.SelectDate);
                    SelectedState = StateCollection[4];
                });
            }
            catch (TaskCanceledException tex)
            {
                Debug.WriteLine(tex);
            }
        }

        private void PeakDate()
        {
            DateTimePicked = true;
        }

        private void ApproveDate(object obj)
        {
            bool approve = false;
            bool.TryParse(obj?.ToString(), out approve);
            if (!approve)
            {
                DateTimePicked = false;
                return;
            }

            try
            {
                dispatcher.Invoke(() =>
                {

                    Messenger.Default.Send(eNavigationNewPatientPage.Upload);

                    SelectedState = StateCollection[5];
                });
            }
            catch (TaskCanceledException tex)
            {
                Debug.WriteLine(tex);
            }
        }

        private void Back(object obj)
        {
            try
            {
                eNavigationNewPatientPage from = eNavigationNewPatientPage.None;

            if (obj != null)
            {
                from = (eNavigationNewPatientPage)obj;
            }

                switch (from)
                {
                    case eNavigationNewPatientPage.Connection:

                        NavigateToMain(eNavigationMainPage.MainMenuPage);
                        break;

                    case eNavigationNewPatientPage.BrowseFile:

                        if (PatientProgramCollection?.Count > 0)
                        {
                            PatientProgramCollection.Clear();
                            PatienName = null;
                            ProgramGenerated = null;
                            SegmentationFinished = 0;
                            PathToFile = string.Empty;
                            SelectedState = StateCollection[2];
                        }
                        else
                        {
                            NavigateThis(eNavigationNewPatientPage.Connection);
                            SelectedState = StateCollection[1];
                        }

                        break;

                    case eNavigationNewPatientPage.Segmentation:
                        if (SegmentationPicked)
                        {
                            SegmentationPicked = false;
                        }
                        else
                        {
                            SelectedSegmentation = 1;
                            SegmentationFinished = 0;
                            SegmentedProgramCollection.Clear();
                            StartTreatmentTime = new TimeSpan(0, 0, 0);
                            StopTreatmentTime = new TimeSpan(23, 55, 00);
                           // SelectedState = StateCollection[2];
                            NavigateThis(eNavigationNewPatientPage.BrowseFile);
                        }
                        break;

                    case eNavigationNewPatientPage.AfterSegmentation:
                        //SelectedSegmentation = 1;
                        SegmentationFinished = 0;
                        SegmentedProgramCollection.Clear();
                        //StartTreatmentTime = new TimeSpan(0, 0, 0);
                        //StopTreatmentTime = new TimeSpan(23, 55, 00);
                        NavigateThis(eNavigationNewPatientPage.Segmentation);
                        break;

                    case eNavigationNewPatientPage.Chart:
                        NavigateThis(eNavigationNewPatientPage.AfterSegmentation);
                        break;

                    case eNavigationNewPatientPage.SelectDate:
                        DateTimePicked = false;
                        NavigateThis(eNavigationNewPatientPage.Chart);
                        SelectedState = StateCollection[3];
                        break;

                    case eNavigationNewPatientPage.Upload:
                        DateTimePicked = false;
                        NavigateThis(eNavigationNewPatientPage.SelectDate);
                        SelectedState = StateCollection[4];
                        break;

                    default:
                        break;
                }
               
            }
            catch (TaskCanceledException tex)
            {
                Debug.WriteLine(tex);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BackToMainMenu()
        {
            try
            {
                NavigateToMain(eNavigationMainPage.MainMenuPage);
            }
            catch (TaskCanceledException tex)
            {
                Debug.WriteLine(tex);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void NavigateToMain(eNavigationMainPage eNavigation)
        {
            await dispatcher.BeginInvoke(new Action(() =>
             {
                 Cleanup();

                 Messenger.Default.Send(eNavigation);
             }));
        }

        private async void NavigateThis(eNavigationNewPatientPage eNavigation)
        {
            await dispatcher.BeginInvoke(new Action(() =>
            {
                Messenger.Default.Send(eNavigation);
            }));
        }

        private bool CanBack(object obj)
        {
            return !Busy;
        }

        public override void Cleanup()
        {
            TaskCollection.Clear();

            StateCollection.Clear();

            SelectedTask = null;

            source = SourceFile.none;

            PatientProgramCollection.Clear();

            SegmentedProgramCollection.Clear();

            SegmentationFinished = 0;

            TreatmentStarted = false;

            PatienName = null;

            ProgramGenerated = null;

            ButtonText = "OK";

            PathToFile = string.Empty;

            UploadingProgress = 0;

            SelectedSegmentation = 1;

            StartTreatmentTime = new TimeSpan(0, 0, 0);

            StopTreatmentTime = new TimeSpan(23, 59, 59);

            Busy = false;

            UploadingSuccess = false;

            DateTimePicked = false;

            CommWrapper.Dispose();

            if (CommWrapper != null)
            {
                CommWrapper.ConnectionChanged -= CommWrapper_ConnectionChanged;
            }

            loaded = false; 
        }

        #endregion
    }
}
