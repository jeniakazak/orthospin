﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Win32;
using orthospin.Consants;
using orthospin.Model;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace orthospin.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        Dispatcher dispatcher = Application.Current.Dispatcher;
        private readonly IDataService _dataService;

        private int progress = 0;
        public int Progress
      
  {
            get
            {
                return progress;
            }
            set
            {
                Set(ref progress, value);
            }
        }
        private bool progressThreadRun = false;
        private Thread progressThread;

        private string _userName = string.Empty;

        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                Set(ref _userName, value);
            }
        }

        public MainViewModel(IDataService dataService)
        {
            _dataService = dataService;
            //_dataService.GetData(
            //    (item, error) =>
            //    {
            //        if (error != null)
            //        {
            //            // Report error here
            //            return;
            //        }

            //        WelcomeTitle = item.Title;
            //    });

            progressRun();
        }

        private void progressRun()
        {
            if (progressThread != null)
            {
                progressThreadRun = false;
            }
            else
            {
                progressThread = new Thread(() =>
                {

                    progressThreadRun = true;
                    Thread.Sleep(485);

                    while (progressThreadRun)
                    {
                        Progress++;
                        if (progress >= 100)
                        {
                            progressThreadRun = false;
                        }

                        Thread.Sleep(12);
                    }

                    try
                    {
                        dispatcher.Invoke(() =>
                        {
                            Messenger.Default.Send(eNavigationMainPage.LoginPage);
                        });
                    }
                    catch (TaskCanceledException tex)
                    {
                        Debug.WriteLine(tex);
                    }
                });
            }

            progressThread.Start();
        }

        public override void Cleanup()
        {
            // Clean up if needed

            base.Cleanup();
        }

        #region The RelayCommand that implements ICommand

        public ICommand LoginCommand
        {
            get { return _LoginCommand ?? (_LoginCommand = new RelayCommand<object>(LogIn)); }
            set
            {
                Set(ref _LoginCommand, value);
            }
        }
        private ICommand _LoginCommand;

        public ICommand ManualModeCommand
        {
            get { return _ManualModeCommand ?? (_ManualModeCommand = new RelayCommand(GoToManualMode)); }
            set
            {
                Set(ref _ManualModeCommand, value);
            }
        }
        private ICommand _ManualModeCommand;

        public ICommand LogCommand
        {
            get { return _LogCommand ?? (_LogCommand = new RelayCommand(GoToLog)); }
            set
            {
                Set(ref _LogCommand, value);
            }
        }
        private ICommand _LogCommand;

        public ICommand NewPatientCommand
        {
            get { return _NewPatientCommand ?? (_NewPatientCommand = new RelayCommand(GoNewPatient)); }
            set
            {
                Set(ref _NewPatientCommand, value);
            }
        }
        private ICommand _NewPatientCommand;


        #endregion

        #region The method encapsulated in the relay command

        private void LogIn(object obj)
        {
            //if (UserName.Equals(MainWindow._settings.username) && UserPassword.Equals(MainWindow._settings.password))
            //{

            //    dispatcher.Invoke(() =>
            //    {
            //        Messenger.Default.Send(eNavigationMainPage.MainMenuPage);
            //    });
            //}

            PasswordBox pwBox = obj as PasswordBox;

            string UserPassword = pwBox.Password;

            if (UserName.Equals(UserName) && UserPassword.Equals(UserPassword))
            {

                dispatcher.Invoke(() =>
                {
                    Messenger.Default.Send(eNavigationMainPage.MainMenuPage);
                });
            }
        }

        private void GoToManualMode()
        {
            dispatcher.Invoke(() =>
            {
                Messenger.Default.Send(eNavigationMainPage.ManualMode);
            });
        }

        private void GoToLog()
        {
            dispatcher.Invoke(() =>
            {
                Messenger.Default.Send(eNavigationMainPage.LogPage);
            });
        }

        private void GoNewPatient()
        {
            dispatcher.Invoke(() =>
            {
                Messenger.Default.Send(eNavigationMainPage.NewPatient);
            });
        }

        #endregion
    }
}