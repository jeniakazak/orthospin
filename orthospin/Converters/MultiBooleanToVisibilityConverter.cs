﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace orthospin.Converters
{
    class MultiBooleanToVisibilityConverter : MarkupExtension, IMultiValueConverter
    {
        private static MultiBooleanToVisibilityConverter _converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new MultiBooleanToVisibilityConverter();
            return _converter;
        }

        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool bValForAll = true;

            string[] arr ;


            if (parameter == null || !(parameter is string[]))
            {
                arr = null;
            }
            else
            {
                arr = parameter as string[];
            }


            for (int i = 0; i < values.Length; i++)
            {
                bool result = false;
                bool.TryParse(values[i].ToString(), out result);

                bool inverse = false;
                if(arr != null && arr?.Length > 0)
                bool.TryParse(arr[i]?.ToString(), out inverse);

                bValForAll &= result ^ inverse;
            }

            var res = bValForAll ? Visibility.Visible : Visibility.Collapsed;
            return res;
        }

        public object[] ConvertBack(object value, System.Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new object[] { Binding.DoNothing, Binding.DoNothing, Binding.DoNothing, Binding.DoNothing, };
        }
    }
}
