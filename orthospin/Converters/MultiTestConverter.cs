﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;

namespace orthospin.Converters
{
    class MultiTestConverter : MarkupExtension, IMultiValueConverter
    {

        private static MultiTestConverter _converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new MultiTestConverter();
            return _converter;
        }


        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string parameterstring = string.Empty;
            if (parameter != null)
            {
                parameterstring = parameter as string;
            }

            if(values.Length < 2)
            {
                return Binding.DoNothing;
            }

            ListBoxItem item = (ListBoxItem)values[1];

            if (item != null)
            {
                ListBox listView = ItemsControl.ItemsControlFromItemContainer(item) as ListBox;

                if (listView != null)
                {
                    int index = listView.ItemContainerGenerator.IndexFromContainer(item);
                    int SelectedIndex = listView.SelectedIndex;

                    SolidColorBrush ColorBrush = new SolidColorBrush(Color.FromArgb(100, 255, 125, 35));

                    if (index < SelectedIndex)
                    {
                        ColorBrush = new SolidColorBrush(Color.FromRgb(43, 105, 101));
                    }
                    else if (index > SelectedIndex)
                    {
                        ColorBrush = new SolidColorBrush(Color.FromRgb(31, 224, 227));
                    }
                    else if (index == SelectedIndex)
                    {
                        ColorBrush = new SolidColorBrush(Color.FromRgb(255, 255, 255));
                    }

                    return ColorBrush;
                }
            }

            return Binding.DoNothing;
        }

        public object[] ConvertBack(object value, System.Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new object[] { Binding.DoNothing, Binding.DoNothing, Binding.DoNothing, Binding.DoNothing, };
        }

    }
}
