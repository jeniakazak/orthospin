﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace orthospin.Converters
{
    class IntToCornerRadiusConverter : MarkupExtension, IValueConverter
    {
        private static IntToCornerRadiusConverter _converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new IntToCornerRadiusConverter();
            return _converter;
        }

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            double topLeft = 0.0;
            double topRight = 0.0;
            double bottomRight = 0.0;
            double bottomLeft = 0.0;

            string arr = parameter as string;
            if(arr.Length == 5)
            {
                int factor = 1;
                int.TryParse(arr[4].ToString(),out factor);
                factor = factor < 1 ? 1 : factor;

                double _value = 0.0;
                double.TryParse(value.ToString(), out _value);
                _value = _value < 1 ? 1 : _value;

                if (arr[0].Equals('1'))
                {
                    double.TryParse(arr[0].ToString(), out topLeft);
                    topLeft = _value/factor;
                }
                if (arr[1].Equals('1'))
                {
                    double.TryParse(arr[1].ToString(), out topRight);
                    topRight = _value / factor;
                }
                if (arr[2].Equals('1'))
                {
                    double.TryParse(arr[2].ToString(), out bottomRight);
                    bottomRight = _value / factor;
                }
                if (arr[3].Equals('1'))
                {
                    double.TryParse(arr[3].ToString(), out bottomLeft);
                    bottomLeft = _value / factor;
                }
            }

            return new CornerRadius(topLeft, topRight, bottomRight, bottomLeft);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
