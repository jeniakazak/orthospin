﻿using System;
using System.Windows.Data;
using System.Windows.Markup;

namespace orthospin.Converters
{
    public class FirstTreatmentIsEnabledConverter : MarkupExtension , IMultiValueConverter
    {
        private static FirstTreatmentIsEnabledConverter _converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new FirstTreatmentIsEnabledConverter();
            return _converter;
        }
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = 0;
            int.TryParse(values[0]?.ToString(), out count);

            int segmentation = 0;
            int.TryParse(values[1]?.ToString(), out segmentation);

            if((count == 1 || count == 0) && segmentation == 1)
            {
                return true;
            }

            return false;

        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return new object[] { Binding.DoNothing, Binding.DoNothing };
        }
    }
}
