﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace orthospin.Converters
{
    public class TimeSpanToStringConverter : MarkupExtension, IValueConverter
    {
        private static TimeSpanToStringConverter _converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new TimeSpanToStringConverter();
            return _converter;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            String result = String.Empty;
            if (value is TimeSpan)
            {
                TimeSpan timeSpan = (TimeSpan)value;
                
                    if (parameter == null || !(parameter is string))
                        result = String.Format("{0:D2}:{1:D2}:{2:D2}:{3:D2}.{4:D1}",
                                    timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds, (int)Math.Round(timeSpan.Milliseconds / 100.0));
                    else
                    {
                        DateTime time = DateTime.Today;
                        time = time.Add(timeSpan);
                        result = time.ToString((string)parameter);
                    }
                }
            
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
            {
                DateTime outSpan = DateTime.Now;

                if(DateTime.TryParse(value.ToString(), out outSpan))
                {
                    return outSpan.TimeOfDay;
                }
            }

            return DependencyProperty.UnsetValue;
        }
    }
}
