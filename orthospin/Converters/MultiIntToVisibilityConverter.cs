﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace orthospin.Converters
{
    class MultiIntToVisibilityConverter : MarkupExtension, IMultiValueConverter
    {
        private static MultiIntToVisibilityConverter _converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new MultiIntToVisibilityConverter();
            return _converter;
        }

        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool bValForAll = true;

            string[] arr = parameter as string[];

            for (int i = 0; i < values.Length; i++)
            {
                int result = 0;
                int.TryParse(values[i].ToString(), out result);

                bool inverse = false;
                bool.TryParse(arr[i]?.ToString(), out inverse);

                bValForAll = bValForAll && (result > 0) ^ inverse;
            }

            var res = bValForAll ? Visibility.Visible : Visibility.Collapsed;
            return res;
        }

        public object[] ConvertBack(object value, System.Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new object[] { Binding.DoNothing, Binding.DoNothing, Binding.DoNothing, Binding.DoNothing, };
        }
    }

}
