﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace orthospin.Converters
{
    class intToBooleanInverseConverter : MarkupExtension, IValueConverter
    {
        private static IntToCornerRadiusConverter _converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new IntToCornerRadiusConverter();
            return _converter;
        }

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        { 
            bool bval = false;
            Boolean.TryParse(value?.ToString(), out bval);

            return !bval;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
