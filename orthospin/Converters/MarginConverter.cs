﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace orthospin.Converters
{
    public class MarginConverter : MarkupExtension, IValueConverter
    {
        private static MarginConverter _converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new MarginConverter();
            return _converter;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Thickness thickness = new Thickness(0, 0, 0, 0);

            thickness = new Thickness(0, 0, System.Convert.ToDouble(value) / 2, 0);

            return thickness;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
