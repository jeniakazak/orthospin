﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace orthospin.Converters
{
    class IndexToVisibilityConverter : MarkupExtension, IValueConverter
    {
        private static IndexToVisibilityConverter _converter = null;

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new IndexToVisibilityConverter();
            return _converter;
        }

        public object Convert(object value, Type TargetType, object parameter, CultureInfo culture)
        {
            string parameterstring = string.Empty;
            if (parameter != null)
            {
                parameterstring = parameter as string;
            }

            ListBoxItem item = (ListBoxItem)value;
            if (item != null)
            {
                ListBox listView = ItemsControl.ItemsControlFromItemContainer(item) as ListBox;

                if (listView != null)
                {
                    int index = listView.ItemContainerGenerator.IndexFromContainer(item);
                    int? Maxindex = listView.ItemContainerGenerator?.Items?.Count - 1;

                    Visibility visible = Visibility.Visible;

                    if (parameterstring.ToLower() == "left" && index == 0)
                    {
                        visible = Visibility.Collapsed;
                    }
                    else if (parameterstring.ToLower() == "right")
                    {
                        if (Maxindex != null)
                        {
                            if (Maxindex == index)
                            {
                                visible = Visibility.Collapsed;
                            }
                        }
                    }

                    return visible;
                }
            }

            return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}