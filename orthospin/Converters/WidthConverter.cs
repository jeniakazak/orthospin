﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace orthospin.Converters
{
    class WidthConverter : MarkupExtension, IValueConverter
    {
        private static WidthConverter _converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new WidthConverter();
            return _converter;
        }
        //LogView convert eight of grid to columns weigh
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double bVal;
            bool result = double.TryParse(value.ToString(), out bVal);
            if (!result) return value;

            int divider = 1;
            if (parameter != null) {

                int.TryParse(parameter.ToString(), out divider);
                divider = divider < 1 ? 1 : divider;
            }

            return bVal/ divider;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
