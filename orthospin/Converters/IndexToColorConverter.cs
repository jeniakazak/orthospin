﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;

namespace orthospin.Converters
{
    class IndexToColorConverter : MarkupExtension, IValueConverter
    {
        private static IndexToColorConverter _converter = null;

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new IndexToColorConverter();
            return _converter;
        }

        public object Convert(object value, Type TargetType, object parameter, CultureInfo culture)
        {
            string parameterstring = string.Empty;
            if (parameter != null)
            {
                parameterstring = parameter as string;
            }

            ListBoxItem item = (ListBoxItem)value;
            if (item != null)
            {
                ListBox listView = ItemsControl.ItemsControlFromItemContainer(item) as ListBox;

                if (listView != null)
                {
                    int index = listView.ItemContainerGenerator.IndexFromContainer(item);
                    int SelectedIndex = listView.SelectedIndex;

                    SolidColorBrush ColorBrush = new SolidColorBrush(Color.FromArgb(100,255, 125, 35));

                    if(index < SelectedIndex)
                    {
                        ColorBrush = new SolidColorBrush(Color.FromRgb( 43, 105, 101));
                    }
                    else if(index > SelectedIndex)
                    {
                        ColorBrush = new SolidColorBrush(Color.FromRgb( 31, 224, 227));
                    }
                    else if (index == SelectedIndex)
                    {
                        ColorBrush = new SolidColorBrush(Color.FromRgb( 255, 255, 255));
                    }

                    return ColorBrush;
                }
            }

            return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}
