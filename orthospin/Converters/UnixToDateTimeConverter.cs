﻿using orthospin.Device;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace orthospin.Converters
{
    class UnixToDateConverter : MarkupExtension, IValueConverter
    {

        private static UnixToDateConverter _converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new UnixToDateConverter();
            return _converter;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            string result = string.Empty;

            uint uval = 0;

            bool hasvalue = UInt32.TryParse(value.ToString(), out uval);

            if (hasvalue && uval > 0)
            {

                DateTime? dateTime = DeviceUtils.UnixTime2DateTime(uval);


                if (parameter == null || !(parameter is string))
                {
                    result = dateTime.Value.ToString();
                }
                else
                {
                    result = dateTime.Value.ToString((string)parameter);
                }
            }

            return result;

        }


        string[] separator = new string[] { " " };
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strValue = value as string;
            DateTime resultDateTime;
            if (DateTime.TryParse(strValue, out resultDateTime))
            {
                return DeviceUtils.DateTime2UnixTime(resultDateTime);
            }
            return DependencyProperty.UnsetValue;
        }
    }
}
