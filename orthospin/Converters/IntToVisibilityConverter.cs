﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace orthospin.Converters
{
    public class IntToVisibilityConverter : MarkupExtension, IValueConverter
    {
        private static IntToVisibilityConverter _converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new IntToVisibilityConverter();
            return _converter;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int bVal;
            bool result = int.TryParse(value.ToString(), out bVal);
            if (!result) return value;

            bool inverse = false;
            bool.TryParse(parameter?.ToString(), out inverse);

            var res = (bVal > 0) ^ inverse ? Visibility.Visible : Visibility.Collapsed;
            return res;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

}
