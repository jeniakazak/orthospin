﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace orthospin.Converters
{

    class HasValueToVisibilityMultiConverter : MarkupExtension, IValueConverter
    {
        private static HasValueToVisibilityMultiConverter _converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new HasValueToVisibilityMultiConverter();
            return _converter;
        }

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var res = Visibility.Collapsed;

            if (value != null)
            {
                res = Visibility.Visible;
            }

            return res;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

}
