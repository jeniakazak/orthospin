﻿using System;
using System.Windows.Data;
using System.Windows.Markup;

namespace orthospin.Converters
{
    class MultiWidthConverter : MarkupExtension, IMultiValueConverter
    {
        private static MultiWidthConverter _converter = null;
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null) _converter = new MultiWidthConverter();
            return _converter;
        }

        // values [0] - Width
        // values [1] - Num of Items
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length < 2)
                return values;

            double ElementWidth;
            bool result = double.TryParse(values[0].ToString(), out ElementWidth);
            if (!result) return values;

            int divider = 1;
            if (values[1] != null)
            {
                if (int.TryParse(values[1].ToString(), out divider))
                {
                    divider = divider < 1 ? 1 : divider;
                }
            }

            return ElementWidth / divider;
        }

        public object[] ConvertBack(object value, System.Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new object[] { Binding.DoNothing, Binding.DoNothing, Binding.DoNothing, Binding.DoNothing, };
        }
    }
}
