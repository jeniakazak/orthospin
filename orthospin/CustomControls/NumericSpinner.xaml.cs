using orthospin.Converters;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace orthospin.CustomControls
{
    /// <summary>
    /// Interaction logic for NumericSpinner.xaml
    /// </summary>
    public partial class NumericSpinner : UserControl
    {
        #region Fields

        public event EventHandler PropertyChanged;
        public event EventHandler ValueChanged;
        #endregion

        public NumericSpinner()
        {
            InitializeComponent();

            tb_main.SetBinding(TextBox.TextProperty, new Binding("Value")
            {
                ElementName = "root_numeric_spinner",
                Mode = BindingMode.TwoWay,
                UpdateSourceTrigger = UpdateSourceTrigger.Explicit,
                Converter = new TimeSpanToStringConverter(),
                ConverterParameter = "hh:mm tt",
                ValidatesOnDataErrors = true,
                NotifyOnValidationError = true
            });

            tb_main.MaxLength = 8;

            tb_main.TextChanged += Tb_main_TextChanged;
            tb_main.PreviewTextInput += Tb_main_PreviewTextInput;


            DependencyPropertyDescriptor.FromProperty(ValueProperty, typeof(NumericSpinner)).AddValueChanged(this, PropertyChanged);
            DependencyPropertyDescriptor.FromProperty(ValueProperty, typeof(NumericSpinner)).AddValueChanged(this, ValueChanged);
            DependencyPropertyDescriptor.FromProperty(DecimalsProperty, typeof(NumericSpinner)).AddValueChanged(this, PropertyChanged);
            DependencyPropertyDescriptor.FromProperty(MinValueProperty, typeof(NumericSpinner)).AddValueChanged(this, PropertyChanged);
            DependencyPropertyDescriptor.FromProperty(MaxValueProperty, typeof(NumericSpinner)).AddValueChanged(this, PropertyChanged);

            PropertyChanged += (x, y) => validate();

        }

        private void Tb_main_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            string text = tb_main.Text;
        }


        private string[] dateFormats = new string[] { "hh:mm tt"};

        private void Tb_main_TextChanged(object sender, TextChangedEventArgs e)
        {

            string text = tb_main.Text;

            DateTime tempDate = DateTime.Now;

            if (DateTime.TryParseExact(text, dateFormats, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out tempDate))
            {

                Value = tempDate.TimeOfDay;

                BindingExpression be = tb_main.GetBindingExpression(TextBox.TextProperty);
                be.UpdateSource();

                BindingExpressionBase bindingExpressionBase =
                 BindingOperations.GetBindingExpressionBase(tb_main, TextBox.TextProperty);

                ValidationError validationError =
                    new ValidationError(new ExceptionValidationRule(), be);

                Validation.ClearInvalid(be);
            }
            else
            {
                BindingExpression bindingExpression =
                        BindingOperations.GetBindingExpression(tb_main, TextBox.TextProperty);

                BindingExpressionBase bindingExpressionBase =
                    BindingOperations.GetBindingExpressionBase(tb_main, TextBox.TextProperty);

                ValidationError validationError =
                    new ValidationError(new ExceptionValidationRule(), bindingExpression);

                Validation.MarkInvalid(bindingExpressionBase, validationError);
            }

        }



        #region ValueProperty

        public readonly static DependencyProperty ValueProperty = DependencyProperty.Register(
            "Value",
            typeof(TimeSpan),
            typeof(NumericSpinner),
            new PropertyMetadata(new TimeSpan(0)));

        public TimeSpan Value
        {
            get { return (TimeSpan)GetValue(ValueProperty); }
            set
            {
                if (value < MinValue)
                    value = MinValue;
                if (value > MaxValue)
                    value = MaxValue;

                SetValue(ValueProperty, value);

                if (ValueChanged != null)
                    ValueChanged(this, new EventArgs());
            }
        }


        #endregion


        #region StepProperty

        public readonly static DependencyProperty StepProperty = DependencyProperty.Register(
            "Step",
            typeof(double),
            typeof(NumericSpinner),
            new PropertyMetadata((double)1));

        public double Step
        {
            get { return (double)GetValue(StepProperty); }
            set
            {
                SetValue(StepProperty, value);
            }
        }

        #endregion

        #region DecimalsProperty

        public readonly static DependencyProperty DecimalsProperty = DependencyProperty.Register(
            "Decimals",
            typeof(int),
            typeof(NumericSpinner),
            new PropertyMetadata(2));

        public int Decimals
        {
            get { return (int)GetValue(DecimalsProperty); }
            set
            {
                SetValue(DecimalsProperty, value);
            }
        }

        #endregion

        #region MinValueProperty

        public readonly static DependencyProperty MinValueProperty = DependencyProperty.Register(
            "MinValue",
            typeof(TimeSpan),
            typeof(NumericSpinner),
            new PropertyMetadata(TimeSpan.Zero));

        public TimeSpan MinValue
        {
            get { return (TimeSpan)GetValue(MinValueProperty); }
            set
            {
                if (value > MaxValue)
                    MaxValue = value;
                SetValue(MinValueProperty, value);
            }
        }

        #endregion

        #region MaxValueProperty

        public readonly static DependencyProperty MaxValueProperty = DependencyProperty.Register(
            "MaxValue",
            typeof(TimeSpan),
            typeof(NumericSpinner),
            new PropertyMetadata(new TimeSpan(23, 59, 59)));

        public TimeSpan MaxValue
        {
            get { return (TimeSpan)GetValue(MaxValueProperty); }
            set
            {
                if (value < MinValue)
                    value = MinValue;
                SetValue(MaxValueProperty, value);
            }
        }

        #endregion

        /// <summary>
        /// Revalidate the object, whenever a value is changed...
        /// </summary>
        private void validate()
        {
            // Logically, This is not needed at all... as it's handled within other properties...
            if (MinValue > MaxValue) MinValue = MaxValue;
            if (MaxValue < MinValue) MaxValue = MinValue;
            if (Value < MinValue) Value = MinValue;
            if (Value > MaxValue) Value = MaxValue;
        }



        private void cmdUp_Click(object sender, RoutedEventArgs e)
        {
            Value += TimeSpan.FromMinutes(Step);
        }

        private void cmdDown_Click(object sender, RoutedEventArgs e)
        {
            Value -= TimeSpan.FromMinutes(Step);
        }

        private void tb_main_Loaded(object sender, RoutedEventArgs e)
        {
            ValueChanged(this, new EventArgs());
        }
    }
}
