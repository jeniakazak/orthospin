﻿using orthospin.Converters;
using orthospin.Device;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace orthospin.CustomControls
{
    /// <summary>
    /// Interaction logic for CustomDateTextBox.xaml
    /// </summary>
    public partial class CustomDateTextBox : UserControl
    {
        #region Fields

        public event EventHandler PropertyChanged;
        public event EventHandler ValueChanged;
        #endregion

        public CustomDateTextBox()
        {
            InitializeComponent();
            tb_Date.SetBinding(TextBox.TextProperty, new Binding("Date")
            {
                ElementName = "root_custom_textbox",
                Mode = BindingMode.TwoWay,
                UpdateSourceTrigger = UpdateSourceTrigger.Explicit,
                Converter = new UnixToDateConverter(),
                ConverterParameter = "MM/dd/yyyy",
                ValidatesOnDataErrors = true,
                NotifyOnValidationError = true
            });

            tb_Hours.SetBinding(TextBox.TextProperty, new Binding("Time")
            {
                ElementName = "root_custom_textbox",
                Mode = BindingMode.TwoWay,
                UpdateSourceTrigger = UpdateSourceTrigger.Explicit,
                Converter = new UnixToDateConverter(),
                ConverterParameter = "hh:mm tt",
                ValidatesOnDataErrors = true,
                NotifyOnValidationError = true
            });


            tb_Date.TextChanged += tb_Date_TextChanged;
            tb_Hours.TextChanged += tb_Hours_TextChanged;


            DependencyPropertyDescriptor.FromProperty(DateProperty, typeof(CustomDateTextBox)).AddValueChanged(this, PropertyChanged);
            DependencyPropertyDescriptor.FromProperty(DateProperty, typeof(CustomDateTextBox)).AddValueChanged(this, ValueChanged);

            DependencyPropertyDescriptor.FromProperty(TimeProperty, typeof(CustomDateTextBox)).AddValueChanged(this, PropertyChanged);
            DependencyPropertyDescriptor.FromProperty(TimeProperty, typeof(CustomDateTextBox)).AddValueChanged(this, ValueChanged);
        }

        private void tb_Date_TextChanged(object sender, TextChangedEventArgs e)
        {

            string text = tb_Date.Text;

            DateTime tempDate = DateTime.Now;

            bool IsDate = DateTime.TryParseExact(text, "MM/dd/yyyy", Thread.CurrentThread.CurrentCulture, DateTimeStyles.AllowWhiteSpaces, out tempDate);

            if (IsDate  && tempDate.Year > 1970)
            {

                DateTime? temp = DeviceUtils.UnixTime2DateTime(Date);

                Date = DeviceUtils.DateTime2UnixTime(new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, temp.Value.Hour,
                    temp.Value.Minute, temp.Value.Second));

                BindingExpression be = tb_Date.GetBindingExpression(TextBox.TextProperty);
                

                BindingExpressionBase bindingExpressionBase =
                   BindingOperations.GetBindingExpressionBase(tb_Date, TextBox.TextProperty);

                ValidationError validationError =
                    new ValidationError(new ExceptionValidationRule(), be);

                Validation.ClearInvalid(be);

                be.UpdateSource();

            }
            else
            {
                BindingExpression bindingExpression =
                        BindingOperations.GetBindingExpression(tb_Date, TextBox.TextProperty);

                BindingExpressionBase bindingExpressionBase =
                    BindingOperations.GetBindingExpressionBase(tb_Date, TextBox.TextProperty);

                ValidationError validationError =
                    new ValidationError(new ExceptionValidationRule(), bindingExpression);

                Validation.MarkInvalid(bindingExpressionBase, validationError);
            }


        }

        private void tb_Hours_TextChanged(object sender, TextChangedEventArgs e)
        {

            string text = tb_Hours.Text;

            DateTime tempHours = DateTime.Now;

            if (DateTime.TryParseExact(text, "hh:mm tt", Thread.CurrentThread.CurrentCulture, DateTimeStyles.AllowWhiteSpaces, out tempHours))
            {

                DateTime? temp = DeviceUtils.UnixTime2DateTime(Time);

                Time = DeviceUtils.DateTime2UnixTime(new DateTime(temp.Value.Year, temp.Value.Month, temp.Value.Day, tempHours.Hour,
                    tempHours.Minute, tempHours.Second));


                BindingExpression be = tb_Hours.GetBindingExpression(TextBox.TextProperty);

                BindingExpressionBase bindingExpressionBase =
                  BindingOperations.GetBindingExpressionBase(tb_Hours, TextBox.TextProperty);

                ValidationError validationError =
                    new ValidationError(new ExceptionValidationRule(), be);

                Validation.ClearInvalid(be);

                be.UpdateSource();
            }
            else
            {
                BindingExpression bindingExpression =
                        BindingOperations.GetBindingExpression(tb_Hours, TextBox.TextProperty);

                BindingExpressionBase bindingExpressionBase =
                    BindingOperations.GetBindingExpressionBase(tb_Hours, TextBox.TextProperty);

                ValidationError validationError =
                    new ValidationError(new ExceptionValidationRule(), bindingExpression);

                Validation.MarkInvalid(bindingExpressionBase, validationError);
            }

        }


        #region DateProperty

        public readonly static DependencyProperty DateProperty = DependencyProperty.Register(
            "Date",
            typeof(uint),
            typeof(CustomDateTextBox),
            new PropertyMetadata( (uint)0));

        public uint Date
        {
            get { return (uint)GetValue(DateProperty); }
            set
            {

                SetValue(DateProperty, value);

                if (ValueChanged != null)
                    ValueChanged(this, new EventArgs());
            }
        }


        #endregion

        #region TimeProperty

        public readonly static DependencyProperty TimeProperty = DependencyProperty.Register(
            "Time",
            typeof(uint),
            typeof(CustomDateTextBox),
            new PropertyMetadata((uint)0));

        public uint Time
        {
            get { return (uint)GetValue(TimeProperty); }
            set
            {

                SetValue(TimeProperty, value);

                if (ValueChanged != null)
                    ValueChanged(this, new EventArgs());
            }
        }


        #endregion


        #region Date Enabled Property 
        public bool? DateEnabled
        {
            get { return (bool?)GetValue(DateEnabledProperty); }
            set { SetValue(DateEnabledProperty, value); }
        }

        public static readonly DependencyProperty DateEnabledProperty
            = DependencyProperty.Register(
                "DateEnabled",
                typeof(bool?),
                typeof(CustomDateTextBox),
                new FrameworkPropertyMetadata(
                    null,
                    OnDateCheckedChanged)
                );

        private static void OnDateCheckedChanged(DependencyObject src, DependencyPropertyChangedEventArgs e)
        {
            var DateTextBox = src as CustomDateTextBox;
            DateTextBox.tb_Date.IsEnabled = (bool)(e.NewValue as bool?);
        }
        #endregion

        #region Hours Enabled Property 
        public bool? HoursEnabled
        {
            get { return (bool?)GetValue(HoursEnabledProperty); }
            set { SetValue(HoursEnabledProperty, value); }
        }

        public static readonly DependencyProperty HoursEnabledProperty
            = DependencyProperty.Register(
                "HoursEnabled",
                typeof(bool?),
                typeof(CustomDateTextBox),
                new FrameworkPropertyMetadata(
                    null,
                    OnHoursCheckedChanged)
                );

        private static void OnHoursCheckedChanged(DependencyObject src, DependencyPropertyChangedEventArgs e)
        {
            var DateTextBox = src as CustomDateTextBox;
            DateTextBox.tb_Hours.IsEnabled = (bool)(e.NewValue as bool?);
        }
        #endregion


        private void tb_main_Loaded(object sender, RoutedEventArgs e)
        {
            ValueChanged(this, new EventArgs());
        }
    }
}
