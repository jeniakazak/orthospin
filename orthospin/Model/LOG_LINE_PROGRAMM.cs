﻿using GalaSoft.MvvmLight;

namespace orthospin.Model
{
    public class LOG_LINE_PROGRAMM : ObservableObject
    {
        private uint _count;
        private uint _ExecuteTime;
        private float _Strut1Movement = 0.0f;
        private float _Strut2Movement = 0.0f;
        private float _Strut3Movement = 0.0f;
        private float _Strut4Movement = 0.0f;
        private float _Strut5Movement = 0.0f;
        private float _Strut6Movement = 0.0f;


        public float _Strut1Position= 0.0f;
        public float _Strut2Position= 0.0f;
        public float _Strut3Position= 0.0f;
        public float _Strut4Position= 0.0f;
        public float _Strut5Position= 0.0f;
        public float _Strut6Position = 0.0f;

        public uint Count
        {
            get
            {
                return _count;
            }
            set
            {
                Set(ref _count, value);
            }
        }

        public uint ExecuteTime
        {
            get
            {
                return _ExecuteTime;
            }
            set
            {
                Set(ref _ExecuteTime, value);
            }
        }
        public float Strut1Movement
        {
            get
            {
                return _Strut1Movement;
            }
            set
            {
                Set(ref _Strut1Movement, value);
            }
        }
        public float Strut2Movement
        {
            get
            {
                return _Strut2Movement;
            }
            set
            {
                Set(ref _Strut2Movement, value);
            }
        }
        public float Strut3Movement
        {
            get
            {
                return _Strut3Movement;
            }
            set
            {
                Set(ref _Strut3Movement, value);
            }
        }
        public float Strut4Movement
        {
            get
            {
                return _Strut4Movement;
            }
            set
            {
                Set(ref _Strut4Movement, value);
            }
        }
        public float Strut5Movement
        {
            get
            {
                return _Strut5Movement;
            }
            set
            {
                Set(ref _Strut5Movement, value);
            }
        }
        public float Strut6Movement
        {
            get
            {
                return _Strut6Movement;
            }
            set
            {
                Set(ref _Strut6Movement, value);
            }
        }


        public float Strut1Position
        {
            get
            {
                return _Strut1Position;
            }
            set
            {
                Set(ref _Strut1Position, value);
            }
        }
        public float Strut2Position
        {
            get
            {
                return _Strut2Position;
            }
            set
            {
                Set(ref _Strut2Position, value);
            }
        }
        public float Strut3Position
        {
            get
            {
                return _Strut3Position;
            }
            set
            {
                Set(ref _Strut3Position, value);
            }
        }
        public float Strut4Position
        {
            get
            {
                return _Strut4Position;
            }
            set
            {
                Set(ref _Strut4Position, value);
            }
        }
        public float Strut5Position
        {
            get
            {
                return _Strut5Position;
            }
            set
            {
                Set(ref _Strut5Position, value);
            }
        }
        public float Strut6Position
        {
            get
            {
                return _Strut6Position;
            }
            set
            {
                Set(ref _Strut6Position, value);
            }
        }
    }
}

