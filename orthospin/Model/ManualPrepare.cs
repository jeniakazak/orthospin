﻿using GalaSoft.MvvmLight;

namespace orthospin.Model
{
    public class ManualPrepare : ObservableObject
    {
        private int _motorId;
        private int _MoveType = 1;
        private bool _readyToMove = false;
        public int MotorId
        {
            get
            {
                return _motorId;
            }
            set
            {
                Set(ref _motorId, value);
            }
        }
        public int MoveType
        {
            get
            {
                return _MoveType;
            }
            set
            {
                Set(ref _MoveType, value);
                RaisePropertyChanged("Display");
            }
        }
        public bool ReadyToMove
        {
            get
            {
                return _readyToMove;
            }
            set
            {
                Set(ref _readyToMove, value);
            }
        }

        public string Display
        {
            get
            {
                string ret = string.Empty;
                if (MoveType == -1)
                {
                    ret = "You are about to make a short movement, are you sure you want to proceed?";
                }
                else if (MoveType == 1)
                {
                    ret = "You are about to make a long movement, are you sure you want to proceed?";
                }
                return ret;
            }
        }
    }
}
