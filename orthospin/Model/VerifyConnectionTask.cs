﻿using GalaSoft.MvvmLight;


namespace orthospin.Model
{
    public class VerifyConnectionTask : ObservableObject
    {
        private int _id;
        public int ID 
        {
            get
            {
                return _id;
            }
            set
            {
                Set(ref _id, value);
            }
        }

        private string _displayext;
        public string Displayext
        {
            get
            {
                return _displayext;
            }
            set
            {
                Set(ref _displayext, value);
            }
        }

        private bool _isDone = false;
        public bool IsDone
        {
            get
            {
                return _isDone;
            }
            set
            {
                Set(ref _isDone, value);
            }
        }
    }
}
