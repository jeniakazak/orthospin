﻿using GalaSoft.MvvmLight;
using orthospin.Utils;
using System;

namespace orthospin.Model
{
    public class PatientProgram : ObservableObject, ICloneable
    {
        public PatientProgram()
        {
            ExecuteDate = 0;
            ExecuteTime = 0;
        }

        private uint _count;
        private uint _ExecuteDate;
        private uint _ExecuteTime;
        private float _Strut1Movement = 0;
        private float _Strut2Movement = 0.0f;
        private float _Strut3Movement = 0.0f;
        private float _Strut4Movement = 0.0f;
        private float _Strut5Movement = 0.0f;
        private float _Strut6Movement = 0.0f;

        public uint Count
        {
            get
            {
                return _count;
            }
            set
            {
                Set(ref _count, value);
            }
        }

        public uint ExecuteDate
        {
            get
            {
                return _ExecuteDate;
            }
            set
            {
                Set(ref _ExecuteDate, value);
            }
        }

        public uint ExecuteTime
        {
            get
            {
                return _ExecuteTime;
            }
            set
            {
                Set(ref _ExecuteTime, value);
            }
        }

        public float Strut1Movement
        {
            get
            {
                return _Strut1Movement;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentException("value must be positive.");

                Set(ref _Strut1Movement, value);
            }
        }
        public float Strut2Movement
        {
            get
            {
                return _Strut2Movement;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentException("value must be positive.");

                Set(ref _Strut2Movement, value);
            }
        }
        public float Strut3Movement
        {
            get
            {
                return _Strut3Movement;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentException("value must be positive.");

                Set(ref _Strut3Movement, value);
            }
        }
        public float Strut4Movement
        {
            get
            {
                return _Strut4Movement;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentException("value must be positive.");

                Set(ref _Strut4Movement, value);
            }
        }
        public float Strut5Movement
        {
            get
            {
                return _Strut5Movement;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentException("value must be positive.");

                Set(ref _Strut5Movement, value);
            }
        }
        public float Strut6Movement
        {
            get
            {
                return _Strut6Movement;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentException("value must be positive.");

                Set(ref _Strut6Movement, value);
            }
        }
   

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
