﻿using GalaSoft.MvvmLight;
using orthospin.Device;
using PDFParser;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace orthospin.Model
{
    public class Patient : ObservableObject
    {
        private string patienName = string.Empty;
        private DateTime? programGenerated = null;
        private ObservableCollection<PatientProgram> patientPrograms = new ObservableCollection<PatientProgram>();

        public string PatienName
        {
            get
            {
                return patienName;
            }
            set
            {
                Set(ref patienName, value);
            }
        }
        public DateTime? ProgramGenerated
        {
            get
            {
                return programGenerated;
            }
            set
            {
                Set(ref programGenerated, value);
            }
        }


        public ObservableCollection<PatientProgram> PatientPrograms
        {
            get
            {
                return patientPrograms;
            }
            set
            {
                Set(ref patientPrograms, value);
            }
        }


        public void SetProgram(List<PatientTable> Tableslist)
        {
            foreach (PatientTable patientTable in Tableslist)
            {

                int limit = patientTable.TableValues.Count / 6;

                for (int i = 0; i < limit; i++)
                {
                    PatientPrograms.Add(new PatientProgram()
                    {
                        Count = patientTable.count,
                        ExecuteTime = DeviceUtils.DateTime2UnixTime(patientTable.RunTime),
                        ExecuteDate = DeviceUtils.DateTime2UnixTime(patientTable.RunTime),
                        Strut1Movement = patientTable.TableValues[i],
                        Strut2Movement = patientTable.TableValues[(i + 1 * limit)],
                        Strut3Movement = patientTable.TableValues[(i + 2 * limit)],
                        Strut4Movement = patientTable.TableValues[(i + 3 * limit)],
                        Strut5Movement = patientTable.TableValues[(i + 4 * limit)],
                        Strut6Movement = patientTable.TableValues[(i + 5 * limit)],
                    });
                }
            }


            if (PatientPrograms.Count > 1)
            {
                PatientPrograms[0].ExecuteTime = PatientPrograms[1].ExecuteTime;
                PatientPrograms[0].ExecuteDate = PatientPrograms[1].ExecuteDate;
            }
        }
    }
}
