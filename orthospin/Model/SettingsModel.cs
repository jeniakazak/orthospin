﻿using System.Collections.Generic;

namespace orthospin.Model
{
    public class SettingsModel
    {
        public string com = "COM3";
        public float step = 0.25f;
        public float minResolution = 0.01f;
        public int encoder2mm = 20864;
        public uint fastRun = 1;
        public string username = "orthospin";
        public string password = "123456";

        public SettingsModel()
        {
        }

        public void RestoreToDefaults()
        {
            com = "COM3";
            step = 0.25f;
            encoder2mm = 20864;
            fastRun = 1;
        }
    }
}
