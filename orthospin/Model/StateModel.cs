﻿using GalaSoft.MvvmLight;

namespace orthospin.Model
{
    public class StateModel : ObservableObject
    {
        public int index;

        private string _title;

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                Set(ref _title, value);
            }
        }
    }
}
