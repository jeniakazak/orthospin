﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using Core;

namespace PDFParser
{
    class ProcessPdf
    {
        static Regex dateRegex = new Regex(@"\b(((0?[469]|11)/(0?[1-9]|[12]\d|30)|(0?[13578]|1[02])/(0?[1-9]|[12]\d|3[01])|0?2/(0?[1-9]|1\d|2[0-8]))/([1-9]\d{3}|\d{2})|0?2/29/([1-9]\d)?([02468][048]|[13579][26]))\b", RegexOptions.ExplicitCapture);

        static Regex alpabetRegex = new Regex("[^a-zA-Z]");

        static Regex numericRegex = new Regex("[^0-9]");

        static string[] weekDays = new string[] { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

        static string[] dateFormats = new string[] { "M/d/yyyy", "M/d/y" };

        static string[] oldDoc = new string[] { "Date", "Day", "Swap", "Strut 1", "Strut 2", "Strut 3", "Strut 4", "Strut 5", "Strut 6" };

        static string[] newDoc = new string[] { "Date", "WkDay", "Day", "Strut 1", "Strut 2", "Strut 3", "Strut 4", "Strut 5", "Strut 6" };

        public static void Extract(string _Page, PatientData patientData , DataTypes.DocType docType)
        {
            PatientTable patientTable = null;

            List<string []> TableRows = ExtractTableRows(_Page, docType);

            foreach(string [] row in TableRows)
            {
                patientTable = new PatientTable();

                if (!row[0].ToLower().Equals("initial"))
                {
                    DateTime dateTime;
                    if (DateTime.TryParseExact(row[0],
                            dateFormats, CultureInfo.InvariantCulture, DateTimeStyles.None,
                            out dateTime))
                    {
                        patientTable.RunTime = dateTime;
                    }
                }

                patientTable.count = uint.Parse(row[1]);

                for (int i = 2; i <= 7; i++)
                {
                    patientTable.TableValues.Add(float.Parse(row[i]));
                }

                patientData.patientTables.Add(patientTable);
            }


            GetGeneratedData(_Page, patientData, docType);

            GetPatientName(_Page, patientData, docType);
        }

        public static void Extract_Backup(string _Page, PatientData patientData, DataTypes.DocType docType)
        {
            string[] pageRows = _Page.Split(new string[] { Environment.NewLine, "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            List<string> pageRowsList = new List<string>(pageRows);

            bool IsTableRow = false;

            PatientTable patientTable = null;

            for (int i = 0; i < pageRowsList.Count; i++)
            {
                Match x = dateRegex.Match(pageRowsList[i]);

                string initial = alpabetRegex.Replace(pageRowsList[i], string.Empty);

                bool isInitial = initial.ToLower().Equals("initial");

                if ((x.Success == true || isInitial))
                {
                    patientTable = new PatientTable();
                    if (!isInitial)
                    {
                        DateTime dateTime;
                        string dateGoesHere = x.Value;
                        dateGoesHere = dateGoesHere.Replace(" ", "");

                        if (DateTime.TryParseExact(dateGoesHere,
                            dateFormats, CultureInfo.InvariantCulture, DateTimeStyles.None,
                            out dateTime))
                        {
                            patientTable.RunTime = dateTime;
                        }
                    }
                    patientData.patientTables.Add(patientTable);
                    IsTableRow = true;
                    ++i;
                }

                if (IsTableRow)
                {
                    string[] splitedRow = pageRowsList[i].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);


                    if (((splitedRow.Length % 6) == 0 || splitedRow.Length == 1))
                    {

                        foreach (string str in splitedRow)
                        {
                            string valueMy = str.Replace("*", "");

                            float TableValue = 0.0f;

                            if (float.TryParse(valueMy, out TableValue))
                            {
                                patientTable.TableValues.Add(float.Parse(valueMy));

                            }
                            else
                            {
                                IsTableRow = false;
                            }
                        }
                    }
                    else
                    {
                        IsTableRow = false;
                    }
                }

                //get the patient name.
                if (pageRowsList[i].ToLower().Contains("patient name") && string.IsNullOrWhiteSpace(patientData.PatienName))
                {
                    string[] nameGoesHere = pageRowsList[i].Split(new string[] { ":", ";", "-" }, StringSplitOptions.RemoveEmptyEntries);
                    if (nameGoesHere.Length > 1)
                    {
                        string name = nameGoesHere[1].TrimStart();
                        name = name.TrimEnd();
                        patientData.PatienName = name;
                    }
                }

                //get the pdf creation date.
                if (pageRowsList[i].ToLower().Contains("generated") && patientData.GeneratedDate == null)
                {
                    string dateGoesHere = pageRowsList[i].ToLower().Replace("generated", "");


                    if (pageRowsList[i].ToLower().Contains("am"))
                    {
                        dateGoesHere = dateGoesHere.Substring(0, dateGoesHere.IndexOf("am"));
                        dateGoesHere += " am";
                    }
                    else if (pageRowsList[i].ToLower().Contains("pm"))
                    {
                        dateGoesHere = dateGoesHere.Substring(0, dateGoesHere.IndexOf("pm"));
                        dateGoesHere += " pm";
                    }


                    if (dateGoesHere.Length > 1)
                    {
                        dateGoesHere = dateGoesHere.TrimStart();
                        dateGoesHere = dateGoesHere.TrimEnd();
                        //dateGoesHere = dateGoesHere.Replace(" ", "");

                        DateTime dateTime;

                        if (DateTime.TryParse(dateGoesHere, out dateTime))
                        {
                            patientData.GeneratedDate = dateTime;
                        }
                    }
                }
            }
        }

        private static List<string []> ExtractTableRows(string PageRows , DataTypes.DocType docType)
        {
            List<string> parsedRows = new List<string>();

            List<string> dates = new List<string>();

            List<string> tempRows = new List<string>();

            List<string[]> res = new List<string[]>();

            foreach (Match match in dateRegex.Matches(PageRows))
            {
                dates.Add(match.Value);
            }

            if (docType == DataTypes.DocType.oldDoc && PageRows.Contains("Initial"))
                dates.Insert(0, "Initial");

            string[] notProcessedRows = PageRows.Split(dates.ToArray(), StringSplitOptions.RemoveEmptyEntries);

            for (int i = 1; i < notProcessedRows.Length; i++)
            {

                notProcessedRows[i] = notProcessedRows[i].Trim();


                if(docType == DataTypes.DocType.newDoc)
                {
                    if (weekDays.Any(day => notProcessedRows[i].StartsWith(day)))
                    {
                        string line = numericRegex.Replace(notProcessedRows[i], " ");

                        line = Regex.Replace(line, @"\s+", " ");

                        string[] tableRowSplitted = line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                        if (tableRowSplitted.Length >= 7)
                        {
                            string parsedLine = tableRowSplitted[0] + " " + tableRowSplitted[1] + " " + tableRowSplitted[2] + " " +
                                tableRowSplitted[3] + " " + tableRowSplitted[4] + " " + tableRowSplitted[5] + " " + tableRowSplitted[6];

                            tempRows.Add(parsedLine);

                        }

                    }
                }
                else if (docType == DataTypes.DocType.oldDoc)
                {
                    var output = Regex.Replace(notProcessedRows[i].Split()[0], @"[^0-9a-zA-Z\ ]+", "");
                    int count = 0;
                    if (int.TryParse(output, out count))
                    {

                        string line = numericRegex.Replace(notProcessedRows[i], " ");

                        line = Regex.Replace(line, @"\s+", " ");

                        string[] tableRowSplitted = line.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                        if(tableRowSplitted.Length >= 7)
                        {
                            string parsedLine = tableRowSplitted[0] + " " + tableRowSplitted[1] + " " + tableRowSplitted[2] + " " +
                                tableRowSplitted[3] + " " + tableRowSplitted[4] + " " + tableRowSplitted[5] + " " + tableRowSplitted[6];

                            tempRows.Add(parsedLine);

                        }
                        else if (tableRowSplitted.Length == 6 && (i == 0 || i ==1))
                        {
                            string parsedLine = "0" + " " + tableRowSplitted[0] + " " + tableRowSplitted[1] + " " +
                                tableRowSplitted[2] + " " + tableRowSplitted[3] + " " + tableRowSplitted[4] + " " + tableRowSplitted[5];

                            tempRows.Add(parsedLine);
                        }
                    }
                }
            }

            for (int k = 0; k < tempRows.Count; k++)
            {
                string resLine = tempRows[k];

                if ( k < dates.Count)
                    resLine = dates[k] + " " + resLine;

                parsedRows.Add(resLine);
            }


            for (int l = 0; l < parsedRows.Count; l++)
            {

                res.Add(parsedRows[l].Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries));

            }

            return res;
        }

        private static void GetGeneratedData(string _Page, PatientData patientData, DataTypes.DocType docType)
        {
            string[] pageRows = _Page.Split(new string[] { Environment.NewLine, "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            List<string> pageRowsList = new List<string>(pageRows);

            for (int i = 0; i < pageRowsList.Count; i++)
            {

                //get the pdf creation date.
                if (pageRowsList[i].ToLower().Contains("generated") && patientData.GeneratedDate == null)
                {
                    string dateGoesHere = pageRowsList[i].ToLower().Replace("generated", "");


                    if (pageRowsList[i].ToLower().Contains("am"))
                    {
                        dateGoesHere = dateGoesHere.Substring(0, dateGoesHere.IndexOf("am"));
                        dateGoesHere += " am";
                    }
                    else if (pageRowsList[i].ToLower().Contains("pm"))
                    {
                        dateGoesHere = dateGoesHere.Substring(0, dateGoesHere.IndexOf("pm"));
                        dateGoesHere += " pm";
                    }


                    if (dateGoesHere.Length > 1)
                    {
                        dateGoesHere = dateGoesHere.TrimStart();
                        dateGoesHere = dateGoesHere.TrimEnd();
                        //dateGoesHere = dateGoesHere.Replace(" ", "");

                        DateTime dateTime;

                        if (DateTime.TryParse(dateGoesHere, out dateTime))
                        {
                            patientData.GeneratedDate = dateTime;
                        }
                    }
                }

            }
        }

        private static void GetPatientName(string _Page, PatientData patientData, DataTypes.DocType docType)
        {
            string[] pageRows = _Page.Split(new string[] { Environment.NewLine, "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            List<string> pageRowsList = new List<string>(pageRows);

            for (int i = 0; i < pageRowsList.Count; i++)
            {

                //get the patient name.
                if (pageRowsList[i].ToLower().Contains("patient name") && string.IsNullOrWhiteSpace(patientData.PatienName))
                {
                    string[] nameGoesHere = pageRowsList[i].Split(new string[] { ":", ";", "-" }, StringSplitOptions.RemoveEmptyEntries);
                    if (nameGoesHere.Length > 1)
                    {
                        string name = nameGoesHere[1].TrimStart();
                        name = name.TrimEnd();
                        patientData.PatienName = name;
                    }
                }

            }

        }

        public static DataTypes.DocType CheckDocType(string PageRows)
        {
            DataTypes.DocType ret = DataTypes.DocType.undefinedDoc;

            bool isOldDoc = oldDoc.All(oldd => PageRows.Contains(oldd));

            bool isNewDoc = newDoc.All(newd => PageRows.Contains(newd));

            if (isOldDoc)
            {
                ret = DataTypes.DocType.oldDoc;

            }else if (isNewDoc)
            {
                ret = DataTypes.DocType.newDoc;
            }
            else if (isOldDoc && isNewDoc)
            {
                ret = DataTypes.DocType.undefinedDoc;
            }

            return ret;
        }

        static string ConvertStringArrayToString(string[] array)
        {
            // Concatenate all the elements into a StringBuilder.
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
                builder.Append(' ');
            }
            return builder.ToString();
        }

    }

    public class Dates
    {
        public string MyDate;
        public int RowNum;
    }

    public class PatientData
    {
        public string PatienName;
        public DateTime? GeneratedDate = null;
        public List<PatientTable> patientTables = new List<PatientTable>();
    }

    public class PatientTable
    {
        public uint count;
        public DateTime RunTime;
        public List<float> TableValues = new List<float>();
    }

    
}
