﻿using Core;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Collections.Generic;

namespace PDFParser
{
    public class ParsePdf
    {
        public static PatientData ReadPdfFile(string srcFileName)
        {
            PatientData patientData = new PatientData();

            PdfReader reader = new PdfReader(srcFileName);

            List<string> PagesContent = new List<string>();

            DataTypes.DocType DocumentType = DataTypes.DocType.undefinedDoc;

            for (int page = 1; page <= reader.NumberOfPages; page++)
            {
                ITextExtractionStrategy its = new SimpleTextExtractionStrategy();
                string pageCSVText = PdfTextExtractor.GetTextFromPage(reader, page, its);
                PagesContent.Add(pageCSVText);
               // ProcessPdf.Extract(pageCSVText, patientData);
            }


            for(int i = 0; i < PagesContent.Count; i++)
            {
                DocumentType = ProcessPdf.CheckDocType(PagesContent[i]);
                if (DocumentType != DataTypes.DocType.undefinedDoc)
                {
                    break;
                }
            }


            if(DocumentType != DataTypes.DocType.undefinedDoc)
            {
                for (int j = 0; j < PagesContent.Count; j++)
                {
                    ProcessPdf.Extract(PagesContent[j], patientData , DocumentType);
                }
            }
            else
            {
                throw new System.Exception("undefined type of pdf.");
            }



            //for (int i = 0; i <  patientData.patientTables.Count; i++)
            //{
            //    if(patientData.patientTables[i].TableValues.Count == 0 )
            //    {
            //        patientData.patientTables.RemoveAt(i);
            //        i--;
            //    }
            //    else
            //    {
            //        patientData.patientTables[i].count = (uint)i;
            //    }
            //}

            reader.Close();
            return patientData;
        }
    }
}
